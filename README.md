# Development
Build and run containers
```
docker-compose up -d
```
## Debug
1. Uncomment debug instructions in docker file. We need:
   - `RUN pecl install xdebug`
   - `ADD conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d`
2. Add loopback ip
    - `sudo ifconfig lo:0 10.254.254.254 up`
3. Configure phpstorm
    - Go to `Languages > PHP > Server`
    - Name: `localhost`
    - Server: `localhost`
    - Port: `5000`
    - Path on server: `/var/www`