<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Criteria;

use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\Entity\Filter;
use MyWaiter\Domain\Entity\Sorting;
use Symfony\Component\HttpFoundation\Request;

final class CriteriaResolver
{
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function createCriteria(): Criteria
    {
        $filter = [];
        $order = [];

        if ($value = $this->request->query->get('filter')) {
            $filter = json_decode($value, true, 512, JSON_THROW_ON_ERROR);
        }

        if ($value = $this->request->query->get('order')) {
            $value = json_decode($value, true, 512, JSON_THROW_ON_ERROR);
            $order[$value[0]] = $value[1];
        }

        $criteria = new Criteria(new Filter($filter), new Sorting($order));

        $criteria->pagerFromRequest($this->request);

        return $criteria;
    }
}
