<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\SideDish\Collection;
use MyWaiter\Domain\Dish\SideDish\SideDish as SideDishEntity;

final class SideDishCollection extends BaseCollection
{
    /**
     * @var SideDish[]
     */
    public array $models;

    public function __construct(int $total, int $offset, int $limit, array $models)
    {
        parent::__construct($total, $offset, $limit);

        $this->models = $models;
    }

    public static function fromDomainCollection(Collection $domain): self
    {
        $models = array_map(static fn (SideDishEntity $entity) => SideDish::fromEntity($entity), $domain->getEntities());

        return new self($domain->getTotal(), $domain->getOffset(), $domain->getLimit(), $models);
    }

    /**
     * @var SideDish[]
     */
    public function getModels(): array
    {
        return $this->models;
    }
}
