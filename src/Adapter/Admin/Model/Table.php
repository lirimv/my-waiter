<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Table\Table as DomainTable;

/**
 * @psalm-immutable
 */
final class Table
{
    public string $id;
    public string $number;
    public int $seats;
    public string $link;
    public string $channel;

    private function __construct(string $id, string $number, int $seats, string $link, string $channel)
    {
        $this->id = $id;
        $this->number = $number;
        $this->seats = $seats;
        $this->link = $link;
        $this->channel = $channel;
    }

    public static function fromEntity(DomainTable $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getNumber(),
            $entity->getSeats(),
            $entity->getLink(),
            $entity->getChannel()->getId()->toString(),
        );
    }
}
