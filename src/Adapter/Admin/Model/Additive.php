<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Additive\Additive as Entity;

final class Additive
{
    public string $abbreviation;
    public string $description;

    public function __construct(string $abbreviation, string $description)
    {
        $this->abbreviation = $abbreviation;
        $this->description = $description;
    }

    public static function fromEntity(Entity $entity): self
    {
        return new self(
            $entity->getAbbreviation(),
            $entity->getDescription()
        );
    }
}
