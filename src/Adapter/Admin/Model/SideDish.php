<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Additive\Additive as DomainAdditive;
use MyWaiter\Domain\Dish\Dish as DomainDish;
use MyWaiter\Domain\Dish\Extra\Extra as DomainExtra;
use MyWaiter\Domain\Dish\Ingredient\Ingredient as DomainIngredient;
use MyWaiter\Domain\Dish\SideDish\SideDish as DomainSideDish;

/**
 * @psalm-immutable
 */
final class SideDish
{
    public string $id;
    public string $name;
    public Price $price;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $dishes;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $extras;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $ingredients;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $additives;

    public static function fromEntity(DomainSideDish $entity): self
    {
        $self = new self();
        $self->id = $entity->getId()->toString();
        $self->name = $entity->getName();
        $self->price = Price::fromDomain($entity->getPrice());
        $self->dishes = array_map(static fn (DomainDish $dish): string => $dish->getId()->toString(), $entity->getDishes());
        $self->extras = array_map(static fn (DomainExtra $extra): string => $extra->getId()->toString(), $entity->getExtras());
        $self->ingredients = array_map(static fn (DomainIngredient $ingredient): string => $ingredient->getId()->toString(), $entity->getIngredients());
        $self->additives = array_map(static fn (DomainAdditive $additive): string => $additive->getId()->toString(), $entity->getAdditives());

        return $self;
    }
}
