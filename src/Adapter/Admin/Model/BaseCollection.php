<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseCollection
{
    public int $total;
    public int $offset;
    public int $limit;

    public function __construct(int $total, int $offset, int $limit)
    {
        $this->total = $total;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    public function toJsonResponse(Request $request): JsonResponse
    {
        $response = new JsonResponse($this->getModels(), Response::HTTP_OK, [
            'X-Total-Count' => $this->total,
            'content-range' => sprintf('%s-%s/%s', $this->offset, $this->limit, $this->total),
        ]);

        $response->setEtag(md5(json_encode($this->getModels(), JSON_THROW_ON_ERROR)));
        $response->setPublic();
        $response->isNotModified($request);

        return $response;
    }

    abstract public function getModels(): array;
}
