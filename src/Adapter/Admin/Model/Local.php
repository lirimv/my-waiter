<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Local\Local as DomainLocal;

/**
 * @psalm-immutable
 */
final class Local
{
    public string $id;
    public string $name;
    public Account $account;
    public Address $address;

    private function __construct(string $id, string $name, Account $account, Address $address)
    {
        $this->id = $id;
        $this->name = $name;
        $this->account = $account;
        $this->address = $address;
    }

    public static function fromEntity(DomainLocal $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getName(),
            Account::fromEntity($entity->getAccount()),
            Address::fromEntity($entity->getAddress()),
        );
    }
}
