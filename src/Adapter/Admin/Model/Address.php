<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Address\Address as DomainAddress;

/**
 * @psalm-immutable
 */
final class Address
{
    public string $id;
    public string $street;
    public ?string $streetNumber;
    public string $city;
    public ?string $postalCode;
    public ?string $state;
    public ?string $country;

    public function __construct(string $id, string $street, ?string $streetNumber, string $city, ?string $postalCode, ?string $state, ?string $country)
    {
        $this->id = $id;
        $this->street = $street;
        $this->streetNumber = $streetNumber;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->state = $state;
        $this->country = $country;
    }

    public static function fromEntity(DomainAddress $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getStreet(),
            $entity->getStreetNumber(),
            $entity->getCity(),
            $entity->getPostalCode(),
            $entity->getState(),
            $entity->getCountry()
        );
    }
}
