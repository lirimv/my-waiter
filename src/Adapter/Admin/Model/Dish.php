<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Dish as DomainDish;
use MyWaiter\Domain\Dish\Extra\Extra as ExtraEntity;
use MyWaiter\Domain\Dish\SideDish\SideDish as SideDishEntity;
use MyWaiter\Domain\Dish\Variation\DishVariation as VariationEntity;

/**
 * @psalm-immutable
 */
final class Dish
{
    public string $id;
    public string $number;
    public string $name;
    public string $description;
    public Price $price;
    public string $group;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $sideDishes;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $variations;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $extras;

    private function __construct(string $id, string $number, string $name, string $description, Price $price, string $group, array $sideDishes, array $variations, array $extras)
    {
        $this->id = $id;
        $this->number = $number;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->group = $group;
        $this->sideDishes = $sideDishes;
        $this->variations = $variations;
        $this->extras = $extras;
    }

    public static function fromEntity(DomainDish $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getNumber(),
            $entity->getName(),
            $entity->getDescription(),
            Price::fromDomain($entity->getPrice()),
            $entity->getGroup()->getId()->toString(),
            array_map(static fn (SideDishEntity $sideDishEntity): string => $sideDishEntity->getId()->toString(), $entity->getSideDishes()),
            array_map(static fn (VariationEntity $variation): string => $variation->getId()->toString(), $entity->getVariations()),
            array_map(static fn (ExtraEntity $extra): string => $extra->getId()->toString(), $entity->getExtras()),
        );
    }
}
