<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Address\Address as DomainAddress;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class AddressCollection
{
    /**
     * @OA\Property(type="number", example="1")
     */
    public int $amount;

    /**
     * @OA\Property(type="array", items=@OA\Items(ref=@Model(type=Address::class)))
     */
    public array $addresses;

    private function __construct(int $amount, array $addresses)
    {
        $this->amount = $amount;
        $this->addresses = $addresses;
    }

    /**
     * @param array<int, DomainAddress> $addresses
     * @psalm-param list<DomainAddress> $addresses
     */
    public static function fromEntities(array $addresses): self
    {
        return new self(
            count($addresses),
            array_map(static fn (DomainAddress $entity) => Address::fromEntity($entity), $addresses)
        );
    }
}
