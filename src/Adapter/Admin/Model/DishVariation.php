<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Variation\DishVariation as DomainDishVariation;

final class DishVariation
{
    public string $id;
    public string $name;
    public string $type;
    public Price $price;
    public string $dish;

    private function __construct(string $id, string $name, string $type, Price $price, string $dish)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->price = $price;
        $this->dish = $dish;
    }

    public static function fromEntity(DomainDishVariation $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getName(),
            (string) $entity->getType(),
            Price::fromDomain($entity->getPrice()),
            $entity->getDish()->getId()->toString()
        );
    }
}
