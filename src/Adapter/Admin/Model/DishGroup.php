<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Group\DishGroup as DomainDishGroup;

/**
 * @psalm-immutable
 */
final class DishGroup
{
    public string $id;
    public string $name;
    public ?string $description;

    private function __construct(string $id, string $name, ?string $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    public static function fromEntity(DomainDishGroup $entity): self
    {
        return new self(
           $entity->getId()->toString(),
           $entity->getName(),
           $entity->getDescription()
       );
    }
}
