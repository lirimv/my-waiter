<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Assortment\Assortment as DomainAssortment;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class AssortmentCollection
{
    /**
     * @OA\Property(type="number", example="1")
     */
    public int $amount;

    /**
     * @OA\Property(type="array", items=@OA\Items(ref=@Model(type=Assortment::class)))
     */
    public array $assortments;

    private function __construct(int $amount, array $assortments)
    {
        $this->amount = $amount;
        $this->assortments = $assortments;
    }

    /**
     * @param array<int, DomainAssortment> $assortments
     * @psalm-param list<DomainAssortment> $assortments
     */
    public static function fromEntities(array $assortments): self
    {
        return new self(
            count($assortments),
            array_map(static fn (DomainAssortment $entity) => Assortment::fromEntity($entity), $assortments)
        );
    }
}
