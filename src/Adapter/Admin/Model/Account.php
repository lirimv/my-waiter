<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Account\Account as DomainAccount;

/**
 * @psalm-immutable
 */
final class Account
{
    public string $id;

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function fromEntity(DomainAccount $account): self
    {
        return new self(
            $account->getId()->toString()
        );
    }
}
