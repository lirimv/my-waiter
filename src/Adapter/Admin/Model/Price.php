<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Price as DomainPrice;

final class Price
{
    public float $gross;
    public string $currency;

    public function __construct(float $gross, string $currency)
    {
        $this->gross = $gross;
        $this->currency = $currency;
    }

    public static function fromDomain(DomainPrice $price): self
    {
        $gross = $price->getGrossPrice();

        return new self((float) ($gross->getAmount() / 100), $gross->getCurrency()->getCode());
    }
}
