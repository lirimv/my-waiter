<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Collection;
use MyWaiter\Domain\Dish\Dish as DishEntity;

final class DishCollection extends BaseCollection
{
    /**
     * @var Dish[]
     */
    public array $models;

    public function __construct(int $total, int $offset, int $limit, array $models)
    {
        parent::__construct($total, $offset, $limit);

        $this->models = $models;
    }

    public static function fromDomainCollection(Collection $domain): self
    {
        $models = array_map(static fn (DishEntity $entity) => Dish::fromEntity($entity), $domain->getEntities());

        return new self($domain->getTotal(), $domain->getOffset(), $domain->getLimit(), $models);
    }

    /**
     * @var Dish[]
     */
    public function getModels(): array
    {
        return $this->models;
    }
}
