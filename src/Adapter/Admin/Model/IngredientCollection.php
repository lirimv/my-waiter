<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Ingredient\Ingredient as IngredientEntity;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class IngredientCollection
{
    /**
     * @OA\Property(type="number", example="1")
     */
    public int $amount;

    /**
     * @OA\Property(type="array", items=@OA\Items(ref=@Model(type=Ingredient::class)))
     */
    public array $ingredients;

    public function __construct(int $amount, array $ingredients)
    {
        $this->amount = $amount;
        $this->ingredients = $ingredients;
    }

    /**
     * @param array<int, IngredientEntity> $ingredients
     * @psalm-param list<IngredientEntity> $ingredients
     */
    public static function fromEntities(array $ingredients): self
    {
        return new self(
            count($ingredients),
            array_map(static fn (IngredientEntity $entity) => Ingredient::fromEntity($entity), $ingredients)
        );
    }
}
