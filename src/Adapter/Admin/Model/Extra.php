<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Dish as DishEntity;
use MyWaiter\Domain\Dish\Extra\Extra as ExtraEntity;
use MyWaiter\Domain\Dish\SideDish\SideDish as SideDishEntity;

final class Extra
{
    public string $id;
    public string $name;
    public Price $price;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $dishes;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $sideDishes;

    private function __construct(string $id, string $name, Price $price, array $dishes, array $sideDishes)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
    }

    public static function fromEntity(ExtraEntity $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getName(),
            Price::fromDomain($entity->getPrice()),
            array_map(static fn (DishEntity $dish): string => $dish->getId()->toString(), $entity->getDishes()),
            array_map(static fn (SideDishEntity $sideDish): string => $sideDish->getId()->toString(), $entity->getSideDishes()),
        );
    }
}
