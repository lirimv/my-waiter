<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Table\Table as TableEntity;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class TableCollection
{
    /**
     * @OA\Property(type="number", example="1")
     */
    public int $amount;

    /**
     * @OA\Property(type="array", items=@OA\Items(ref=@Model(type=Table::class)))
     */
    public array $tables;

    public function __construct(int $amount, array $tables)
    {
        $this->amount = $amount;
        $this->tables = $tables;
    }

    /**
     * @param array<int, TableEntity> $tables
     * @psalm-param list<TableEntity> $tables
     */
    public static function fromEntities(array $tables): self
    {
        return new self(
            count($tables),
            array_map(static fn (TableEntity $entity) => Table::fromEntity($entity), $tables)
        );
    }
}
