<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Dish as DishEntity;
use MyWaiter\Domain\Dish\Ingredient\Ingredient as DomainIngredient;
use MyWaiter\Domain\Dish\SideDish\SideDish as SideDishEntity;

final class Ingredient
{
    public string $id;
    public string $name;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $dishes;

    /**
     * @var string[]
     *
     * @psalm-var list<string>
     */
    public array $sideDishes;

    private function __construct(string $id, string $name, array $dishes, array $sideDishes)
    {
        $this->id = $id;
        $this->name = $name;
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
    }

    public static function fromEntity(DomainIngredient $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getName(),
            array_map(static fn (DishEntity $dish) => $dish->getId()->toString(), $entity->getDishes()),
            array_map(static fn (SideDishEntity $sideDish) => $sideDish->getId()->toString(), $entity->getSideDishes()),
        );
    }
}
