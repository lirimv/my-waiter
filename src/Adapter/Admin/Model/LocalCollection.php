<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Local\Local as DomainLocal;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class LocalCollection
{
    /**
     * @OA\Property(type="number", example="1")
     */
    public int $amount;

    /**
     * @OA\Property(type="array", items=@OA\Items(ref=@Model(type=Local::class)))
     */
    public array $locals;

    private function __construct(int $amount, array $locals)
    {
        $this->amount = $amount;
        $this->locals = $locals;
    }

    /**
     * @param array<int, DomainLocal> $locals
     * @psalm-param list<DomainLocal> $locals
     */
    public static function fromEntities(array $locals): self
    {
        return new self(
            count($locals),
            array_map(static fn (DomainLocal $entity) => Local::fromEntity($entity), $locals)
        );
    }
}
