<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Assortment\Assortment as DomainAssortment;
use MyWaiter\Domain\Channel\Channel as DomainChannel;
use MyWaiter\Domain\Dish\Dish as DomainDish;

/**
 * @psalm-immutable
 */
final class Assortment
{
    public string $id;
    public string $name;

    /**
     * @var string[]
     */
    public array $dishes;

    /**
     * @var string[]
     */
    public array $channels;

    public function __construct(string $id, string $name, array $dishes, array $channels)
    {
        $this->id = $id;
        $this->name = $name;
        $this->dishes = $dishes;
        $this->channels = $channels;
    }

    public static function fromEntity(DomainAssortment $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getName(),
            array_map(static fn (DomainDish $entity) => $entity->getId()->toString(), $entity->getDishes()),
            array_map(static fn (DomainChannel $entity) => $entity->getId()->toString(), $entity->getChannels())
        );
    }
}
