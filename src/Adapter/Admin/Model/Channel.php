<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Assortment\Assortment as DomainAssortment;
use MyWaiter\Domain\Channel\Channel as DomainChannel;
use MyWaiter\Domain\Local\Local as DomainLocal;
use MyWaiter\Domain\Table\Table as DomainTable;

/**
 * @psalm-immutable
 */
final class Channel
{
    public string $id;
    public string $name;
    public string $email;
    public string $type;
    public ?string $baseUrl;

    /**
     * @var string[]
     */
    public array $locals;

    /**
     * @var string[]
     */
    public array $assortments;

    /**
     * @var string[]
     */
    public array $tables;

    private function __construct(string $id, string $name, string $email, string $type, ?string $baseUrl, array $locals, array $assortments, array $tables)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->type = $type;
        $this->baseUrl = $baseUrl;
        $this->locals = $locals;
        $this->assortments = $assortments;
        $this->tables = $tables;
    }

    public static function fromEntity(DomainChannel $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getName(),
            $entity->getEmail(),
            (string) $entity->getType(),
            $entity->getBaseUrl(),
            array_map(static fn (DomainLocal $entity) => $entity->getId()->toString(), $entity->getLocals()),
            array_map(static fn (DomainAssortment $entity) => $entity->getId()->toString(), $entity->getAssortments()),
            array_map(static fn (DomainTable $entity) => $entity->getId()->toString(), $entity->getTables())
        );
    }
}
