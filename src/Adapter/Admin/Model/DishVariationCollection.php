<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Variation\Collection;
use MyWaiter\Domain\Dish\Variation\DishVariation as DishVariationEntity;

final class DishVariationCollection extends BaseCollection
{
    /**
     * @var DishVariation[]
     */
    public array $models;

    public function __construct(int $total, int $offset, int $limit, array $models)
    {
        parent::__construct($total, $offset, $limit);

        $this->models = $models;
    }

    public static function fromDomainCollection(Collection $domain): self
    {
        $models = array_map(static fn (DishVariationEntity $entity) => DishVariation::fromEntity($entity), $domain->getEntities());

        return new self($domain->getTotal(), $domain->getOffset(), $domain->getLimit(), $models);
    }

    /**
     * @var DishVariation[]
     */
    public function getModels(): array
    {
        return $this->models;
    }
}
