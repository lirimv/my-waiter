<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Channel\Channel as DomainChannel;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class ChannelCollection
{
    /**
     * @OA\Property(type="number", example="1")
     */
    public int $amount;

    /**
     * @OA\Property(type="array", items=@OA\Items(ref=@Model(type=Channel::class)))
     */
    public array $channels;

    private function __construct(int $amount, array $channels)
    {
        $this->amount = $amount;
        $this->channels = $channels;
    }

    /**
     * @param array<int, DomainChannel> $entities
     * @psalm-param list<DomainChannel> $entities
     */
    public static function fromEntities(array $entities): self
    {
        return new self(
            count($entities),
            array_map(static fn (DomainChannel $entity) => Channel::fromEntity($entity), $entities)
        );
    }
}
