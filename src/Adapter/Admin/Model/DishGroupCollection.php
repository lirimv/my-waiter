<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Model;

use MyWaiter\Domain\Dish\Group\Collection;
use MyWaiter\Domain\Dish\Group\DishGroup as DishGroupEntity;

final class DishGroupCollection extends BaseCollection
{
    /**
     * @var DishGroup[]
     */
    public array $models;

    public function __construct(int $total, int $offset, int $limit, array $models)
    {
        parent::__construct($total, $offset, $limit);

        $this->models = $models;
    }

    public static function fromDomainCollection(Collection $domain): self
    {
        $models = array_map(static fn (DishGroupEntity $entity) => DishGroup::fromEntity($entity), $domain->getEntities());

        return new self($domain->getTotal(), $domain->getOffset(), $domain->getLimit(), $models);
    }

    /**
     * @var DishGroup[]
     */
    public function getModels(): array
    {
        return $this->models;
    }
}
