<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Model\Ingredient;
use MyWaiter\Adapter\Admin\Model\IngredientCollection;
use MyWaiter\Domain\Dish\Ingredient\Command\CreateIngredient;
use MyWaiter\Domain\Dish\Ingredient\Command\ReplaceIngredient;
use MyWaiter\Domain\Dish\Ingredient\Event\IngredientDeleted;
use MyWaiter\Domain\Dish\Ingredient\IngredientRepositoryInterface;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/ingredient")
 *
 * @OA\Tag(name="DishIngredient")
 *
 * @IsGranted("ROLE_ADMIN")
 */
final class IngredientController
{
    private IngredientRepositoryInterface $ingredients;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        IngredientRepositoryInterface $ingredients,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->ingredients = $ingredients;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * List all ingredients.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="All ingredients.",
     *     @Model(type=IngredientCollection::class)
     * )
     */
    public function getAction(): JsonResponse
    {
        $entities = $this->ingredients->getAll();

        return new JsonResponse(IngredientCollection::fromEntities($entities), Response::HTTP_OK);
    }

    /**
     * Get a ingredient resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Ingredient resource.",
     *     @OA\JsonContent(ref=@Model(type=Ingredient::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->ingredients->get(Uuid::fromString($id));

        return new JsonResponse(Ingredient::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates an ingredient resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Ingredient resource created.",
     *     @OA\JsonContent(ref=@Model(type=Ingredient::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Ingredient",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateIngredient::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateIngredient::class);

        if (!$command instanceof CreateIngredient) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->ingredients->get($command->getId());

        return new JsonResponse(Ingredient::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces an ingredient resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Ingredient resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Ingredient::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Ingredient",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceIngredient::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceIngredient::class);

        if (!$command instanceof ReplaceIngredient) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->ingredients->get($command->id());

        return new JsonResponse(Ingredient::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes an ingredient resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Ingredient resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->ingredients->get(Uuid::fromString($id));

        $this->ingredients->delete($entity);

        $this->eventDispatcher->dispatch(new IngredientDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
