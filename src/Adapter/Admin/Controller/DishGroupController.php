<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Criteria\CriteriaResolver;
use MyWaiter\Adapter\Admin\Model\DishGroup;
use MyWaiter\Adapter\Admin\Model\DishGroupCollection;
use MyWaiter\Domain\Dish\Group\Command\CreateDishGroup;
use MyWaiter\Domain\Dish\Group\Command\ReplaceDishGroup;
use MyWaiter\Domain\Dish\Group\DishGroupRepositoryInterface;
use MyWaiter\Domain\Dish\Group\Event\DishGroupDeleted;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/dish-group")
 *
 * @OA\Tag(name="DishGroup")
 *
 * @IsGranted("ROLE_ADMIN")
 */
final class DishGroupController
{
    private DishGroupRepositoryInterface $groups;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishGroupRepositoryInterface $groups,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->groups = $groups;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * List all dish groups.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="All dish groups.",
     *     @OA\JsonContent(type="array", items=@OA\Items(ref=@Model(type=DishGroup::class)))
     * )
     */
    public function getAction(Request $request): JsonResponse
    {
        $criteria = (new CriteriaResolver($request))->createCriteria();

        $groups = DishGroupCollection::fromDomainCollection(
            $this->groups->findByCriteria($criteria)
        );

        return $groups->toJsonResponse($request);
    }

    /**
     * Get a dish group resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Dish group resource.",
     *     @OA\JsonContent(ref=@Model(type=DishGroup::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->groups->get(Uuid::fromString($id));

        return new JsonResponse(DishGroup::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a dish group resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Dish group resource created.",
     *     @OA\JsonContent(ref=@Model(type=DishGroup::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Dish group",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateDishGroup::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateDishGroup::class);

        if (!$command instanceof CreateDishGroup) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->groups->get($command->getId());

        return new JsonResponse(DishGroup::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces a dish group resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Dish group resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=DishGroup::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Dish group",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceDishGroup::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceDishGroup::class);

        if (!$command instanceof ReplaceDishGroup) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->groups->get($command->id());

        return new JsonResponse(DishGroup::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a dish group resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Dish group resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->groups->get(Uuid::fromString($id));

        $this->groups->delete($entity);

        $this->eventDispatcher->dispatch(new DishGroupDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
