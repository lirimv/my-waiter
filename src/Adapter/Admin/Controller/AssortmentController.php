<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Model\Assortment;
use MyWaiter\Adapter\Admin\Model\AssortmentCollection;
use MyWaiter\Domain\Assortment\AssortmentRepositoryInterface;
use MyWaiter\Domain\Assortment\Command\CreateAssortment;
use MyWaiter\Domain\Assortment\Command\ReplaceAssortment;
use MyWaiter\Domain\Assortment\Event\AssortmentDeleted;
use MyWaiter\Domain\User\Role;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/assortment")
 *
 * @OA\Tag(name="Assortment")
 *
 * @IsGranted(Role::ADMIN_USER)
 */
final class AssortmentController
{
    private AssortmentRepositoryInterface $assortments;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AssortmentRepositoryInterface $assortments,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->assortments = $assortments;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Get all assortments.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Get all assortments.",
     *     @Model(type=AssortmentCollection::class)
     * )
     */
    public function get(): JsonResponse
    {
        $entities = $this->assortments->getAll();

        return new JsonResponse(AssortmentCollection::fromEntities($entities), Response::HTTP_OK);
    }

    /**
     * Get an assortment resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Assortment resource.",
     *     @OA\JsonContent(ref=@Model(type=Assortment::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->assortments->get(Uuid::fromString($id));

        return new JsonResponse(Assortment::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Create an assortment resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Assortment resource created.",
     *     @OA\JsonContent(ref=@Model(type=Assortment::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Assortment",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateAssortment::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateAssortment::class);

        if (!$command instanceof CreateAssortment) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->assortments->get($command->getId());

        return new JsonResponse(Assortment::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces an assortment resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Assortment resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Assortment::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Assortment",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceAssortment::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceAssortment::class);

        if (!$command instanceof ReplaceAssortment) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->assortments->get($command->id());

        return new JsonResponse(Assortment::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a local resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Assortment resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->assortments->get(Uuid::fromString($id));

        $this->assortments->delete($entity);

        $this->eventDispatcher->dispatch(new AssortmentDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
