<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Model\Local;
use MyWaiter\Adapter\Admin\Model\LocalCollection;
use MyWaiter\Domain\Local\Command\CreateLocal;
use MyWaiter\Domain\Local\Command\ReplaceLocal;
use MyWaiter\Domain\Local\Event\LocalDeleted;
use MyWaiter\Domain\Local\LocalRepositoryInterface;
use MyWaiter\Domain\User\Role;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/local")
 *
 * @OA\Tag(name="Local")
 *
 * @IsGranted(Role::ADMIN_USER)
 */
final class LocalController
{
    private LocalRepositoryInterface $locals;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        LocalRepositoryInterface $locals,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->locals = $locals;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Get all locals.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Get all locals.",
     *     @Model(type=LocalCollection::class)
     * )
     */
    public function get(): JsonResponse
    {
        $entities = $this->locals->getAll();

        return new JsonResponse(LocalCollection::fromEntities($entities), Response::HTTP_OK);
    }

    /**
     * Get a local resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Assortment resource.",
     *     @OA\JsonContent(ref=@Model(type=Local::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->locals->get(Uuid::fromString($id));

        return new JsonResponse(Local::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a local resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Local resource created.",
     *     @OA\JsonContent(ref=@Model(type=Local::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Local",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateLocal::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateLocal::class);

        if (!$command instanceof CreateLocal) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->locals->get($command->getId());

        return new JsonResponse(Local::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces a local resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Local resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Local::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Local",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceLocal::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceLocal::class);

        if (!$command instanceof ReplaceLocal) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->locals->get($command->id());

        return new JsonResponse(Local::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a local resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Local resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->locals->get(Uuid::fromString($id));

        $this->locals->delete($entity);

        $this->eventDispatcher->dispatch(new LocalDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
