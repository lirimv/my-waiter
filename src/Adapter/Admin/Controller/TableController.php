<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Model\Table;
use MyWaiter\Adapter\Admin\Model\TableCollection;
use MyWaiter\Domain\Table\Command\CreateTable;
use MyWaiter\Domain\Table\Command\ReplaceTable;
use MyWaiter\Domain\Table\Event\TableDeleted;
use MyWaiter\Domain\Table\TableRepositoryInterface;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/table")
 *
 * @OA\Tag(name="Table")
 *
 * @IsGranted("ROLE_ADMIN")
 */
final class TableController
{
    private TableRepositoryInterface $tables;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        TableRepositoryInterface $tables,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->tables = $tables;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * List all tables.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="All tables.",
     *     @Model(type=TableCollection::class)
     * )
     */
    public function get(): JsonResponse
    {
        $entities = $this->tables->getAll();

        return new JsonResponse(TableCollection::fromEntities($entities), Response::HTTP_OK);
    }

    /**
     * Get a table resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Table resource.",
     *     @OA\JsonContent(ref=@Model(type=Table::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->tables->get(Uuid::fromString($id));

        return new JsonResponse(Table::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a table resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Table resource created.",
     *     @OA\JsonContent(ref=@Model(type=Table::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Table",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateTable::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateTable::class);

        if (!$command instanceof CreateTable) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->tables->get($command->getId());

        return new JsonResponse(Table::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces a table resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Table resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Table::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Table",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceTable::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceTable::class);

        if (!$command instanceof ReplaceTable) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->tables->get($command->id());

        return new JsonResponse(Table::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a table resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Table resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->tables->get(Uuid::fromString($id));

        $this->tables->delete($entity);

        $this->eventDispatcher->dispatch(new TableDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
