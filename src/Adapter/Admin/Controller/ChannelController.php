<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Model\Channel;
use MyWaiter\Adapter\Admin\Model\ChannelCollection;
use MyWaiter\Domain\Channel\ChannelRepositoryInterface;
use MyWaiter\Domain\Channel\Command\CreateChannel;
use MyWaiter\Domain\Channel\Command\ReplaceChannel;
use MyWaiter\Domain\Channel\Event\ChannelDeleted;
use MyWaiter\Domain\User\Role;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/channel")
 *
 * @OA\Tag(name="Channel")
 *
 * @IsGranted(Role::ADMIN_USER)
 */
final class ChannelController
{
    private ChannelRepositoryInterface $channels;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ChannelRepositoryInterface $channels,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->channels = $channels;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Get all channels.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Get all channels.",
     *     @Model(type=ChannelCollection::class)
     * )
     */
    public function get(): JsonResponse
    {
        $channels = $this->channels->getAll();

        return new JsonResponse(ChannelCollection::fromEntities($channels), Response::HTTP_OK);
    }

    /**
     * Get a channel resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Channel resource.",
     *     @OA\JsonContent(ref=@Model(type=Channel::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->channels->get(Uuid::fromString($id));

        return new JsonResponse(Channel::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Create a channel resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Channel resource created.",
     *     @OA\JsonContent(ref=@Model(type=Channel::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Channel",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateChannel::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateChannel::class);

        if (!$command instanceof CreateChannel) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->channels->get($command->getId());

        return new JsonResponse(Channel::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces a channel resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Channel resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Channel::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Channel",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceChannel::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceChannel::class);

        if (!$command instanceof ReplaceChannel) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->channels->get($command->id());

        return new JsonResponse(Channel::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a channel resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Channel resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->channels->get(Uuid::fromString($id));

        $this->channels->delete($entity);

        $this->eventDispatcher->dispatch(new ChannelDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
