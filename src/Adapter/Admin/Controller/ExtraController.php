<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Criteria\CriteriaResolver;
use MyWaiter\Adapter\Admin\Model\Extra;
use MyWaiter\Adapter\Admin\Model\ExtraCollection;
use MyWaiter\Domain\Dish\Extra\Command\CreateExtra;
use MyWaiter\Domain\Dish\Extra\Command\ReplaceExtra;
use MyWaiter\Domain\Dish\Extra\Event\ExtraDeleted;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/dish-extra")
 *
 * @OA\Tag(name="DishExtra")
 *
 * @IsGranted("ROLE_ADMIN")
 */
final class ExtraController
{
    private ExtraRepositoryInterface $extras;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ExtraRepositoryInterface $extras,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->extras = $extras;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Get extras by criteria.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Extras",
     *     @OA\JsonContent(type="array", items=@OA\Items(ref=@Model(type=Extra::class)))
     * )
     */
    public function getAction(Request $request): JsonResponse
    {
        $criteria = (new CriteriaResolver($request))->createCriteria();

        $collection = ExtraCollection::fromDomainCollection(
            $this->extras->findByCriteria($criteria)
        );

        return $collection->toJsonResponse($request);
    }

    /**
     * Get an extra resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Extra resource.",
     *     @OA\JsonContent(ref=@Model(type=Extra::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->extras->get(Uuid::fromString($id));

        return new JsonResponse(Extra::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a extra resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Extra resource created.",
     *     @OA\JsonContent(ref=@Model(type=Extra::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Extra",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateExtra::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateExtra::class);

        if (!$command instanceof CreateExtra) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->extras->get($command->getId());

        return new JsonResponse(Extra::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces an extra resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Extra resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Extra::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Extra",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceExtra::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceExtra::class);

        if (!$command instanceof ReplaceExtra) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->extras->get($command->id());

        return new JsonResponse(Extra::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes an extra resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Extra resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->extras->get(Uuid::fromString($id));

        $this->extras->delete($entity);

        $this->eventDispatcher->dispatch(new ExtraDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
