<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Criteria\CriteriaResolver;
use MyWaiter\Adapter\Admin\Model\Dish;
use MyWaiter\Adapter\Admin\Model\DishCollection;
use MyWaiter\Domain\Dish\Command\AddSideDishToDish;
use MyWaiter\Domain\Dish\Command\CreateDish;
use MyWaiter\Domain\Dish\Command\ReplaceDish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Event\DishDeleted;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/dish")
 *
 * @OA\Tag(name="Dish")
 *
 * @IsGranted("ROLE_ADMIN")
 */
final class DishController
{
    private DishRepositoryInterface $dishes;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishRepositoryInterface $dishes,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->dishes = $dishes;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * List all dishes.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Dishes",
     *     @OA\JsonContent(type="array", items=@OA\Items(ref=@Model(type=Dish::class)))
     * )
     */
    public function getAction(Request $request): JsonResponse
    {
        $criteria = (new CriteriaResolver($request))->createCriteria();

        $dishCollection = DishCollection::fromDomainCollection(
            $this->dishes->findByCriteria($criteria)
        );

        return $dishCollection->toJsonResponse($request);
    }

    /**
     * Get a dish resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Dish resource.",
     *     @OA\JsonContent(ref=@Model(type=Dish::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->dishes->get(Uuid::fromString($id));

        return new JsonResponse(Dish::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a dish resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Dish resource created.",
     *     @OA\JsonContent(ref=@Model(type=Dish::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Dish",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateDish::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateDish::class);

        if (!$command instanceof CreateDish) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->dishes->get($command->getId());

        return new JsonResponse(Dish::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces a dish resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Dish resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Dish::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Dish",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceDish::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceDish::class);

        if (!$command instanceof ReplaceDish) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->dishes->get($command->id());

        return new JsonResponse(Dish::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a dish resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Dish resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->dishes->get(Uuid::fromString($id));

        $this->dishes->delete($entity);

        $this->eventDispatcher->dispatch(new DishDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Adds a side dish to a dish.
     *
     * @Route("/{id}/add-side-dish/{sideDishId}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     * @OA\Parameter(name="sideDishId", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Side dish added to dish"
     * )
     */
    public function addSideDish(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, AddSideDishToDish::class);

        $this->messageBus->dispatch($command);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
