<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Criteria\CriteriaResolver;
use MyWaiter\Adapter\Admin\Model\SideDish;
use MyWaiter\Adapter\Admin\Model\SideDishCollection;
use MyWaiter\Domain\Dish\SideDish\Command\CreateSideDish;
use MyWaiter\Domain\Dish\SideDish\Command\ReplaceSideDish;
use MyWaiter\Domain\Dish\SideDish\Event\SideDishDeleted;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/side-dish")
 *
 * @OA\Tag(name="SideDish")
 *
 * @IsGranted("ROLE_ADMIN")
 */
final class SideDishController
{
    private SideDishRepositoryInterface $sideDishes;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        SideDishRepositoryInterface $sideDishes,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->sideDishes = $sideDishes;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * List all side dishes.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="All side dishes.",
     *     @OA\JsonContent(type="array", items=@OA\Items(ref=@Model(type=SideDish::class)))
     * )
     */
    public function getAction(Request $request): JsonResponse
    {
        $criteria = (new CriteriaResolver($request))->createCriteria();

        $dishCollection = SideDishCollection::fromDomainCollection(
            $this->sideDishes->findByCriteria($criteria)
        );

        return $dishCollection->toJsonResponse($request);
    }

    /**
     * Get a side dish resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="SideDish resource.",
     *     @OA\JsonContent(ref=@Model(type=SideDish::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->sideDishes->get(Uuid::fromString($id));

        return new JsonResponse(SideDish::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a side dish resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="SideDish resource created.",
     *     @OA\JsonContent(ref=@Model(type=SideDish::class))
     * )
     *
     * @OA\RequestBody(
     *     description="SideDish",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateSideDish::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateSideDish::class);

        if (!$command instanceof CreateSideDish) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->sideDishes->get($command->getId());

        return new JsonResponse(SideDish::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces a side dish resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="SideDish resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=SideDish::class))
     * )
     *
     * @OA\RequestBody(
     *     description="SideDish",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceSideDish::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceSideDish::class);

        if (!$command instanceof ReplaceSideDish) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->sideDishes->get($command->id());

        return new JsonResponse(SideDish::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a side dish resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="SideDish resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->sideDishes->get(Uuid::fromString($id));

        $this->sideDishes->delete($entity);

        $this->eventDispatcher->dispatch(new SideDishDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
