<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use Exception;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Tag(name="Security")
 */
final class SecurityController
{
    /**
     * Retrieves a JWT.
     *
     * @Route("/login", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="A JWT response.",
     *     @OA\Schema(
     *         type="object",
     *         properties={
     *             @OA\Property(property="id", type="string"),
     *             @OA\Property(property="token", type="string")
     *         }
     *     )
     * )
     * @OA\RequestBody(
     *     description="request body",
     *     required=true,
     *     content={@OA\MediaType(
     *             mediaType="application/json",
     *             schema=@OA\Schema(
     *                 properties={
     *                     @OA\Property(property="username", type="string"),
     *                     @OA\Property(property="password", type="string")
     *                 }
     *             )
     *     )}
     * )
     */
    public function loginCheckAction(): void
    {
        // Only for documentation
        throw new Exception('Should not happened');
    }
}
