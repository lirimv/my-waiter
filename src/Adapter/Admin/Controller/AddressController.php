<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Model\Address;
use MyWaiter\Adapter\Admin\Model\AddressCollection;
use MyWaiter\Domain\Address\AddressRepositoryInterface;
use MyWaiter\Domain\Address\Command\CreateAddress;
use MyWaiter\Domain\Address\Command\ReplaceAddress;
use MyWaiter\Domain\Address\Event\AddressDeleted;
use MyWaiter\Domain\User\Role;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/address")
 *
 * @OA\Tag(name="Address")
 *
 * @IsGranted(Role::ADMIN_USER)
 */
final class AddressController
{
    private AddressRepositoryInterface $addresses;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AddressRepositoryInterface $addresses,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->addresses = $addresses;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Get all addresses.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="All addresses.",
     *     @Model(type=AddressCollection::class)
     * )
     */
    public function get(): JsonResponse
    {
        $entities = $this->addresses->getAll();

        return new JsonResponse(AddressCollection::fromEntities($entities), Response::HTTP_OK);
    }

    /**
     * Get an address resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Address resource.",
     *     @OA\JsonContent(ref=@Model(type=Address::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->addresses->get(Uuid::fromString($id));

        return new JsonResponse(Address::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a address resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Address resource created.",
     *     @OA\JsonContent(ref=@Model(type=Address::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Address",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateAddress::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateAddress::class);

        if (!$command instanceof CreateAddress) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->addresses->get($command->getId());

        return new JsonResponse(Address::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces an address resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Address resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=Address::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Address",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceAddress::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceAddress::class);

        if (!$command instanceof ReplaceAddress) {
            throw new \Exception();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->addresses->get($command->id());

        return new JsonResponse(Address::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes an address resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Address resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->addresses->get(Uuid::fromString($id));

        $this->addresses->delete($entity);

        $this->eventDispatcher->dispatch(new AddressDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
