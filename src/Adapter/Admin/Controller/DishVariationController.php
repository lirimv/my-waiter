<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Admin\Controller;

use MyWaiter\Adapter\Admin\Criteria\CriteriaResolver;
use MyWaiter\Adapter\Admin\Model\DishVariation;
use MyWaiter\Adapter\Admin\Model\DishVariationCollection;
use MyWaiter\Domain\Dish\Variation\Command\CreateDishVariation;
use MyWaiter\Domain\Dish\Variation\Command\ReplaceDishVariation;
use MyWaiter\Domain\Dish\Variation\DishVariationRepositoryInterface;
use MyWaiter\Domain\Dish\Variation\Event\DishVariationDeleted;
use MyWaiter\Infrastructure\Api\CommandResolver;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Psr\EventDispatcher\EventDispatcherInterface;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

/**
 * @Route("/dish-variation")
 *
 * @OA\Tag(name="DishVariation")
 *
 * @IsGranted("ROLE_ADMIN")
 */
final class DishVariationController
{
    private DishVariationRepositoryInterface $variations;
    private MessageBusInterface $messageBus;
    private CommandResolver $commandResolver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishVariationRepositoryInterface $sideDishes,
        MessageBusInterface $messageBus,
        CommandResolver $commandResolver,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->variations = $sideDishes;
        $this->messageBus = $messageBus;
        $this->commandResolver = $commandResolver;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Get dishes by criteria.
     *
     * @Route("", methods={"GET"})
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="All dish variation.",
     *     @Model(type=DishVariationCollection::class)
     * )
     */
    public function getAction(Request $request): JsonResponse
    {
        $criteria = (new CriteriaResolver($request))->createCriteria();

        $variationCollection = DishVariationCollection::fromDomainCollection(
            $this->variations->findByCriteria($criteria)
        );

        return $variationCollection->toJsonResponse($request);
    }

    /**
     * Get a dish variation resource by Id.
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Dish variation resource.",
     *     @OA\JsonContent(ref=@Model(type=DishVariation::class))
     * )
     */
    public function getById(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->variations->get(Uuid::fromString($id));

        return new JsonResponse(DishVariation::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Creates a dish variation resource.
     *
     * @Route("", methods={"POST"})
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Dish variation resource created.",
     *     @OA\JsonContent(ref=@Model(type=DishVariation::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Dish variation",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=CreateDishVariation::class))
     *     )}
     * )
     */
    public function post(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, CreateDishVariation::class);

        if (!$command instanceof CreateDishVariation) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->variations->get($command->getId());

        return new JsonResponse(DishVariation::fromEntity($entity), Response::HTTP_CREATED);
    }

    /**
     * Replaces a dish variation resource.
     *
     * @Route("/{id}", methods={"PUT"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Dish variation resource replaced.",
     *     @OA\JsonContent(ref=@Model(type=DishVariation::class))
     * )
     *
     * @OA\RequestBody(
     *     description="Dish variation",
     *     content={@OA\MediaType(
     *         mediaType="application/json",
     *         schema=@OA\Schema(ref=@Model(type=ReplaceDishVariation::class))
     *     )}
     * )
     */
    public function put(Request $request): JsonResponse
    {
        $command = $this->commandResolver->resolveRequest($request, ReplaceDishVariation::class);

        if (!$command instanceof ReplaceDishVariation) {
            throw new RuntimeException();
        }

        $this->messageBus->dispatch($command);

        $entity = $this->variations->get($command->id());

        return new JsonResponse(DishVariation::fromEntity($entity), Response::HTTP_OK);
    }

    /**
     * Deletes a dish variation resource.
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @OA\Parameter(name="id", in="path", allowEmptyValue=false, required=true, schema=@OA\Schema(type="string"))
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Dish variation resource deleted."
     * )
     */
    public function delete(string $id): JsonResponse
    {
        Assert::uuid($id);

        $entity = $this->variations->get(Uuid::fromString($id));

        $this->variations->delete($entity);

        $this->eventDispatcher->dispatch(new DishVariationDeleted($entity));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
