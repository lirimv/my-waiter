<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Controller;

use MyWaiter\Domain\Dish\DishRepositoryInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dish")
 *
 * @OA\Tag(name="Dish")
 */
final class DishController
{
    private DishRepositoryInterface $dishes;

    public function __construct(DishRepositoryInterface $dishes)
    {
        $this->dishes = $dishes;
    }
}
