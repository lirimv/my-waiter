<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Security;

use MyWaiter\Infrastructure\Security\AbstractUser;

final class AnonymousUser extends AbstractUser
{
    public function __construct()
    {
        parent::__construct('anonymous@mywaiter.de', null, ['SHOP_USER']);
    }

    public function isAdminUser(): bool
    {
        return false;
    }

    public function isShopUser(): bool
    {
        return false;
    }

    public function isAnonymousShopUser(): bool
    {
        return true;
    }
}
