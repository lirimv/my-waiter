<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Security;

use MyWaiter\Domain\User\User as DomainUser;
use MyWaiter\Infrastructure\Security\AbstractUser;

final class User extends AbstractUser
{
    public function isAdminUser(): bool
    {
        return false;
    }

    public function isShopUser(): bool
    {
        return true;
    }

    public function isAnonymousShopUser(): bool
    {
        return false;
    }

    public static function fromUser(DomainUser $user): self
    {
        return new self(
            $user->getEmail(),
            $user->getPassword(),
            $user->getRoles(),
        );
    }
}
