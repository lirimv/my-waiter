<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Exception\UserNotFoundException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\PayloadAwareUserProviderInterface;
use MyWaiter\Domain\User\UserRepositoryInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Webmozart\Assert\Assert;

final class UserProvider implements UserProviderInterface, PayloadAwareUserProviderInterface, PasswordUpgraderInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
    ) {
        $this->userRepository = $userRepository;
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        if ('' === $username) {
            return new AnonymousUser();
        }

        return $this->fetchUser($username);
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if ($user instanceof AnonymousUser) {
            return $user;
        }

        Assert::isInstanceOf($user, User::class);

        return $this->fetchUser($user->getUsername());
    }

    public function supportsClass(string $class): bool
    {
        return in_array($class, [User::class, AnonymousUser::class], true);
    }

    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        $entity = $this->userRepository->getByEmail($user->getUsername());
        $entity->setPassword($newEncodedPassword);
        $this->userRepository->save($entity);
    }

    public function loadUserByUsernameAndPayload($username, array $payload): UserInterface
    {
        return $this->fetchUser($username);
    }

    private function fetchUser(string $username): UserInterface
    {
        try {
            $user = $this->userRepository->getByEmail($username);
        } catch (UserNotFoundException $e) {
            throw new UsernameNotFoundException('User not found.');
        }

        return User::fromUser($user);
    }
}
