<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Model;

use MyWaiter\Domain\Dish\Dish as DomainDish;

/**
 * @psalm-immutable
 */
final class Dish
{
    public string $id;
    public string $number;
    public string $name;
    public string $description;
    public Price $price;
    public string $group;

    private function __construct(string $id, string $number, string $name, string $description, Price $price, string $group)
    {
        $this->id = $id;
        $this->number = $number;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->group = $group;
    }

    public static function fromEntity(DomainDish $entity): self
    {
        return new self(
            $entity->getId()->toString(),
            $entity->getNumber(),
            $entity->getName(),
            $entity->getDescription(),
            Price::fromDomain($entity->getPrice()),
            $entity->getGroup()->getId()->toString()
        );
    }
}
