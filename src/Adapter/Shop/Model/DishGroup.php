<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Model;

use MyWaiter\Domain\Dish\Group\DishGroup as DomainDishGroup;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class DishGroup
{
    /**
     * @OA\Property(type="string", example="d61130d8-a842-46c1-9e77-22ff8fa5f692")
     */
    public string $id;

    /**
     * @OA\Property(type="string", example="Salat")
     */
    public ?string $name = null;

    /**
     * @OA\Property(type="string", example="Vorspeise Salate")
     */
    public ?string $description = null;

    private function __construct(string $id, string $name, string $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    public static function fromEntity(DomainDishGroup $dishGroup): self
    {
        return new self(
            $dishGroup->getId()->toString(),
            $dishGroup->getName(),
            $dishGroup->getDescription() ?? ''
        );
    }
}
