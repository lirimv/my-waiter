<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Model;

use MyWaiter\Domain\Dish\Price as DomainPrice;

final class Price
{
    public int $gross;
    public string $currency;

    public function __construct(int $gross, string $currency)
    {
        $this->gross = $gross;
        $this->currency = $currency;
    }

    public static function fromDomain(DomainPrice $price): self
    {
        $gross = $price->getGrossPrice();

        return new self((int) $gross->getAmount(), $gross->getCurrency()->getCode());
    }
}
