<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Shop\Model;

use MyWaiter\Domain\Dish\Dish as DishEntity;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @psalm-immutable
 */
final class DishCollection
{
    /**
     * @OA\Property(type="number", example="1")
     */
    public int $amount;

    /**
     * @OA\Property(type="array", items=@OA\Items(ref=@Model(type=Dish::class)))
     */
    public array $dishes;

    public function __construct(int $amount, array $dishes)
    {
        $this->amount = $amount;
        $this->dishes = $dishes;
    }

    /**
     * @param array<int, DishEntity> $dishes
     * @psalm-param list<DishEntity> $dishes
     */
    public static function fromDishes(array $dishes): self
    {
        return new self(
            count($dishes),
            array_map(static fn (DishEntity $entity) => Dish::fromEntity($entity), $dishes)
        );
    }
}
