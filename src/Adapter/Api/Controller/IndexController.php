<?php

declare(strict_types=1);

namespace MyWaiter\Adapter\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class IndexController extends AbstractController
{
    /**
     * @Route("", name="home", methods={"GET"})
     */
    public function indexAction(): Response
    {
        return $this->render('home.html.twig');
    }
}
