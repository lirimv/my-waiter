<?php

namespace MyWaiter\Domain\Account;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use MyWaiter\Domain\Local\Local;
use MyWaiter\Domain\User\Role;
use MyWaiter\Domain\User\User;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity(repositoryClass=AccountRepositoryInterface::class)
 */
class Account implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @var Collection|Local[]
     *
     * @ORM\OneToMany(targetEntity=Local::class, mappedBy="account", orphanRemoval=true)
     */
    private iterable $locals;

    /**
     * @var Collection|User[]
     *
     * @ORM\ManyToMany(targetEntity=User::class)
     * @JoinTable(
     *     name="account_admin_user",
     *     joinColumns={@JoinColumn(name="account_id", referencedColumnName="id")},
     *     inverseJoinColumns={@JoinColumn(name="admin_id", referencedColumnName="id", unique=true)}
     * )
     */
    private iterable $admins;

    public function __construct(UuidInterface $id)
    {
        $this->id = $id;
        $this->locals = new ArrayCollection();
        $this->admins = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return Local[]
     *
     * @psalm-return list<Local>
     */
    public function getLocals(): array
    {
        return $this->locals->toArray();
    }

    public function addLocal(Local $local): void
    {
        if (!$this->locals->contains($local)) {
            $this->locals[] = $local;
            $local->setAccount($this);
        }
    }

    /**
     * @return User[]
     *
     * @psalm-return list<User>
     */
    public function getAdmins(): array
    {
        return $this->admins->toArray();
    }

    public function addAdmin(User $user): void
    {
        Assert::inArray(Role::ADMIN, $user->getRoles(), 'Only admin user can be added to account.');

        if (!$this->admins->contains($user)) {
            $this->admins->add($user);
        }
    }
}
