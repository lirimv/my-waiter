<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Account\Command;

use MyWaiter\Domain\User\Command\CreateUser;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateAccount
{
    /**
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\Valid()
     */
    private CreateUser $admin;

    public static function fromArray(array $data): self
    {
        $self = new self();
        $self->id = $data['id'] ?? Uuid::uuid4()->toString();
        $self->admin = CreateUser::fromArray($data['admin'] ?? []);

        return $self;
    }

    public function getId(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    public function getAdmin(): CreateUser
    {
        return $this->admin;
    }
}
