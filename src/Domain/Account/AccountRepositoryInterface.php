<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Account;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface AccountRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Account;

    public function save(Account $account): void;

    /**
     * @throws NotFoundException
     */
    public function getFirst(): Account;
}
