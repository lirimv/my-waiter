<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Account\Event;

use MyWaiter\Domain\Account\Account;
use Ramsey\Uuid\UuidInterface;

final class AccountCreated
{
    private UuidInterface $accountId;

    public function __construct(Account $account)
    {
        $this->accountId = $account->getId();
    }

    public function getUserId(): UuidInterface
    {
        return $this->accountId;
    }
}
