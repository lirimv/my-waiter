<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Account\Handler;

use MyWaiter\Domain\Account\Account;
use MyWaiter\Domain\Account\AccountRepositoryInterface;
use MyWaiter\Domain\Account\Command\CreateAccount;
use MyWaiter\Domain\Account\Event\AccountCreated;
use MyWaiter\Domain\User\Event\UserCreated;
use MyWaiter\Domain\User\Role;
use MyWaiter\Domain\User\User;
use MyWaiter\Domain\User\UserRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class CreateAccountHandler implements MessageHandlerInterface
{
    private AccountRepositoryInterface $accounts;
    private UserRepositoryInterface $users;
    private EncoderFactoryInterface $encoderFactory;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AccountRepositoryInterface $accounts,
        UserRepositoryInterface $users,
        EncoderFactoryInterface $encoderFactory,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->accounts = $accounts;
        $this->users = $users;
        $this->encoderFactory = $encoderFactory;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateAccount $command): void
    {
        $encoder = $this->encoderFactory->getEncoder(User::class);
        $createAdmin = $command->getAdmin();

        $admin = new User(
            $createAdmin->getId(),
            $createAdmin->getEmail(),
            $encoder->encodePassword($createAdmin->getPassword(), ''),
            $createAdmin->getPhone(),
            [Role::ADMIN]
        );

        $this->users->save($admin);

        $account = new Account($command->getId());
        $account->addAdmin($admin);

        $this->accounts->save($account);

        $this->eventDispatcher->dispatch(new UserCreated($admin));
        $this->eventDispatcher->dispatch(new AccountCreated($account));
    }
}
