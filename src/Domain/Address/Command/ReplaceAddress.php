<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Address\Command;

use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-immutable
 */
final class ReplaceAddress implements Command, IdAwareCommand
{
    /**
     * @Assert\Uuid()
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string")
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string")
     */
    private mixed $street;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     *
     * @OA\Property(type="string", nullable=true)
     */
    private mixed $streetNumber;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string")
     */
    private mixed $city;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     *
     * @OA\Property(type="string", nullable=true)
     */
    private mixed $postalCode;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     *
     * @OA\Property(type="string", nullable=true)
     */
    private mixed $state;

    /**
     * @Assert\NotBlank()
     * @Assert\Country()
     *
     * @OA\Property(type="string")
     */
    private mixed $country;

    public static function fromArray(array $data): self
    {
        $self = new self();

        $self->id = $data['id'] ?? null;
        $self->street = $data['street'] ?? null;
        $self->streetNumber = $data['streetNumber'] ?? null;
        $self->city = $data['city'] ?? null;
        $self->postalCode = $data['postalCode'] ?? null;
        $self->state = $data['state'] ?? null;
        $self->country = $data['country'] ?? null;

        return $self;
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getStreet(): string
    {
        $street = $this->street;

        \Webmozart\Assert\Assert::stringNotEmpty($street);

        return $street;
    }

    /**
     * @psalm-return non-empty-string|null
     */
    public function getStreetNumber(): ?string
    {
        $streetNumber = $this->streetNumber;

        \Webmozart\Assert\Assert::nullOrStringNotEmpty($streetNumber);

        return $streetNumber;
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getCity(): string
    {
        $city = $this->city;

        \Webmozart\Assert\Assert::stringNotEmpty($city);

        return $city;
    }

    /**
     * @psalm-return non-empty-string|null
     */
    public function getPostalCode(): ?string
    {
        $postalCode = $this->postalCode;

        \Webmozart\Assert\Assert::nullOrStringNotEmpty($postalCode);

        return $postalCode;
    }

    /**
     * @psalm-return non-empty-string|null
     */
    public function getState(): ?string
    {
        $state = $this->state;

        \Webmozart\Assert\Assert::nullOrStringNotEmpty($state);

        return $state;
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getCountry(): string
    {
        $country = $this->country;

        \Webmozart\Assert\Assert::stringNotEmpty($country);

        return $country;
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
