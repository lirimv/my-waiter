<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Address\Handler;

use MyWaiter\Domain\Address\AddressRepositoryInterface;
use MyWaiter\Domain\Address\Command\ReplaceAddress;
use MyWaiter\Domain\Address\Event\AddressReplaced;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class ReplaceAddressHandler implements MessageHandlerInterface
{
    private AddressRepositoryInterface $addressRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AddressRepositoryInterface $addressRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->addressRepository = $addressRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceAddress $command): void
    {
        $address = $this->addressRepository->get($command->id());

        $address->setStreet($command->getStreet());
        $address->setStreetNumber($command->getStreetNumber());
        $address->setCity($command->getCity());
        $address->setPostalCode($command->getPostalCode());
        $address->setState($command->getState());
        $address->setCountry($command->getCountry());

        $this->addressRepository->save($address);

        $this->eventDispatcher->dispatch(new AddressReplaced($address));
    }
}
