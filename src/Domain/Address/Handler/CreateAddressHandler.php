<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Address\Handler;

use MyWaiter\Domain\Address\Address;
use MyWaiter\Domain\Address\AddressRepositoryInterface;
use MyWaiter\Domain\Address\Command\CreateAddress;
use MyWaiter\Domain\Address\Event\AddressCreated;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class CreateAddressHandler implements MessageHandlerInterface
{
    private AddressRepositoryInterface $addressRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AddressRepositoryInterface $addressRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->addressRepository = $addressRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateAddress $command): void
    {
        $address = new Address(
            $command->getId(),
            $command->getStreet(),
            $command->getStreetNumber(),
            $command->getCity(),
            $command->getPostalCode(),
            $command->getState(),
            $command->getCountry()
        );

        $this->addressRepository->save($address);

        $this->eventDispatcher->dispatch(new AddressCreated($address));
    }
}
