<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Address;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface AddressRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Address;

    /**
     * @return Address[]
     *
     * @psalm-return list<Address>
     */
    public function getAll(): array;

    public function save(Address $address): void;

    public function delete(Address $address): void;
}
