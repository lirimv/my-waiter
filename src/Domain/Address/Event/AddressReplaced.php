<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Address\Event;

use MyWaiter\Domain\Address\Address;
use Symfony\Contracts\EventDispatcher\Event;

final class AddressReplaced extends Event
{
    private Address $address;

    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    public function getEntity(): Address
    {
        return $this->address;
    }
}
