<?php

namespace MyWaiter\Domain\Address;

use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=AddressRepositoryInterface::class)
 */
class Address implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $streetNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $postalCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $state;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $country;

    /**
     * @psalm-param non-empty-string $street
     * @psalm-param non-empty-string $streetNumber
     * @psalm-param non-empty-string $city
     * @psalm-param non-empty-string $postalCode
     * @psalm-param non-empty-string $state
     * @psalm-param non-empty-string $country
     */
    public function __construct(?UuidInterface $id, string $street, ?string $streetNumber, string $city, ?string $postalCode, ?string $state, ?string $country)
    {
        $this->id = $id ?? Uuid::uuid4();
        $this->street = $street;
        $this->streetNumber = $streetNumber;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->state = $state;
        $this->country = $country ?? 'DE';
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setStreetNumber(?string $streetNumber): void
    {
        $this->streetNumber = $streetNumber;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setState(?string $state): void
    {
        $this->state = $state;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }
}
