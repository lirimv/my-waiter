<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

use Symfony\Component\HttpFoundation\Request;

trait PagerCriteria
{
    public int $page = 1;
    private int $offset = 0;
    private int $limit = 20;

    public function pagerFromRequest(Request $request): void
    {
        $limit = (int) $request->query->get('limit', '20');
        $page = (int) $request->query->get('page', '1');
        $offset = (int) $request->query->get('offset', '0');

        $max = $limit - $offset + 1;

        $this->offset = $offset;
        $this->limit = $max;
        $this->page = $page;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }
}
