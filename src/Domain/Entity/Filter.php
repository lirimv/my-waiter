<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

/** @psalm-immutable */
final class Filter
{
    /**
     * @psalm-var array<string, mixed>
     */
    private array $filter;

    /** @psalm-param array<string, mixed> $filter */
    public function __construct(array $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return array<string, mixed>
     */
    public function all(): array
    {
        return $this->filter;
    }

    public function has(string $name): bool
    {
        return isset($this->filter[$name]);
    }

    public function get(string $name): mixed
    {
        return $this->filter[$name];
    }

    public function remove(string $name): self
    {
        $filters = $this->filter;
        unset($filters[$name]);

        return new self($filters);
    }

    public function empty(): bool
    {
        return [] === $this->filter;
    }

    /**
     * @psalm-param mixed $value
     */
    public function add(string $name, $value): self
    {
        return new self(array_merge($this->all(), [$name => $value]));
    }
}
