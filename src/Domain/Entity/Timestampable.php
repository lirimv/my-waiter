<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

use DateTimeImmutable;

trait Timestampable
{
    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $createdAt = null;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $updatedAt = null;

    public function getCreatedAt(): DateTimeImmutable
    {
        if (null === $this->createdAt) {
            $this->createdAt = new DateTimeImmutable();
        }

        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): DateTimeImmutable
    {
        if (null === $this->updatedAt) {
            $this->updatedAt = new DateTimeImmutable();
        }

        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
