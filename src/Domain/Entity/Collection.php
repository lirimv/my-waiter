<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

use Doctrine\ORM\Tools\Pagination\Paginator;

abstract class Collection
{
    private int $total;
    private int $offset;
    private int $limit;

    public function __construct(int $total, int $offset, int $limit)
    {
        $this->total = $total;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    abstract public static function fromPaginatorAndCriteria(Paginator $paginator, Criteria $criteria): self;

    /**
     * @return EntityInterface[]
     *
     * @psalm-return list<EntityInterface>
     */
    abstract public function getEntities(): array;
}
