<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

/** @psalm-immutable */
final class Sorting
{
    /**
     * @var array<string, string>
     *
     * @psalm-var array<non-empty-string, 'asc'|'desc'>
     */
    private array $orders;

    /**
     * @param array<string, string> $orders
     *
     * @psalm-param array<non-empty-string, 'asc'|'desc'> $orders
     */
    public function __construct(array $orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return array<string, string>
     *
     * @psalm-return array<non-empty-string, 'asc'|'desc'>
     */
    public function all(): array
    {
        return $this->orders;
    }

    public function empty(): bool
    {
        return [] === $this->orders;
    }

    public function has(string $name): bool
    {
        return isset($this->orders[$name]);
    }

    public function get(string $name): mixed
    {
        return $this->orders[$name] ?? null;
    }
}
