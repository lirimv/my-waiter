<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

use Ramsey\Uuid\UuidInterface;

interface EntityInterface
{
    public function getId(): UuidInterface;
}
