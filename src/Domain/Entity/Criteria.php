<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

final class Criteria
{
    use PagerCriteria;

    private Sorting $order;
    private Filter $filter;

    public function __construct(Filter $filter, Sorting $order)
    {
        $this->order = $order;
        $this->filter = $filter;
    }

    public function getFilter(): Filter
    {
        return $this->filter;
    }

    public function getOrder(): Sorting
    {
        return $this->order;
    }
}
