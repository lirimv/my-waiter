<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Entity;

use DateTimeImmutable;

interface TimestampableInterface
{
    public function getCreatedAt(): DateTimeImmutable;

    public function setCreatedAt(DateTimeImmutable $createdAt): void;

    public function getUpdatedAt(): DateTimeImmutable;

    public function setUpdatedAt(DateTimeImmutable $updatedAt): void;
}
