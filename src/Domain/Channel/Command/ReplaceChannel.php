<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel\Command;

use MyWaiter\Domain\Channel\ChannelType;
use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class ReplaceChannel implements Command, IdAwareCommand
{
    /**
     * @Assert\Uuid()
     * @Assert\NotBlank()
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Online Shop")
     */
    private mixed $name;

    /**
     * @Assert\Email())
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="test@example.tld")
     */
    private mixed $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Choice(ChannelType::VALUES)
     *
     * @OA\Property(type="string", enum=ChannelType::VALUES)
     */
    private mixed $type;

    /**
     * @Assert\Url()
     * @Assert\NotBlank(allowNull=true)
     *
     * @OA\Property(type="string", example="https://my-shop.de", nullable=true)
     */
    private mixed $baseUrl;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(
     *     type="array",
     *     items=@OA\Items(type="string", example="fbec8641-88d6-4026-84b3-471579595c32"),
     *     nullable=true
     * )
     */
    private mixed $locals;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(
     *     type="array",
     *     items=@OA\Items(type="string", example="fbec8641-88d6-4026-84b3-471579595c32"),
     *     nullable=true
     * )
     */
    private mixed $assortments;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(
     *     type="array",
     *     items=@OA\Items(type="string", example="fbec8641-88d6-4026-84b3-471579595c32"),
     *     nullable=true
     * )
     */
    private mixed $tables;

    public static function fromArray(array $data): self
    {
        $self = new self();
        $self->id = $data['id'] ?? null;
        $self->name = $data['name'] ?? null;
        $self->email = $data['email'] ?? null;
        $self->type = $data['type'] ?? null;
        $self->baseUrl = $data['baseUrl'] ?? null;
        $self->locals = $data['locals'] ?? [];
        $self->assortments = $data['assortments'] ?? [];
        $self->tables = $data['tables'] ?? [];

        return $self;
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    /**
     * @psalm-return non-empty-string
     *
     * @psalm-suppress ImpureMethodCall dunno where this error comes from
     */
    public function getEmail(): string
    {
        $email = $this->email;

        \Webmozart\Assert\Assert::stringNotEmpty($email);
        \Webmozart\Assert\Assert::email($email);

        return $email;
    }

    /**
     * @psalm-return non-empty-string|null
     */
    public function getBaseUrl(): ?string
    {
        $baseUrl = $this->baseUrl;

        \Webmozart\Assert\Assert::nullOrStringNotEmpty($baseUrl);

        return $baseUrl;
    }

    public function getType(): ChannelType
    {
        $type = $this->type;

        \Webmozart\Assert\Assert::stringNotEmpty($type);

        return ChannelType::fromString($type);
    }

    /**
     * @return UuidInterface[]
     *
     * @psalm-return list<UuidInterface>
     */
    public function getLocals(): array
    {
        $locals = $this->locals;

        \Webmozart\Assert\Assert::isList($locals);
        \Webmozart\Assert\Assert::allStringNotEmpty($locals);
        \Webmozart\Assert\Assert::allUuid($locals);

        return array_map([Uuid::class, 'fromString'], $locals);
    }

    /**
     * @return UuidInterface[]
     *
     * @psalm-return list<UuidInterface>
     */
    public function getAssortments(): array
    {
        $assortments = $this->assortments;

        \Webmozart\Assert\Assert::isList($assortments);
        \Webmozart\Assert\Assert::allStringNotEmpty($assortments);
        \Webmozart\Assert\Assert::allUuid($assortments);

        return array_map([Uuid::class, 'fromString'], $assortments);
    }

    /**
     * @return UuidInterface[]
     *
     * @psalm-return list<UuidInterface>
     */
    public function getTables(): array
    {
        $tables = $this->tables;

        \Webmozart\Assert\Assert::isList($tables);
        \Webmozart\Assert\Assert::allStringNotEmpty($tables);
        \Webmozart\Assert\Assert::allUuid($tables);

        return array_map([Uuid::class, 'fromString'], $tables);
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
