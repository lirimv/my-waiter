<?php

namespace MyWaiter\Domain\Channel\Tax;

use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Entity\EntityInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=TaxRepositoryInterface::class)
 */
class Tax implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary"))
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $rate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $label;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private string $type;

    public function __construct(int $rate, string $label, TaxType $type)
    {
        $this->rate = $rate;
        $this->label = $label;
        $this->type = (string) $type;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getRate(): int
    {
        return $this->rate;
    }

    public function setRate(int $rate): void
    {
        $this->rate = $rate;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getType(): TaxType
    {
        return TaxType::fromString($this->type);
    }

    public function setType(TaxType $type): void
    {
        $this->type = (string) $type;
    }
}
