<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel\Tax;

use MyWaiter\Domain\Channel\ChannelType;
use Webmozart\Assert\Assert;

final class TaxType
{
    private string $value;

    private const DEFAULT = 'default';
    private const REDUCED = 'reduced';

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string[]
     *
     * @psalm-return list<string>
     */
    public static function values(): array
    {
        return [self::DEFAULT, self::REDUCED];
    }

    public static function default(): self
    {
        return new self(self::DEFAULT);
    }

    public static function reduced(): self
    {
        return new self(self::REDUCED);
    }

    /**
     * @psalm-param non-empty-string
     */
    public static function fromString(string $value): self
    {
        Assert::inArray($value, self::values());

        return new self($value);
    }

    public function __toString()
    {
        return $this->value;
    }

    public function equals(ChannelType $type): bool
    {
        return $this->value === (string) $type;
    }

    /**
     * @return array<string, string>
     */
    public static function choiceValues(): array
    {
        return [
            self::DEFAULT => self::DEFAULT,
            self::REDUCED => self::REDUCED,
        ];
    }
}
