<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface ChannelRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Channel;

    /**
     * @return Channel[]
     *
     * @psalm-return list<Channel>
     */
    public function getAll(): array;

    public function save(Channel $channel): void;

    public function delete(Channel $channel): void;
}
