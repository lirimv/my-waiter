<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel;

use Webmozart\Assert\Assert;

/**
 * @psalm-immutable
 */
final class ChannelType
{
    private string $value;

    private const ONLINE = 'online';
    private const LOCALLY = 'locally';

    public const VALUES = [self::ONLINE, self::LOCALLY];
    public const CHOICE = [
        self::ONLINE => self::ONLINE,
        self::LOCALLY => self::LOCALLY,
    ];

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function online(): self
    {
        return new self(self::ONLINE);
    }

    public static function locally(): self
    {
        return new self(self::LOCALLY);
    }

    /**
     * @psalm-param non-empty-string
     */
    public static function fromString(string $value): self
    {
        Assert::oneOf($value, self::VALUES);

        return new self($value);
    }

    public function __toString()
    {
        return $this->value;
    }

    public function equals(ChannelType $type): bool
    {
        return $this->value === (string) $type;
    }
}
