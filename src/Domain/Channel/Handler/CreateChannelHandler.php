<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel\Handler;

use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Channel\ChannelRepositoryInterface;
use MyWaiter\Domain\Channel\Command\CreateChannel;
use MyWaiter\Domain\Channel\Event\ChannelCreated;
use MyWaiter\Domain\Local\Local;
use MyWaiter\Domain\Local\LocalRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateChannelHandler implements MessageHandlerInterface
{
    private ChannelRepositoryInterface $channelRepository;
    private LocalRepositoryInterface $localRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ChannelRepositoryInterface $channelRepository,
        LocalRepositoryInterface $localRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->channelRepository = $channelRepository;
        $this->localRepository = $localRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateChannel $command): void
    {
        $channel = new Channel(
            $command->getId(),
            $command->getName(),
            $command->getEmail(),
            $command->getType(),
            $command->getBaseUrl()
        );

        $channel->setLocals(array_map(function (UuidInterface $id): Local {
            return $this->localRepository->get($id);
        }, $command->getLocals()));

        $this->channelRepository->save($channel);

        $this->eventDispatcher->dispatch(new ChannelCreated($channel));
    }
}
