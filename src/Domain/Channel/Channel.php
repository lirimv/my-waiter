<?php

namespace MyWaiter\Domain\Channel;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use MyWaiter\Domain\Assortment\Assortment;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use MyWaiter\Domain\Event\EventStorableEntity;
use MyWaiter\Domain\Event\EventStoreTrait;
use MyWaiter\Domain\Local\Local;
use MyWaiter\Domain\Table\Table;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity(repositoryClass=ChannelRepositoryInterface::class)
 *
 * @ORM\AssociationOverrides({
 *      @ORM\AssociationOverride(
 *          name="events",
 *          joinColumns={@JoinColumn(name="event_id", referencedColumnName="id")},
 *          joinTable=@JoinTable(
 *              name="channel_event",
 *              joinColumns={@JoinColumn(name="channel_id", referencedColumnName="id")},
 *              inverseJoinColumns={@JoinColumn(name="event_id", referencedColumnName="id", unique=true)}
 *          )
 *      )
 * })
 */
class Channel implements EntityInterface, TimestampableInterface, EventStorableEntity
{
    use Timestampable;
    use EventStoreTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $baseUrl;

    /**
     * @var Collection|Local[]
     *
     * @ORM\ManyToMany(targetEntity=Local::class, inversedBy="channels")
     * )
     */
    private iterable $locals;

    /**
     * @var Collection|Table[]
     *
     * @ORM\OneToMany(targetEntity=Table::class, mappedBy="channel", orphanRemoval=true, cascade={"persist"})
     */
    private iterable $tables;

    /**
     * @var Collection|Assortment[]
     *
     * @ORM\ManyToMany(targetEntity=Assortment::class, mappedBy="channels", cascade={"persist"})
     */
    private iterable $assortments;

    /**
     * @psalm-param non-empty-string $name
     * @psalm-param non-empty-string $email
     * @psalm-param non-empty-string $baseUrl
     */
    public function __construct(?UuidInterface $id, string $name, string $email, ChannelType $type, ?string $baseUrl)
    {
        $this->id = $id ?? Uuid::uuid4();
        $this->name = $name;
        $this->email = $email;
        $this->type = (string) $type;
        $this->baseUrl = $baseUrl;
        $this->tables = new ArrayCollection();
        $this->locals = new ArrayCollection();
        $this->assortments = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @psalm-param non-empty-string $nemail
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getType(): ChannelType
    {
        return ChannelType::fromString($this->type);
    }

    public function setType(ChannelType $type): void
    {
        $this->type = (string) $type;
    }

    /**
     * @return Local[]
     *
     * @psalm-return list<Local>
     */
    public function getLocals(): array
    {
        return $this->locals->toArray();
    }

    public function addLocal(Local $local): void
    {
        if (!$this->locals->contains($local)) {
            $this->locals[] = $local;
        }
    }

    /**
     * @psalm-param list<Local> $locals
     */
    public function setLocals(array $locals): void
    {
        Assert::allIsInstanceOf($locals, Local::class);

        $this->locals = new ArrayCollection($locals);
    }

    public function removeLocal(Local $local): void
    {
        if ($this->locals->contains($local)) {
            $this->locals->removeElement($local);
        }
    }

    /**
     * @return Table[]
     *
     * @spalm-return list<Table>
     */
    public function getTables(): array
    {
        return $this->tables->toArray();
    }

    public function addTable(Table $table): self
    {
        if (!$this->tables->contains($table)) {
            $this->tables[] = $table;
            $table->setChannel($this);
        }

        return $this;
    }

    public function getBaseUrl(): ?string
    {
        return $this->baseUrl;
    }

    /**
     * @psalm-param non-empty-string|null
     */
    public function setBaseUrl(?string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return Assortment[]
     *
     * @psalm-return list<Assortment>
     */
    public function getAssortments(): array
    {
        return $this->assortments->toArray();
    }

    public function addAssortment(Assortment $assortment): void
    {
        if (!$this->assortments->contains($assortment)) {
            $this->assortments[] = $assortment;
            $assortment->addChannel($this);
        }
    }

    public function removeAssortment(Assortment $assortment): void
    {
        if ($this->assortments->contains($assortment)) {
            $this->assortments->removeElement($assortment);
            $assortment->removeChannel($this);
        }
    }
}
