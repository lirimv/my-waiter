<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel\Event;

use MyWaiter\Domain\Channel\Channel;
use Symfony\Contracts\EventDispatcher\Event;

final class ChannelDeleted extends Event
{
    private Channel $channel;

    public function __construct(Channel $channel)
    {
        $this->channel = $channel;
    }

    public function getEntity(): Channel
    {
        return $this->channel;
    }
}
