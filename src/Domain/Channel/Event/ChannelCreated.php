<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel\Event;

use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Channel\Dto\Channel as Dto;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Event\EventStorableEntity;

final class ChannelCreated
{
    private Channel $channel;

    public function __construct(Channel $channel)
    {
        $this->channel = $channel;
    }

    public function entity(): EntityInterface | EventStorableEntity
    {
        return $this->channel;
    }

    public function isEventStorable(): bool
    {
        return $this->channel instanceof EventStorableEntity;
    }

    public function eventName(): string
    {
        return self::class;
    }

    public function payload(): array
    {
        return (array) Dto::fromEntity($this->channel);
    }
}
