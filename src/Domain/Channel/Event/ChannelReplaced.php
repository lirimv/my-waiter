<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Channel\Event;

use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Channel\Dto\Channel as Dto;
use MyWaiter\Domain\Event\DomainEvent;
use Ramsey\Uuid\UuidInterface;

final class ChannelReplaced implements DomainEvent
{
    private UuidInterface $channel;

    /** @psalm-var class-string */
    private string $class;
    private array $payload;

    public function __construct(Channel $channel)
    {
        $this->channel = $channel->getId();
        $this->class = $channel::class;
        $this->payload = (array) Dto::fromEntity($channel);
    }

    public function getId(): UuidInterface
    {
        return $this->channel;
    }

    /**
     * @psalm-return class-string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }

    public static function getName(): string
    {
        return 'Channel Replaced';
    }
}
