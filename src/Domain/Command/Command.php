<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Command;

interface Command
{
    public static function fromArray(array $data): self;
}
