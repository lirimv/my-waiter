<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Command;

interface IdAwareCommand
{
    /**
     * @return string[]
     *
     * @psalm-return list<non-empty-string>
     */
    public static function getIds(): array;
}
