<?php

declare(strict_types=1);

namespace MyWaiter\Domain;

final class Metadata
{
    public const FILTER_TYPE_TERM = 'term';
    public const FILTER_TYPE_NESTED_TERM = 'nestedTerm';
    public const FILTER_TYPE_RANGE = 'range';
    public const FILTER_TYPE_BOOL = 'bool';

    public const FILTER_TYPES = [
        self::FILTER_TYPE_TERM,
        self::FILTER_TYPE_NESTED_TERM,
        self::FILTER_TYPE_RANGE,
        self::FILTER_TYPE_BOOL,
    ];

    public const FILTER_DATA_TYPE_STRING = 'string';
    public const FILTER_DATA_TYPE_UUID = 'uuid';
    public const FILTER_DATA_TYPE_NUMBER = 'number';

    public const FILTER_DATA_TYPES = [
        self::FILTER_DATA_TYPE_STRING,
        self::FILTER_DATA_TYPE_UUID,
        self::FILTER_DATA_TYPE_NUMBER,
    ];
}
