<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Notification\Subscriber;

use MyWaiter\Domain\Account\AccountRepositoryInterface;
use MyWaiter\Domain\Account\Event\AccountCreated;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

final class WhenAccountCreatedWelcomeAdmin implements EventSubscriberInterface
{
    private AccountRepositoryInterface $accounts;
    private NotifierInterface $notifier;

    public function __construct(
        AccountRepositoryInterface $accounts,
        NotifierInterface $notifier
    ) {
        $this->accounts = $accounts;
        $this->notifier = $notifier;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AccountCreated::class => 'welcomeAdmin',
        ];
    }

    public function welcomeAdmin(AccountCreated $event): void
    {
        $account = $this->accounts->get($event->getUserId());

        $admins = $account->getAdmins();

        $notification = new Notification('Welcome to my waiter api!', ['email']);

        foreach ($admins as $admin) {
            $notification->content(sprintf('Welcome to my waiter api %s !', $admin->getEmail()));
            $recipient = new Recipient($admin->getEmail());
            $this->notifier->send($notification, $recipient);
        }
    }
}
