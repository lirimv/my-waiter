<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Exception;

use Ramsey\Uuid\UuidInterface;

interface NotFoundException extends DomainException
{
    /**
     * @psalm-param class-string $resource
     */
    public static function byResource(string $resource): self;

    /**
     * @psalm-param class-string $resource
     */
    public static function byId(string $resource, UuidInterface $uuid): self;
}
