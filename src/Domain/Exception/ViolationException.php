<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ViolationException extends DomainException
{
    public function getViolationList(): ConstraintViolationListInterface;
}
