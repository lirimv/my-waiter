<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Exception;

interface AccessDeniedException extends DomainException
{
}
