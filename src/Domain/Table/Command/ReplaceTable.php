<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Table\Command;

use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-immutable
 */
final class ReplaceTable implements Command, IdAwareCommand
{
    /**
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="14-1")
     */
    private mixed $number;

    /**
     * @Assert\Type("numeric")
     * @Assert\PositiveOrZero()
     *
     * @OA\Property(type="string", example="6")
     */
    private mixed $seats;

    /**
     * @Assert\NotBlank()
     * @Assert\UUid
     *
     * @OA\Property(type="string", example="fbec8641-88d6-4026-84b3-471579595c32")
     */
    private mixed $channel;

    public static function fromArray(array $data): Command
    {
        $self = new self();

        $self->id = $data['id'] ?? null;
        $self->number = $data['number'] ?? null;
        $self->seats = $data['seats'] ?? null;
        $self->channel = $data['channel'] ?? null;

        return $self;
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getNumber(): string
    {
        $number = $this->number;

        \Webmozart\Assert\Assert::stringNotEmpty($number);

        return $number;
    }

    public function getSeats(): int
    {
        $seats = (int) $this->seats;

        \Webmozart\Assert\Assert::greaterThanEq($seats, 0);

        return $seats;
    }

    public function getChannel(): UuidInterface
    {
        $channel = $this->channel;

        \Webmozart\Assert\Assert::stringNotEmpty($channel);
        \Webmozart\Assert\Assert::uuid($channel);

        return Uuid::fromString($channel);
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
