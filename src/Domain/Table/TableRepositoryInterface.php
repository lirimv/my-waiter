<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Table;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface TableRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Table;

    /**
     * @return Table[]
     *
     * @psalm-return list<Table>
     */
    public function getAll(): array;

    public function save(Table $table): void;

    public function delete(Table $table): void;
}
