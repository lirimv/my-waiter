<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Table\Event;

use MyWaiter\Domain\Table\Table;

final class TableReplaced
{
    private Table $table;

    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    public function getTable(): Table
    {
        return $this->table;
    }
}
