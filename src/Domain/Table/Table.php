<?php

namespace MyWaiter\Domain\Table;

use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Channel\ChannelType;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity(repositoryClass=TableRepositoryInterface::class)
 * @ORM\Table(name="dining_table")
 */
class Table implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary"))
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $number;

    /**
     * @ORM\Column(type="smallint", options={"unsigned":true})
     */
    private int $seats;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $link;

    /**
     * @ORM\ManyToOne(targetEntity=Channel::class, inversedBy="tables")
     * @ORM\JoinColumn(nullable=false)
     */
    private Channel $channel;

    public function __construct(?UuidInterface $id, string $number, ?int $seats, Channel $channel)
    {
        Assert::true(ChannelType::locally()->equals($channel->getType()));
        Assert::nullOrGreaterThanEq($seats, 0);

        $this->id = $id ?? Uuid::uuid4();
        $this->number = $number;
        $this->seats = $seats ?? 0;
        $this->channel = $channel;

        $this->generateLink();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @psalm-param non-empty-string $Number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function getChannel(): Channel
    {
        return $this->channel;
    }

    public function setChannel(Channel $channel): void
    {
        $this->channel = $channel;

        $this->generateLink();
    }

    public function getSeats(): int
    {
        return $this->seats;
    }

    public function setSeats(int $number): void
    {
        Assert::greaterThanEq($number, 0);

        $this->seats = $number;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    private function generateLink(): void
    {
        $channel = $this->channel;

        if (null !== $channel->getBaseUrl() && (string) $channel->getType()->equals(ChannelType::locally())) {
            $this->link = sprintf('%s/channel/%s/table/%s',
                $channel->getBaseUrl(),
                $channel->getId()->toString(),
                $this->id->toString()
            );
        }
    }
}
