<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Table\Handler;

use MyWaiter\Domain\Channel\ChannelRepositoryInterface;
use MyWaiter\Domain\Table\Command\CreateTable;
use MyWaiter\Domain\Table\Event\TableCreated;
use MyWaiter\Domain\Table\Table;
use MyWaiter\Domain\Table\TableRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateTableHandler implements MessageHandlerInterface
{
    private TableRepositoryInterface $tableRepository;
    private ChannelRepositoryInterface $channelRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        TableRepositoryInterface $tableRepository,
        ChannelRepositoryInterface $channelRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->tableRepository = $tableRepository;
        $this->channelRepository = $channelRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateTable $command): void
    {
        $channel = $this->channelRepository->get($command->getChannel());

        $table = new Table(
            $command->getId(),
            $command->getNumber(),
            $command->getSeats(),
            $channel
        );

        $this->tableRepository->save($table);

        $this->eventDispatcher->dispatch(new TableCreated($table));
    }
}
