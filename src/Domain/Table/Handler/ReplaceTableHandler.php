<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Table\Handler;

use MyWaiter\Domain\Channel\ChannelRepositoryInterface;
use MyWaiter\Domain\Table\Command\ReplaceTable;
use MyWaiter\Domain\Table\Event\TableReplaced;
use MyWaiter\Domain\Table\TableRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceTableHandler implements MessageHandlerInterface
{
    private TableRepositoryInterface $tableRepository;
    private ChannelRepositoryInterface $channelRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        TableRepositoryInterface $tableRepository,
        ChannelRepositoryInterface $channelRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->tableRepository = $tableRepository;
        $this->channelRepository = $channelRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceTable $command): void
    {
        $channel = $this->channelRepository->get($command->getChannel());
        $table = $this->tableRepository->get($command->id());

        $table->setNumber($command->getNumber());
        $table->setSeats($command->getSeats());
        $table->setChannel($channel);

        $this->tableRepository->save($table);

        $this->eventDispatcher->dispatch(new TableReplaced($table));
    }
}
