<?php

namespace MyWaiter\Domain\Local;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Account\Account;
use MyWaiter\Domain\Address\Address;
use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=LocalRepositoryInterface::class)
 */
class Local implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary"))
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="locals")
     * @ORM\JoinColumn(nullable=false)
     */
    private Account $account;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, cascade={"persist", "remove"})
     */
    private Address $address;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=Channel::class, mappedBy="locals", cascade={"persist", "remove"})
     */
    private iterable $channels;

    public function __construct(?UuidInterface $id, string $name, Account $account, Address $address)
    {
        $this->id = $id ?? Uuid::uuid4();
        $this->name = $name;
        $this->account = $account;
        $this->address = $address;
        $this->channels = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @psalm-return list<Channel>
     *
     * @return Channel[]
     */
    public function getChannels(): array
    {
        return $this->channels->toArray();
    }

    public function addChannel(Channel $channel): void
    {
        if (!$this->channels->contains($channel)) {
            $this->channels[] = $channel;
            $channel->addLocal($this);
        }
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }

    public function __toString()
    {
        return $this->name;
    }
}
