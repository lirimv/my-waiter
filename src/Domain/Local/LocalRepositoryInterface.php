<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Local;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface LocalRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Local;

    /**
     * @return Local[]
     *
     * @psalm-return list<Local>
     */
    public function getAll(): array;

    public function save(Local $local): void;

    public function delete(Local $local): void;
}
