<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Local\Event;

use MyWaiter\Domain\Local\Local;
use Symfony\Contracts\EventDispatcher\Event;

final class LocalCreated extends Event
{
    private Local $local;

    public function __construct(Local $local)
    {
        $this->local = $local;
    }

    public function getEntity(): Local
    {
        return $this->local;
    }
}
