<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Local\Event;

use MyWaiter\Domain\Local\Local;

final class LocalReplaced
{
    private Local $local;

    public function __construct(Local $local)
    {
        $this->local = $local;
    }

    public function getEntity(): Local
    {
        return $this->local;
    }
}
