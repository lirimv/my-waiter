<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Local\Handler;

use MyWaiter\Domain\Account\AccountRepositoryInterface;
use MyWaiter\Domain\Address\AddressRepositoryInterface;
use MyWaiter\Domain\Local\Command\CreateLocal;
use MyWaiter\Domain\Local\Event\LocalCreated;
use MyWaiter\Domain\Local\Local;
use MyWaiter\Domain\Local\LocalRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateLocalHandler implements MessageHandlerInterface
{
    private LocalRepositoryInterface $localRepository;
    private AccountRepositoryInterface $accountRepository;
    private AddressRepositoryInterface $addressRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        LocalRepositoryInterface $localRepository,
        AccountRepositoryInterface $accountRepository,
        AddressRepositoryInterface $addressRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->localRepository = $localRepository;
        $this->accountRepository = $accountRepository;
        $this->addressRepository = $addressRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateLocal $command): void
    {
        $account = $this->accountRepository->getFirst();
        $address = $this->addressRepository->get($command->getAddress());

        $local = new Local(
            $command->getId(),
            $command->getName(),
            $account,
            $address
        );

        $this->localRepository->save($local);

        $this->eventDispatcher->dispatch(new LocalCreated($local));
    }
}
