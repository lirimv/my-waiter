<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Local\Handler;

use MyWaiter\Domain\Address\AddressRepositoryInterface;
use MyWaiter\Domain\Local\Command\ReplaceLocal;
use MyWaiter\Domain\Local\Event\LocalReplaced;
use MyWaiter\Domain\Local\LocalRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceLocalHandler implements MessageHandlerInterface
{
    private LocalRepositoryInterface $localRepository;
    private AddressRepositoryInterface $addressRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        LocalRepositoryInterface $localRepository,
        AddressRepositoryInterface $addressRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->localRepository = $localRepository;
        $this->addressRepository = $addressRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceLocal $command): void
    {
        $address = $this->addressRepository->get($command->getAddress());
        $local = $this->localRepository->get($command->id());

        $local->setName($command->getName());
        $local->setAddress($address);

        $this->localRepository->save($local);

        $this->eventDispatcher->dispatch(new LocalReplaced($local));
    }
}
