<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Local\Command;

use MyWaiter\Domain\Command\Command;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-immutable
 */
final class CreateLocal implements Command
{
    /**
     * @Assert\Uuid()
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="fbec8641-88d6-4026-84b3-471579595c32", nullable=true, description="Will be generated if null")
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string")
     */
    private mixed $name;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Uuid()
     *
     * @OA\Property(type="string")
     */
    private mixed $address;

    public static function fromArray(array $data): self
    {
        $self = new self();

        $self->id = $data['id'] ?? Uuid::uuid4()->toString();
        $self->name = $data['name'] ?? null;
        $self->address = $data['address'] ?? null;

        return $self;
    }

    public function getId(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    public function getAddress(): UuidInterface
    {
        $addressId = $this->address;

        \Webmozart\Assert\Assert::stringNotEmpty($addressId);
        \Webmozart\Assert\Assert::uuid($addressId);

        return Uuid::fromString($addressId);
    }
}
