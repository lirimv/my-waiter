<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Additive;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface AdditiveRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Additive;

    public function save(Additive $address): void;
}
