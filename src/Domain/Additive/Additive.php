<?php

namespace MyWaiter\Domain\Additive;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=AdditiveRepositoryInterface::class)
 */
class Additive implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $abbreviation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $description;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=Dish::class, mappedBy="additives")
     */
    private iterable $dishes;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=SideDish::class, mappedBy="additives")
     */
    private iterable $sideDishes;

    public function __construct(string $abbreviation, string $description)
    {
        $this->id = Uuid::uuid4();
        $this->abbreviation = $abbreviation;
        $this->description = $description;
        $this->dishes = new ArrayCollection();
        $this->sideDishes = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getAbbreviation(): string
    {
        return $this->abbreviation;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setAbbreviation(string $abbreviation): void
    {
        $this->abbreviation = $abbreviation;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @psalm-param non-empty-string
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Dish[]
     *
     * @psalm-return list<Dish>
     */
    public function getDishes(): iterable
    {
        return $this->dishes->toArray();
    }

    public function addDish(Dish $dish): void
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes[] = $dish;
        }
    }

    public function removeDish(Dish $dish): void
    {
        if ($this->dishes->contains($dish)) {
            $this->dishes->removeElement($dish);
        }
    }

    /**
     * @return SideDish[]
     *
     * @psalm-return list<SideDish>
     */
    public function getSideDishes(): array
    {
        return $this->sideDishes->toArray();
    }

    public function addSideDish(SideDish $sideDish): void
    {
        if (!$this->sideDishes->contains($sideDish)) {
            $this->sideDishes[] = $sideDish;
        }
    }

    public function removeSideDish(SideDish $sideDish): void
    {
        if ($this->sideDishes->contains($sideDish)) {
            $this->sideDishes->removeElement($sideDish);
        }
    }
}
