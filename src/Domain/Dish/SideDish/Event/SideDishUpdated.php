<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\SideDish\Event;

use MyWaiter\Domain\Dish\SideDish\SideDish;

final class SideDishUpdated
{
    private SideDish $sideDish;

    public function __construct(SideDish $sideDish)
    {
        $this->sideDish = $sideDish;
    }

    public function getEntity(): SideDish
    {
        return $this->sideDish;
    }
}
