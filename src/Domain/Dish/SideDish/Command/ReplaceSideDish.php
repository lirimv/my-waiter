<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\SideDish\Command;

use Money\Currency;
use Money\Money;
use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Intl\Currencies;
use Symfony\Component\Validator\Constraints as Assert;

final class ReplaceSideDish implements Command, IdAwareCommand
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Beilagensalat")
     */
    private mixed $name;

    /**
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @OA\Property(type="integer", default=0, nullable=true, format="cents")
     */
    private mixed $price;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     * @Assert\Currency()
     *
     * @OA\Property(default="EUR")
     */
    private mixed $currency;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc"), nullable=true)
     */
    private mixed $dishes;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc"), nullable=true)
     */
    private mixed $extras;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc"), nullable=true)
     */
    private mixed $ingredients;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc"), nullable=true)
     */
    private mixed $additives;

    public function __construct(mixed $id, mixed $name, mixed $price, mixed $currency, mixed $dishes, mixed $extras, mixed $ingredients, mixed $additives)
    {
        $this->id = $id ?? null;
        $this->name = $name ?? null;
        $this->price = $price ?? 0;
        $this->currency = $currency ?? 'EUR';
        $this->dishes = $dishes ?? [];
        $this->extras = $extras ?? [];
        $this->ingredients = $ingredients ?? [];
        $this->additives = $additives ?? [];
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'] ?? Uuid::uuid4()->toString(),
            $data['name'] ?? null,
            $data['price'] ?? 0,
            $data['currency'] ?? 'EUR',
            $data['dishes'] ?? [],
            $data['extras'] ?? [],
            $data['ingredients'] ?? [],
            $data['additives'] ?? [],
        );
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($this->id);
    }

    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    public function getPrice(): Money
    {
        $price = $this->price;
        $currency = $this->currency;

        \Webmozart\Assert\Assert::integer($price);
        \Webmozart\Assert\Assert::greaterThanEq($price, 0);
        \Webmozart\Assert\Assert::stringNotEmpty($currency);
        \Webmozart\Assert\Assert::true(Currencies::exists($currency));

        return new Money($this->price, new Currency($this->currency));
    }

    /**
     * @return UuidInterface[]
     */
    public function getDishes(): array
    {
        $dishes = $this->dishes;

        \Webmozart\Assert\Assert::isArray($dishes);
        \Webmozart\Assert\Assert::allStringNotEmpty($dishes);
        \Webmozart\Assert\Assert::allUuid($dishes);

        return array_map(static fn (string $id) => Uuid::fromString($id), $dishes);
    }

    /**
     * @return UuidInterface[]
     */
    public function getExtras(): array
    {
        $extras = $this->extras;

        \Webmozart\Assert\Assert::isArray($extras);
        \Webmozart\Assert\Assert::allStringNotEmpty($extras);
        \Webmozart\Assert\Assert::allUuid($extras);

        return array_map(static fn (string $id) => Uuid::fromString($id), $extras);
    }

    /**
     * @return UuidInterface[]
     */
    public function getIngredients(): array
    {
        $ingredients = $this->ingredients;

        \Webmozart\Assert\Assert::isArray($ingredients);
        \Webmozart\Assert\Assert::allStringNotEmpty($ingredients);
        \Webmozart\Assert\Assert::allUuid($ingredients);

        return array_map(static fn (string $id) => Uuid::fromString($id), $ingredients);
    }

    /**
     * @return UuidInterface[]
     */
    public function getAdditives(): array
    {
        $additives = $this->additives;

        \Webmozart\Assert\Assert::isArray($additives);
        \Webmozart\Assert\Assert::allStringNotEmpty($additives);
        \Webmozart\Assert\Assert::allUuid($additives);

        return array_map(static fn (string $id) => Uuid::fromString($id), $additives);
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
