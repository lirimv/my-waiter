<?php

namespace MyWaiter\Domain\Dish\SideDish;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Additive\Additive;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\Extra\Extra;
use MyWaiter\Domain\Dish\Ingredient\Ingredient;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=SideDishRepositoryInterface::class)
 */
class SideDish implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary"))
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Embedded(class=Price::class)
     */
    private Price $price;

    /**
     * @var Collection|Dish[]
     *
     * @ORM\ManyToMany(targetEntity=Dish::class, mappedBy="sideDishes", cascade={"persist", "refresh"})
     */
    private iterable $dishes;

    /**
     * @var Collection|Extra[]
     *
     * @ORM\ManyToMany(targetEntity=Extra::class, inversedBy="sideDishes", cascade={"persist", "refresh"})
     */
    private iterable $extras;

    /**
     * @var Collection|Ingredient[]
     *
     * @ORM\ManyToMany(targetEntity=Ingredient::class, inversedBy="sideDishes", cascade={"persist", "refresh"})
     */
    private iterable $ingredients;

    /**
     * @var Collection|Additive[]
     *
     * @ORM\ManyToMany(targetEntity=Additive::class, inversedBy="sideDishes", cascade={"persist", "refresh"})
     */
    private iterable $additives;

    /**
     * @psalm-param non-empty-string $name
     */
    public function __construct(UuidInterface $id, string $name, Price $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->dishes = new ArrayCollection();
        $this->extras = new ArrayCollection();
        $this->ingredients = new ArrayCollection();
        $this->additives = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): void
    {
        $this->price = $price;
    }

    /**
     * @return Dish[]
     *
     * @psalm-return list<Dish>
     */
    public function getDishes(): array
    {
        return $this->dishes->toArray();
    }

    public function addDish(Dish $dish): void
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes[] = $dish;
        }
    }

    public function removeDish(Dish $dish): void
    {
        if ($this->dishes->contains($dish)) {
            $this->dishes->removeElement($dish);
        }
    }

    /**
     * @param Dish[] $dishes
     *
     * @psalm-param list<Dish>
     */
    public function setDishes(array $dishes): void
    {
        $this->dishes = new ArrayCollection($dishes);
    }

    /**
     * @return Extra[]
     *
     * @psalm-return list<Extra>
     */
    public function getExtras(): array
    {
        return $this->extras->toArray();
    }

    public function addExtra(Extra $extra): void
    {
        if (!$this->extras->contains($extra)) {
            $this->extras[] = $extra;
            $extra->addSideDish($this);
        }
    }

    /**
     * @param Extra[] $extras
     *
     * @psalm-param list<Extra>
     */
    public function setExtras(array $extras): void
    {
        $this->extras = new ArrayCollection($extras);
    }

    public function removeExtra(Extra $extra): void
    {
        if ($this->extras->contains($extra)) {
            $this->extras->removeElement($extra);
            $extra->removeSideDish($this);
        }
    }

    /**
     * @return Ingredient[]
     *
     * @psalm-return list<Ingredient>
     */
    public function getIngredients(): array
    {
        return $this->ingredients->toArray();
    }

    public function addIngredient(Ingredient $ingredient): void
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->addSideDish($this);
        }
    }

    /**
     * @param Ingredient[] $ingredients
     *
     * @psalm-param list<Ingredient>
     */
    public function setIngredients(array $ingredients): void
    {
        $this->ingredients = new ArrayCollection($ingredients);
    }

    public function removeIngredient(Ingredient $ingredient): void
    {
        if ($this->ingredients->contains($ingredient)) {
            $this->ingredients->removeElement($ingredient);
            $ingredient->removeSideDish($this);
        }
    }

    /**
     * @return Additive[]
     *
     * @psalm-return list<Additive>
     */
    public function getAdditives(): array
    {
        return $this->additives->toArray();
    }

    public function addAdditive(Additive $additive): void
    {
        if (!$this->additives->contains($additive)) {
            $this->additives[] = $additive;
            $additive->addSideDish($this);
        }
    }

    /**
     * @param Additive[] $additives
     *
     * @psalm-param list<Additive>
     */
    public function setAdditives(array $additives): void
    {
        $this->additives = new ArrayCollection($additives);
    }

    public function removeAdditive(Additive $additive): void
    {
        if ($this->additives->contains($additive)) {
            $this->additives->removeElement($additive);
            $additive->removeSideDish($this);
        }
    }
}
