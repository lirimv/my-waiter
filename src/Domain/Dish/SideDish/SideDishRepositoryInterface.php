<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\SideDish;

use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface SideDishRepositoryInterface
{
    public function findByCriteria(Criteria $criteria): Collection;

    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): SideDish;

    public function save(SideDish $sideDish): void;

    public function delete(SideDish $sideDish): void;
}
