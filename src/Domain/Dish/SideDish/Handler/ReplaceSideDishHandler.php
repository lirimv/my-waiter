<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\SideDish\Handler;

use MyWaiter\Domain\Additive\AdditiveRepositoryInterface;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Domain\Dish\Ingredient\IngredientRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\SideDish\Command\ReplaceSideDish;
use MyWaiter\Domain\Dish\SideDish\Event\SideDishUpdated;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceSideDishHandler implements MessageHandlerInterface
{
    private EventDispatcherInterface $eventDispatcher;
    private SideDishRepositoryInterface $sideDishes;
    private DishRepositoryInterface $dishes;
    private ExtraRepositoryInterface $extras;
    private IngredientRepositoryInterface $ingredients;
    private AdditiveRepositoryInterface $additives;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        SideDishRepositoryInterface $sideDishRepository,
        DishRepositoryInterface $dishRepository,
        ExtraRepositoryInterface $extraRepository,
        IngredientRepositoryInterface $ingredientRepository,
        AdditiveRepositoryInterface $additiveRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->sideDishes = $sideDishRepository;
        $this->dishes = $dishRepository;
        $this->extras = $extraRepository;
        $this->ingredients = $ingredientRepository;
        $this->additives = $additiveRepository;
    }

    public function __invoke(ReplaceSideDish $command): void
    {
        $sideDish = $this->sideDishes->get($command->id());

        $sideDish->setName($command->getName());
        $sideDish->setPrice(new Price($command->getPrice()));

        $sideDish->setDishes(array_map(function (UuidInterface $id) {
            return $this->dishes->get($id);
        }, $command->getDishes()));

        $sideDish->setExtras(array_map(function (UuidInterface $id) {
            return $this->extras->get($id);
        }, $command->getExtras()));

        $sideDish->setIngredients(array_map(function (UuidInterface $id) {
            return $this->ingredients->get($id);
        }, $command->getIngredients()));

        $sideDish->setAdditives(array_map(function (UuidInterface $id) {
            return $this->additives->get($id);
        }, $command->getAdditives()));

        $this->sideDishes->save($sideDish);

        $this->eventDispatcher->dispatch(new SideDishUpdated($sideDish));
    }
}
