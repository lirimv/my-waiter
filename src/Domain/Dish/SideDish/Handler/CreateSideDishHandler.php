<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\SideDish\Handler;

use MyWaiter\Domain\Additive\AdditiveRepositoryInterface;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Domain\Dish\Ingredient\IngredientRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\SideDish\Command\CreateSideDish;
use MyWaiter\Domain\Dish\SideDish\Event\SideDishCreated;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateSideDishHandler implements MessageHandlerInterface
{
    private EventDispatcherInterface $eventDispatcher;
    private SideDishRepositoryInterface $sideDishRepository;
    private DishRepositoryInterface $dishRepository;
    private ExtraRepositoryInterface $extraRepository;
    private IngredientRepositoryInterface $ingredientRepository;
    private AdditiveRepositoryInterface $additiveRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        SideDishRepositoryInterface $sideDishRepository,
        DishRepositoryInterface $dishRepository,
        ExtraRepositoryInterface $extraRepository,
        IngredientRepositoryInterface $ingredientRepository,
        AdditiveRepositoryInterface $additiveRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->sideDishRepository = $sideDishRepository;
        $this->dishRepository = $dishRepository;
        $this->extraRepository = $extraRepository;
        $this->ingredientRepository = $ingredientRepository;
        $this->additiveRepository = $additiveRepository;
    }

    public function __invoke(CreateSideDish $command): void
    {
        $sideDish = new SideDish(
            $command->getId(),
            $command->getName(),
            new Price($command->getPrice())
        );

        foreach ($command->getDishes() as $dish) {
            $sideDish->addDish($this->dishRepository->get($dish));
        }

        foreach ($command->getExtras() as $extra) {
            $sideDish->addExtra($this->extraRepository->get($extra));
        }

        foreach ($command->getIngredients() as $ingredient) {
            $sideDish->addIngredient($this->ingredientRepository->get($ingredient));
        }

        foreach ($command->getAdditives() as $additive) {
            $sideDish->addAdditive($this->additiveRepository->get($additive));
        }

        $this->sideDishRepository->save($sideDish);

        $this->eventDispatcher->dispatch(new SideDishCreated($sideDish));
    }
}
