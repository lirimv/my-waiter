<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Extra;

use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface ExtraRepositoryInterface
{
    public function findByCriteria(Criteria $criteria): Collection;

    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Extra;

    public function save(Extra $extra): void;

    public function delete(Extra $extra): void;
}
