<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Extra\Event;

use MyWaiter\Domain\Dish\Extra\Extra;
use Symfony\Contracts\EventDispatcher\Event;

final class ExtraReplaced extends Event
{
    private Extra $extra;

    public function __construct(Extra $extra)
    {
        $this->extra = $extra;
    }

    public function getEntity(): Extra
    {
        return $this->extra;
    }
}
