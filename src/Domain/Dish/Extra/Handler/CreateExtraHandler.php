<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Extra\Handler;

use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Extra\Command\CreateExtra;
use MyWaiter\Domain\Dish\Extra\Event\ExtraCreated;
use MyWaiter\Domain\Dish\Extra\Extra;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateExtraHandler implements MessageHandlerInterface
{
    private ExtraRepositoryInterface $extras;
    private DishRepositoryInterface $dishes;
    private SideDishRepositoryInterface $sideDishes;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ExtraRepositoryInterface $extras,
        DishRepositoryInterface $dishes,
        SideDishRepositoryInterface $sideDishes,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->extras = $extras;
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateExtra $command): void
    {
        $extra = new Extra($command->getId(), $command->getName(), new Price($command->getPrice()));

        foreach ($command->getDishes() as $dishId) {
            $extra->addDish($this->dishes->get($dishId));
        }

        foreach ($command->getSideDishes() as $sideDishId) {
            $extra->addSideDish($this->sideDishes->get($sideDishId));
        }

        $this->extras->save($extra);

        $this->eventDispatcher->dispatch(new ExtraCreated($extra));
    }
}
