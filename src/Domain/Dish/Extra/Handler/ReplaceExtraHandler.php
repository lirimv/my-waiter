<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Extra\Handler;

use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Extra\Command\ReplaceExtra;
use MyWaiter\Domain\Dish\Extra\Event\ExtraReplaced;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceExtraHandler implements MessageHandlerInterface
{
    private ExtraRepositoryInterface $extras;
    private DishRepositoryInterface $dishes;
    private SideDishRepositoryInterface $sideDishes;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ExtraRepositoryInterface $extras,
        DishRepositoryInterface $dishes,
        SideDishRepositoryInterface $sideDishes,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->extras = $extras;
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceExtra $command): void
    {
        $extra = $this->extras->get($command->id());

        $extra->setName($command->getName());
        $extra->setPrice(new Price($command->getPrice()));

        $extra->setDishes(array_map(fn (UuidInterface $id): Dish => $this->dishes->get($id), $command->getDishes()));
        $extra->setSideDishes(array_map(fn (UuidInterface $id): SideDish => $this->sideDishes->get($id), $command->getSideDishes()));

        $this->extras->save($extra);

        $this->eventDispatcher->dispatch(new ExtraReplaced($extra));
    }
}
