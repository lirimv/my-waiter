<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Extra\Command;

use Money\Currency;
use Money\Money;
use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Intl\Currencies;
use Symfony\Component\Validator\Constraints as Assert;

final class ReplaceExtra implements Command, IdAwareCommand
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Käse")
     */
    private mixed $name;

    /**
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     *
     * @OA\Property(type="integer", default=0, nullable=true, format="cents")
     */
    private mixed $price;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Currency()
     *
     * @OA\Property(type="string", example="EUR", description="Currency ISO code", default="EUR")
     */
    private mixed $currency;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc"), nullable=true)
     */
    private mixed $dishes;

    /**
     * @Assert\All({
     *     @Assert\NotBlank(),
     *     @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc"), nullable=true)
     */
    private mixed $sideDishes;

    private function __construct(mixed $id, mixed $name, mixed $price, mixed $currency, mixed $dishes, mixed $sideDishes)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->currency = $currency;
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'] ?? null,
            $data['name'] ?? null,
            $data['price'] ?? 0,
            $data['currency'] ?? 'EUR',
            $data['dishes'] ?? [],
            $data['sideDishes'] ?? [],
        );
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    public function getPrice(): Money
    {
        $price = $this->price;
        $currency = $this->currency;

        \Webmozart\Assert\Assert::integer($price);
        \Webmozart\Assert\Assert::greaterThanEq($price, 0);
        \Webmozart\Assert\Assert::stringNotEmpty($currency);
        \Webmozart\Assert\Assert::true(Currencies::exists($currency));

        return new Money($this->price, new Currency($this->currency));
    }

    /**
     * @return UuidInterface[]
     */
    public function getDishes(): array
    {
        $dishes = $this->dishes;

        \Webmozart\Assert\Assert::isArray($dishes);
        \Webmozart\Assert\Assert::allStringNotEmpty($dishes);
        \Webmozart\Assert\Assert::allUuid($dishes);

        return array_map(static fn (string $id) => Uuid::fromString($id), $dishes);
    }

    /**
     * @return UuidInterface[]
     */
    public function getSideDishes(): array
    {
        $sideDishes = $this->sideDishes;

        \Webmozart\Assert\Assert::isArray($sideDishes);
        \Webmozart\Assert\Assert::allStringNotEmpty($sideDishes);
        \Webmozart\Assert\Assert::allUuid($sideDishes);

        return array_map(static fn (string $id) => Uuid::fromString($id), $sideDishes);
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
