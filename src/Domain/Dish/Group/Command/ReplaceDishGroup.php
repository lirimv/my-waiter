<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Group\Command;

use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class ReplaceDishGroup implements Command, IdAwareCommand
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Uuid()
     *
     * @OA\Property(type="string", nullable=true, example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc", default="auto generated")
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Pizzas")
     */
    private mixed $name;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     *
     * @OA\Property(type="string", example="Pizza Gruppe")
     */
    private mixed $description;

    private function __construct(mixed $id, mixed $name, mixed $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    public static function fromArray(array $data): Command
    {
        return new self(
            $data['id'] ?? null,
            $data['name'] ?? null,
            $data['description'] ?? null
        );
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    public function getDescription(): ?string
    {
        $description = $this->description;

        \Webmozart\Assert\Assert::nullOrStringNotEmpty($description);

        return $description;
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
