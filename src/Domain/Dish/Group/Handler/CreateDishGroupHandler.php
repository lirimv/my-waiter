<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Group\Handler;

use MyWaiter\Domain\Dish\Group\Command\CreateDishGroup;
use MyWaiter\Domain\Dish\Group\DishGroup;
use MyWaiter\Domain\Dish\Group\DishGroupRepositoryInterface;
use MyWaiter\Domain\Dish\Group\Event\DishGroupCreated;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateDishGroupHandler implements MessageHandlerInterface
{
    private DishGroupRepositoryInterface $repository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(DishGroupRepositoryInterface $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateDishGroup $command): void
    {
        $dishGroup = new DishGroup(
            $command->getId(),
            $command->getName(),
            $command->getDescription()
        );

        $this->repository->save($dishGroup);

        $this->eventDispatcher->dispatch(new DishGroupCreated($dishGroup));
    }
}
