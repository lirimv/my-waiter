<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Group\Handler;

use MyWaiter\Domain\Dish\Group\Command\ReplaceDishGroup;
use MyWaiter\Domain\Dish\Group\DishGroupRepositoryInterface;
use MyWaiter\Domain\Dish\Group\Event\DishGroupReplaced;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceDishGroupHandler implements MessageHandlerInterface
{
    private DishGroupRepositoryInterface $repository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(DishGroupRepositoryInterface $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceDishGroup $command): void
    {
        $dishGroup = $this->repository->get($command->id());
        $dishGroup->setName($command->getName());
        $dishGroup->setDescription($command->getDescription());

        $this->repository->save($dishGroup);

        $this->eventDispatcher->dispatch(new DishGroupReplaced($dishGroup));
    }
}
