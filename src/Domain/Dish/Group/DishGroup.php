<?php

namespace MyWaiter\Domain\Dish\Group;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=DishGroupRepositoryInterface::class)
 */
class DishGroup implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity=Dish::class, mappedBy="group")
     */
    private iterable $dishes;

    /**
     * @psalm-param non-empty-string $name
     * @psalm-param non-empty-string $description
     */
    public function __construct(?UuidInterface $id, string $name, ?string $description)
    {
        $this->id = $id ?? Uuid::uuid4();
        $this->name = $name;
        $this->description = $description;
        $this->dishes = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Dish[]
     *
     * @psalm-return list<Dish>
     */
    public function getDishes(): iterable
    {
        return $this->dishes->toArray();
    }

    public function addDish(Dish $dish): void
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes[] = $dish;
            $dish->setGroup($this);
        }
    }
}
