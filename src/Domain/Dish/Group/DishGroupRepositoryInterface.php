<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Group;

use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface DishGroupRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): DishGroup;

    public function findByCriteria(Criteria $criteria): Collection;

    public function save(DishGroup $address): void;

    public function delete(DishGroup $dishGroup): void;
}
