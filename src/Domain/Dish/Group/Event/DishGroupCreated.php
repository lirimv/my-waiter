<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Group\Event;

use MyWaiter\Domain\Dish\Group\DishGroup;
use Symfony\Contracts\EventDispatcher\Event;

final class DishGroupCreated extends Event
{
    private DishGroup $dishGroup;

    public function __construct(DishGroup $group)
    {
        $this->dishGroup = $group;
    }

    public function getEntity(): DishGroup
    {
        return $this->dishGroup;
    }
}
