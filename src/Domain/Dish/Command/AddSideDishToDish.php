<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Command;

use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class AddSideDishToDish implements Command, IdAwareCommand
{
    /**
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $sideDishId;

    public function __construct(mixed $dishId, mixed $sideDishId)
    {
        $this->id = $dishId;
        $this->sideDishId = $sideDishId;
    }

    public function dish(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    public function sideDish(): UuidInterface
    {
        $id = $this->sideDishId;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'] ?? null,
            $data['sideDishId'] ?? null
        );
    }

    public static function getIds(): array
    {
        return ['id', 'sideDishId'];
    }
}
