<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Command;

use Money\Currency;
use Money\Money;
use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class ReplaceDish implements Command, IdAwareCommand
{
    /**
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="12")
     */
    private mixed $number;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Pizza Salami")
     */
    private mixed $name;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Pizza belegt mit Käse und Salami")
     */
    private mixed $description;

    /**
     * @Assert\Type("numeric")
     * @Assert\PositiveOrZero()
     *
     * @OA\Property(type="number", example="1200", description="Price in CENT", default=0)
     */
    private mixed $price;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     * @Assert\Currency()
     *
     * @OA\Property(type="string", example="EUR", description="Currency ISO code", default="EUR")
     */
    private mixed $currency;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     * @Assert\Uuid()
     *
     * @OA\Property(type="string", example="fbec8641-88d6-4026-84b3-471579595c32")
     */
    private mixed $group;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *   @Assert\NotNull(),
     *   @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", nullable=true, items=@OA\Items(type="string", example="fbec8641-88d6-4026-84b3-471579595c32"))
     */
    private mixed $sideDishes;

    /**
     * @Assert\Type("array")
     * @Assert\All({
     *   @Assert\NotNull(),
     *   @Assert\Uuid()
     * })
     *
     * @OA\Property(type="array", nullable=true, items=@OA\Items(type="string", example="fbec8641-88d6-4026-84b3-471579595c32"))
     */
    private mixed $extras;

    public static function fromArray(array $data): self
    {
        $self = new self();

        $self->id = $data['id'] ?? null;
        $self->number = $data['number'] ?? null;
        $self->name = $data['name'] ?? null;
        $self->description = $data['description'] ?? null;
        $self->price = $data['price'] ?? 0;
        $self->currency = $data['currency'] ?? 'EUR';
        $self->group = $data['group'] ?? null;
        $self->sideDishes = $data['sideDishes'] ?? [];
        $self->extras = $data['extras'] ?? [];

        return $self;
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getNumber(): string
    {
        $number = $this->number;

        \Webmozart\Assert\Assert::stringNotEmpty($number);

        return $number;
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getDescription(): string
    {
        $description = $this->description;

        \Webmozart\Assert\Assert::stringNotEmpty($description);

        return $description;
    }

    public function getPrice(): Money
    {
        $price = (int) $this->price;
        $currency = $this->currency;

        \Webmozart\Assert\Assert::greaterThanEq($price, 0);
        \Webmozart\Assert\Assert::stringNotEmpty($currency);

        return new Money($this->price, new Currency($this->currency));
    }

    public function getGroup(): UuidInterface
    {
        $groupId = $this->group;

        \Webmozart\Assert\Assert::nullOrStringNotEmpty($groupId);
        \Webmozart\Assert\Assert::nullOrUuid($groupId);

        return Uuid::fromString($groupId);
    }

    /**
     * @return UuidInterface[]
     *
     * @psalm-return list<UuidInterface>
     */
    public function getSideDishes(): array
    {
        $sideDishes = $this->sideDishes;

        \Webmozart\Assert\Assert::isArray($sideDishes);
        \Webmozart\Assert\Assert::allStringNotEmpty($sideDishes);
        \Webmozart\Assert\Assert::allUuid($sideDishes);

        return array_map(static fn (string $id): UuidInterface => Uuid::fromString($id), $sideDishes);
    }

    /**
     * @return UuidInterface[]
     *
     * @psalm-return list<UuidInterface>
     */
    public function getExtras(): array
    {
        $extras = $this->extras;

        \Webmozart\Assert\Assert::isArray($extras);
        \Webmozart\Assert\Assert::allStringNotEmpty($extras);
        \Webmozart\Assert\Assert::allUuid($extras);

        return array_map(static fn (string $id): UuidInterface => Uuid::fromString($id), $extras);
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
