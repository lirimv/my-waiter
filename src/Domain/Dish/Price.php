<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish;

use Doctrine\ORM\Mapping as ORM;
use Money\Money;

/**
 * @psalm-immutable
 *
 * @ORM\Embeddable()
 */
final class Price
{
    /**
     * @ORM\Embedded(class=Money::class, columnPrefix="false", columnPrefix="net_")
     */
    private Money $grossPrice;

    public function __construct(Money $price)
    {
        $this->grossPrice = $price;
    }

    public function getGrossPrice(): Money
    {
        return $this->grossPrice;
    }
}
