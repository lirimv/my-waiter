<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish;

use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface DishRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Dish;

    public function save(Dish $dish): void;

    public function delete(Dish $dish): void;

    public function findByCriteria(Criteria $criteria): Collection;
}
