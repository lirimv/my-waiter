<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Variation\Event;

use MyWaiter\Domain\Dish\Variation\DishVariation;

final class DishVariationReplaced
{
    private DishVariation $variation;

    public function __construct(DishVariation $variation)
    {
        $this->variation = $variation;
    }

    public function getEntity(): DishVariation
    {
        return $this->variation;
    }
}
