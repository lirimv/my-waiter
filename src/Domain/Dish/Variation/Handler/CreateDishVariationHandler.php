<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Variation\Handler;

use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\Variation\Command\CreateDishVariation;
use MyWaiter\Domain\Dish\Variation\DishVariation;
use MyWaiter\Domain\Dish\Variation\DishVariationRepositoryInterface;
use MyWaiter\Domain\Dish\Variation\Event\DishVariationCreated;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateDishVariationHandler implements MessageHandlerInterface
{
    private DishVariationRepositoryInterface $dishVariationRepository;
    private DishRepositoryInterface $dishRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishVariationRepositoryInterface $dishVariationRepository,
        DishRepositoryInterface $dishRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->dishVariationRepository = $dishVariationRepository;
        $this->dishRepository = $dishRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateDishVariation $command): void
    {
        $dish = $this->dishRepository->get($command->getDish());

        $dishVariation = new DishVariation(
            $command->getId(),
            $command->getName(),
            $command->getType(),
            new Price($command->getPrice()),
            $dish
        );

        $this->dishVariationRepository->save($dishVariation);

        $this->eventDispatcher->dispatch(new DishVariationCreated($dishVariation));
    }
}
