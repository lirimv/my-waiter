<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Variation\Handler;

use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\Variation\Command\ReplaceDishVariation;
use MyWaiter\Domain\Dish\Variation\DishVariationRepositoryInterface;
use MyWaiter\Domain\Dish\Variation\Event\DishVariationReplaced;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceDishVariationHandler implements MessageHandlerInterface
{
    private DishVariationRepositoryInterface $dishVariationRepository;
    private DishRepositoryInterface $dishRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishVariationRepositoryInterface $dishVariationRepository,
        DishRepositoryInterface $dishRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->dishVariationRepository = $dishVariationRepository;
        $this->dishRepository = $dishRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceDishVariation $command): void
    {
        $dish = $this->dishRepository->get($command->getDish());
        $dishVariation = $this->dishVariationRepository->get($command->id());

        $dishVariation->setName($command->getName());
        $dishVariation->setType($command->getType());
        $dishVariation->setPrice(new Price($command->getPrice()));
        $dishVariation->setDish($dish);

        $this->dishVariationRepository->save($dishVariation);

        $this->eventDispatcher->dispatch(new DishVariationReplaced($dishVariation));
    }
}
