<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Variation;

use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface DishVariationRepositoryInterface
{
    public function findByCriteria(Criteria $criteria): Collection;

    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): DishVariation;

    public function save(DishVariation $variation): void;

    public function delete(DishVariation $variation): void;
}
