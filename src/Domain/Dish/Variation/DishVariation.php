<?php

namespace MyWaiter\Domain\Dish\Variation;

use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=DishVariationRepositoryInterface::class)
 */
class DishVariation implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $type;

    /**
     * @ORM\Embedded(class=Price::class)
     */
    private Price $price;

    /**
     * @ORM\ManyToOne(targetEntity=Dish::class, inversedBy="variations", cascade="persist")
     */
    private Dish $dish;

    /**
     * @psalm-param non-empty-string $name
     */
    public function __construct(UuidInterface $id, string $name, DishVariationType $type, Price $price, Dish $dish)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = (string) $type;
        $this->price = $price;
        $this->dish = $dish;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getType(): DishVariationType
    {
        return DishVariationType::fromString($this->type);
    }

    public function setType(DishVariationType $type): void
    {
        $this->type = (string) $type;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): void
    {
        $this->price = $price;
    }

    public function getDish(): Dish
    {
        return $this->dish;
    }

    public function setDish(Dish $dish): void
    {
        $this->dish = $dish;
    }
}
