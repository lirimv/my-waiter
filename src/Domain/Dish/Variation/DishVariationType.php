<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Variation;

use Webmozart\Assert\Assert;

final class DishVariationType
{
    private string $value;

    private const PORTION = 'portion';
    private const VARIATION = 'variation';

    public const VALUES = [self::PORTION, self::VARIATION];

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string[]
     *
     * @psalm-return list<string>
     */
    public static function values(): array
    {
        return [self::PORTION, self::VARIATION];
    }

    public static function variation(): self
    {
        return new self(self::VARIATION);
    }

    public static function portion(): self
    {
        return new self(self::PORTION);
    }

    /**
     * @psalm-param non-empty-string $value
     */
    public static function fromString(string $value): self
    {
        Assert::inArray($value, self::values());

        return new self($value);
    }

    public function __toString()
    {
        return $this->value;
    }

    public function equals(self $type): bool
    {
        return $this->value === (string) $type;
    }
}
