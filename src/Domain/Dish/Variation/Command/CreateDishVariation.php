<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Variation\Command;

use Money\Currency;
use Money\Money;
use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Dish\Variation\DishVariationType;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Intl\Currencies;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateDishVariation implements Command
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Uuid()
     *
     * @OA\Property(type="string", nullable=true, example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc", default="auto generated")
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Kleine Portion")
     */
    private mixed $name;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Choice(DishVariationType::VALUES)
     *
     * @OA\Property(type="string", example="portion", enum=DishVariationType::VALUES)
     */
    private mixed $type;

    /**
     * @Assert\Type("numeric")
     * @Assert\PositiveOrZero()
     *
     * @OA\Property(type="number", example="1200", description="Price in CENT", default=0, nullable=true)
     */
    private mixed $price;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Currency()
     *
     * @OA\Property(type="string", example="EUR", description="Currency ISO code", default="EUR", nullable=true)
     */
    private mixed $currency;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     * @Assert\Uuid()
     *
     * @OA\Property(type="string", example="dcb1866c-b54e-4e45-b883-98aa8ac4d7dc")
     */
    private mixed $dish;

    private function __construct(mixed $id, mixed $name, mixed $type, mixed $price, mixed $currency, mixed $dish)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->price = $price;
        $this->currency = $currency;
        $this->dish = $dish;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'] ?? Uuid::uuid4()->toString(),
            $data['name'] ?? null,
            $data['type'] ?? null,
            $data['price'] ?? 0,
            $data['currency'] ?? 'EUR',
            $data['dish'] ?? null,
        );
    }

    public function getId(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    public function getPrice(): Money
    {
        $price = $this->price;
        $currency = $this->currency;

        \Webmozart\Assert\Assert::integer($price);
        \Webmozart\Assert\Assert::greaterThanEq($price, 0);
        \Webmozart\Assert\Assert::stringNotEmpty($currency);
        \Webmozart\Assert\Assert::true(Currencies::exists($currency));

        return new Money($this->price, new Currency($this->currency));
    }

    public function getType(): DishVariationType
    {
        $type = $this->type;

        \Webmozart\Assert\Assert::inArray($type, DishVariationType::values());

        return DishVariationType::fromString($type);
    }

    public function getDish(): UuidInterface
    {
        $dishId = $this->dish;

        \Webmozart\Assert\Assert::stringNotEmpty($dishId);
        \Webmozart\Assert\Assert::uuid($dishId);

        return Uuid::fromString($dishId);
    }
}
