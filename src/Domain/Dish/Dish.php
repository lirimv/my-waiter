<?php

namespace MyWaiter\Domain\Dish;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Additive\Additive;
use MyWaiter\Domain\Dish\Extra\Extra;
use MyWaiter\Domain\Dish\Group\DishGroup;
use MyWaiter\Domain\Dish\Ingredient\Ingredient;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\Variation\DishVariation;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=DishRepositoryInterface::class)
 */
class Dish implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="text")
     */
    private string $description;

    /**
     * @ORM\Embedded(class=Price::class)
     */
    private Price $price;

    /**
     * @ORM\ManyToOne(targetEntity=DishGroup::class, inversedBy="dishes")
     */
    private DishGroup $group;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=SideDish::class, inversedBy="dishes", cascade={"persist", "refresh"})
     */
    private iterable $sideDishes;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity=DishVariation::class, mappedBy="dish", cascade={"persist", "remove"})
     */
    private iterable $variations;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=Extra::class, inversedBy="dishes", cascade={"persist", "refresh"})
     */
    private iterable $extras;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=Ingredient::class, inversedBy="dishes", cascade={"persist", "refresh"})
     */
    private iterable $ingredients;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=Additive::class, inversedBy="dishes", cascade={"persist", "refresh"})
     */
    private iterable $additives;

    /**
     * @psalm-param non-empty-string $name
     * @psalm-param non-empty-string $number
     * @psalm-param non-empty-string $description
     */
    public function __construct(UuidInterface $id, string $name, string $number, string $description, Price $price, DishGroup $group)
    {
        $this->id = $id;
        $this->name = $name;
        $this->number = $number;
        $this->description = $description;
        $this->price = $price;
        $this->group = $group;
        $this->sideDishes = new ArrayCollection();
        $this->variations = new ArrayCollection();
        $this->extras = new ArrayCollection();
        $this->ingredients = new ArrayCollection();
        $this->additives = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @psalm-param non-empty-string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @psalm-param non-empty-string $description
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): void
    {
        $this->price = $price;
    }

    public function getGroup(): DishGroup
    {
        return $this->group;
    }

    public function setGroup(DishGroup $group): void
    {
        $this->group = $group;
    }

    /**
     * @return SideDish[]
     */
    public function getSideDishes(): array
    {
        return $this->sideDishes->toArray();
    }

    /**
     * @param SideDish[] $sideDishes
     *
     * @psalm-param list<SideDish>
     */
    public function setSideDishes(array $sideDishes): void
    {
        $this->sideDishes = new ArrayCollection($sideDishes);
    }

    public function addSideDish(SideDish $sideDish): void
    {
        if (false === $this->sideDishes->contains($sideDish)) {
            $this->sideDishes->add($sideDish);
            $sideDish->addDish($this);
        }
    }

    public function removeSideDish(SideDish $sideDish): void
    {
        if (true === $this->sideDishes->contains($sideDish)) {
            $this->sideDishes->removeElement($sideDish);
            $sideDish->removeDish($this);
        }
    }

    /**
     * @return DishVariation[]
     *
     * @psalm-return list<DishVariation>
     */
    public function getVariations(): array
    {
        return $this->variations->toArray();
    }

    public function addVariation(DishVariation $variation): void
    {
        if (!$this->variations->contains($variation)) {
            $this->variations[] = $variation;
            $variation->setDish($this);
        }
    }

    /**
     * @return Extra[]
     *
     * @psalm-return list<Extra>
     */
    public function getExtras(): array
    {
        return $this->extras->toArray();
    }

    /**
     * @param Extra[] $extras
     *
     * @psalm-param list<Extra>
     */
    public function setExtras(array $extras): void
    {
        $this->extras = new ArrayCollection($extras);
    }

    public function addExtra(Extra $extra): void
    {
        if (!$this->extras->contains($extra)) {
            $this->extras[] = $extra;
            $extra->addDish($this);
        }
    }

    public function removeExtra(Extra $extra): void
    {
        if ($this->extras->contains($extra)) {
            $this->extras->removeElement($extra);
            $extra->removeDish($this);
        }
    }

    /**
     * @return Ingredient[]
     *
     * @psalm-return list<Ingredient>
     */
    public function getIngredients(): iterable
    {
        return $this->ingredients->toArray();
    }

    public function addIngredient(Ingredient $ingredient): void
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->addDish($this);
        }
    }

    public function removeIngredient(Ingredient $ingredient): void
    {
        if ($this->ingredients->contains($ingredient)) {
            $this->ingredients->removeElement($ingredient);
            $ingredient->removeDish($this);
        }
    }

    /**
     * @return Additive[]
     *
     * @psalm-return list<Additive>
     */
    public function getAdditives(): iterable
    {
        return $this->additives->toArray();
    }

    public function addAdditive(Additive $additive): void
    {
        if (!$this->additives->contains($additive)) {
            $this->additives[] = $additive;
            $additive->addDish($this);
        }
    }

    public function removeAdditive(Additive $additive): void
    {
        if ($this->additives->contains($additive)) {
            $this->additives->removeElement($additive);
            $additive->removeDish($this);
        }
    }
}
