<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Ingredient;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface IngredientRepositoryInterface
{
    /**
     * @return Ingredient[]
     *
     * @psalm-return list<Ingredient>
     */
    public function getAll(): array;

    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Ingredient;

    public function save(Ingredient $ingredient): void;

    public function delete(Ingredient $ingredient): void;
}
