<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Ingredient\Event;

use MyWaiter\Domain\Dish\Ingredient\Ingredient;

final class IngredientDeleted
{
    private Ingredient $ingredient;

    public function __construct(Ingredient $ingredient)
    {
        $this->ingredient = $ingredient;
    }

    public function getEntity(): Ingredient
    {
        return $this->ingredient;
    }
}
