<?php

namespace MyWaiter\Domain\Dish\Ingredient;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=IngredientRepositoryInterface::class)
 */
class Ingredient implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary"))
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=Dish::class, mappedBy="ingredients", fetch="EXTRA_LAZY")
     */
    private iterable $dishes;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity=SideDish::class, mappedBy="ingredients", fetch="EXTRA_LAZY")
     */
    private iterable $sideDishes;

    /**
     * @psalm-param non-empty-string $name
     */
    public function __construct(?UuidInterface $id, string $name)
    {
        $this->id = $id ?? Uuid::uuid4();
        $this->name = $name;
        $this->dishes = new ArrayCollection();
        $this->sideDishes = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Dish[]
     *
     * @psalm-return list<Dish>
     */
    public function getDishes(): array
    {
        return $this->dishes->toArray();
    }

    public function addDish(Dish $dish): void
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes[] = $dish;
        }
    }

    /**
     * @param Dish[] $dishes
     *
     * @psalm-param list<Dish>
     */
    public function setDishes(array $dishes): void
    {
        $this->dishes = new ArrayCollection($dishes);
    }

    public function removeDish(Dish $dish): void
    {
        if ($this->dishes->contains($dish)) {
            $this->dishes->removeElement($dish);
        }
    }

    /**
     * @return SideDish[]
     *
     * @psalm-return list<SideDish>
     */
    public function getSideDishes(): array
    {
        return $this->sideDishes->toArray();
    }

    public function addSideDish(SideDish $sideDish): void
    {
        if (!$this->sideDishes->contains($sideDish)) {
            $this->sideDishes[] = $sideDish;
        }
    }

    /**
     * @param SideDish[] $sideDishes
     *
     * @psalm-param list<SideDish>
     */
    public function setSideDishes(array $sideDishes): void
    {
        $this->sideDishes = new ArrayCollection($sideDishes);
    }

    public function removeSideDish(SideDish $sideDish): void
    {
        if ($this->sideDishes->contains($sideDish)) {
            $this->sideDishes->removeElement($sideDish);
        }
    }
}
