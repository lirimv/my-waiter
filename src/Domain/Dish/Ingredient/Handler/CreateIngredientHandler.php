<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Ingredient\Handler;

use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Ingredient\Command\CreateIngredient;
use MyWaiter\Domain\Dish\Ingredient\Event\IngredientCreated;
use MyWaiter\Domain\Dish\Ingredient\Ingredient;
use MyWaiter\Domain\Dish\Ingredient\IngredientRepositoryInterface;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateIngredientHandler implements MessageHandlerInterface
{
    private IngredientRepositoryInterface $ingredients;
    private DishRepositoryInterface $dishes;
    private SideDishRepositoryInterface $sideDishes;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        IngredientRepositoryInterface $ingredients,
        DishRepositoryInterface $dishes,
        SideDishRepositoryInterface $sideDishes,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->ingredients = $ingredients;
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateIngredient $command): void
    {
        $ingredient = new Ingredient($command->getId(), $command->getName());

        foreach ($command->getDishes() as $dishId) {
            $ingredient->addDish($this->dishes->get($dishId));
        }

        foreach ($command->getSideDishes() as $sideDishId) {
            $ingredient->addSideDish($this->sideDishes->get($sideDishId));
        }

        $this->ingredients->save($ingredient);

        $this->eventDispatcher->dispatch(new IngredientCreated($ingredient));
    }
}
