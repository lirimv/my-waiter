<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Ingredient\Handler;

use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Ingredient\Command\ReplaceIngredient;
use MyWaiter\Domain\Dish\Ingredient\Event\IngredientReplaced;
use MyWaiter\Domain\Dish\Ingredient\IngredientRepositoryInterface;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceIngredientHandler implements MessageHandlerInterface
{
    private IngredientRepositoryInterface $ingredients;
    private DishRepositoryInterface $dishes;
    private SideDishRepositoryInterface $sideDishes;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        IngredientRepositoryInterface $ingredients,
        DishRepositoryInterface $dishes,
        SideDishRepositoryInterface $sideDishes,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->ingredients = $ingredients;
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceIngredient $command): void
    {
        $ingredient = $this->ingredients->get($command->id());
        $ingredient->setName($command->getName());

        $ingredient->setDishes(array_map(fn (UuidInterface $id): Dish => $this->dishes->get($id), $command->getDishes()));
        $ingredient->setSideDishes(array_map(fn (UuidInterface $id): SideDish => $this->sideDishes->get($id), $command->getSideDishes()));

        $this->ingredients->save($ingredient);

        $this->eventDispatcher->dispatch(new IngredientReplaced($ingredient));
    }
}
