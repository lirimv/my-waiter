<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish;

use Doctrine\ORM\Tools\Pagination\Paginator;
use MyWaiter\Domain\Entity\Collection as BaseCollection;
use MyWaiter\Domain\Entity\Criteria;

final class Collection extends BaseCollection
{
    /**
     * @var Dish[]
     *
     * @psalm-var list<Dish>
     */
    private array $entities;

    public function __construct(int $total, int $offset, int $limit, array $entities)
    {
        parent::__construct($total, $offset, $limit);

        $this->entities = $entities;
    }

    public static function fromPaginatorAndCriteria(Paginator $paginator, Criteria $criteria): self
    {
        return new self(
            $paginator->count(),
            $criteria->getOffset(),
            $criteria->getLimit(),
            iterator_to_array($paginator),
        );
    }

    public function getEntities(): array
    {
        return $this->entities;
    }
}
