<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Event;

use MyWaiter\Domain\Dish\Dish;
use Symfony\Contracts\EventDispatcher\Event;

final class DishDeleted extends Event
{
    private Dish $dish;

    public function __construct(Dish $dish)
    {
        $this->dish = $dish;
    }

    public function getEntity(): Dish
    {
        return $this->dish;
    }
}
