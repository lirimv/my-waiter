<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Event;

use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use Ramsey\Uuid\UuidInterface;
use Symfony\Contracts\EventDispatcher\Event;

final class SideDishAddedToDish extends Event
{
    private UuidInterface $dishId;
    private UuidInterface $sideDishId;

    public function __construct(Dish $dish, SideDish $sideDish)
    {
        $this->dishId = $dish->getId();
        $this->sideDishId = $sideDish->getId();
    }

    public function getDishId(): UuidInterface
    {
        return $this->dishId;
    }

    public function getSideDishId(): UuidInterface
    {
        return $this->sideDishId;
    }
}
