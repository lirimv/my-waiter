<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Handler;

use MyWaiter\Domain\Dish\Command\ReplaceDish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Event\DishReplaced;
use MyWaiter\Domain\Dish\Extra\Extra;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Domain\Dish\Group\DishGroupRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceDishHandler implements MessageHandlerInterface
{
    private DishRepositoryInterface $dishRepository;
    private DishGroupRepositoryInterface $dishGroupRepository;
    private SideDishRepositoryInterface $sideDishes;
    private ExtraRepositoryInterface $extras;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishRepositoryInterface $dishRepository,
        DishGroupRepositoryInterface $dishGroupRepository,
        SideDishRepositoryInterface $sideDishes,
        ExtraRepositoryInterface $extras,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->dishRepository = $dishRepository;
        $this->dishGroupRepository = $dishGroupRepository;
        $this->sideDishes = $sideDishes;
        $this->extras = $extras;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceDish $command): void
    {
        $dish = $this->dishRepository->get($command->id());
        $group = $this->dishGroupRepository->get($command->getGroup());

        $dish->setNumber($command->getNumber());
        $dish->setName($command->getName());
        $dish->setDescription($command->getDescription());
        $dish->setPrice(new Price($command->getPrice()));
        $dish->setGroup($group);

        $dish->setSideDishes(array_map(fn (UuidInterface $id): SideDish => $this->sideDishes->get($id), $command->getSideDishes()));
        $dish->setExtras(array_map(fn (UuidInterface $id): Extra => $this->extras->get($id), $command->getExtras()));

        $this->dishRepository->save($dish);

        $this->eventDispatcher->dispatch(new DishReplaced($dish));
    }
}
