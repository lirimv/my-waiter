<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Handler;

use MyWaiter\Domain\Dish\Command\AddSideDishToDish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Event\SideDishAddedToDish;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddSideDishToDishHandler implements MessageHandlerInterface
{
    private DishRepositoryInterface $dishes;
    private SideDishRepositoryInterface $sideDishes;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishRepositoryInterface $dishes,
        SideDishRepositoryInterface $sideDishes,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->dishes = $dishes;
        $this->sideDishes = $sideDishes;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(AddSideDishToDish $command): void
    {
        $dish = $this->dishes->get($command->dish());
        $sideDish = $this->sideDishes->get($command->sideDish());

        $dish->addSideDish($sideDish);

        $this->dishes->save($dish);

        $this->eventDispatcher->dispatch(new SideDishAddedToDish($dish, $sideDish));
    }
}
