<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Dish\Handler;

use MyWaiter\Domain\Dish\Command\CreateDish;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Dish\Event\DishCreated;
use MyWaiter\Domain\Dish\Extra\Extra;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Domain\Dish\Group\DishGroupRepositoryInterface;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateDishHandler implements MessageHandlerInterface
{
    private DishRepositoryInterface $dishRepository;
    private DishGroupRepositoryInterface $dishGroupRepository;
    private SideDishRepositoryInterface $sideDishes;
    private ExtraRepositoryInterface $extras;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DishRepositoryInterface $dishRepository,
        DishGroupRepositoryInterface $dishGroupRepository,
        SideDishRepositoryInterface $sideDishes,
        ExtraRepositoryInterface $extras,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->dishRepository = $dishRepository;
        $this->dishGroupRepository = $dishGroupRepository;
        $this->sideDishes = $sideDishes;
        $this->extras = $extras;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateDish $command): void
    {
        $group = $this->dishGroupRepository->get($command->getGroup());

        $dish = new Dish(
            $command->getId(),
            $command->getName(),
            $command->getNumber(),
            $command->getDescription(),
            new Price($command->getPrice()),
            $group
        );

        $dish->setSideDishes(array_map(fn (UuidInterface $id): SideDish => $this->sideDishes->get($id), $command->getSideDishes()));
        $dish->setExtras(array_map(fn (UuidInterface $id): Extra => $this->extras->get($id), $command->getExtras()));

        $this->dishRepository->save($dish);

        $this->eventDispatcher->dispatch(new DishCreated($dish));
    }
}
