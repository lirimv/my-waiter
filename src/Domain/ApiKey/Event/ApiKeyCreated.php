<?php

declare(strict_types=1);

namespace MyWaiter\Domain\ApiKey\Event;

use MyWaiter\Domain\ApiKey\ApiKey;
use Ramsey\Uuid\UuidInterface;

/**
 * @psalm-immutable
 */
final class ApiKeyCreated
{
    public UuidInterface $id;

    public function __construct(ApiKey $apiKey)
    {
        $this->id = $apiKey->getId();
    }
}
