<?php

declare(strict_types=1);

namespace MyWaiter\Domain\ApiKey\Command;

use MyWaiter\Domain\ApiKey\ApiKey;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-immutable
 */
final class CreateApiKey
{
    private const TYPES = [ApiKey::ADMIN, ApiKey::SHOP];

    /**
     * @Assert\Type("string")
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private mixed $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private mixed $key;

    /**
     * @Assert\NotBlank()
     * @Assert\Choice(
     *     callback={"MyWaiter\Domain\ApiKey\ApiKey", "getAllowedApiKeyTypes"},
     *     message="Not allowed api key type"
     * )
     */
    private mixed $type;

    public function getId(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    public function getName(): string
    {
        \Webmozart\Assert\Assert::stringNotEmpty($this->name);

        return $this->name;
    }

    public function getKey(): string
    {
        \Webmozart\Assert\Assert::stringNotEmpty($this->key);

        return $this->key;
    }

    public function getType(): string
    {
        \Webmozart\Assert\Assert::stringNotEmpty($this->type);
        \Webmozart\Assert\Assert::oneOf($this->type, ApiKey::getAllowedApiKeyTypes());

        return $this->type;
    }

    private function __construct()
    {
    }

    public static function fromArray(array $data): self
    {
        $self = new self();
        $self->id = $data['id'] ?? Uuid::uuid4()->toString();
        $self->name = $data['name'] ?? '';
        $self->type = $data['type'] ?? '';
        $self->key = $data['key'] ?? sha1(Uuid::uuid4()->toString());

        return $self;
    }
}
