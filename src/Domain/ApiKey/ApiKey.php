<?php

declare(strict_types=1);

namespace MyWaiter\Domain\ApiKey;

use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Entity\EntityInterface;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity(repositoryClass=ApiKeyRepositoryInterface::class)
 */
class ApiKey implements EntityInterface
{
    public const ADMIN = 'admin';
    public const SHOP = 'shop';

    private const ALLOWED_API_KEY_TYPES = [
        self::SHOP,
        self::ADMIN,
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $value;

    /**
     * @ORM\Column(type="string")
     */
    private string $type;

    public function __construct(UuidInterface $id, string $name, string $value, string $type)
    {
        Assert::oneOf($type, self::getAllowedApiKeyTypes());

        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->type = $type;
    }

    /**
     * @return string[]
     *
     * @psalm-return list<non-empty-string>
     * @psalm-pure
     */
    public static function getAllowedApiKeyTypes(): array
    {
        return self::ALLOWED_API_KEY_TYPES;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
