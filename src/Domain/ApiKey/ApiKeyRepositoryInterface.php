<?php

declare(strict_types=1);

namespace MyWaiter\Domain\ApiKey;

use Ramsey\Uuid\UuidInterface;

interface ApiKeyRepositoryInterface
{
    public function get(UuidInterface $id): ApiKey;

    /**
     * @return ApiKey[]
     *
     * @psalm-return list<ApiKey>
     */
    public function getAll(): array;

    public function save(ApiKey $key): void;

    public function delete(ApiKey $key): void;

    public function findByApiKey(string $apiKey): ?ApiKey;
}
