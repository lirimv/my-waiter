<?php

declare(strict_types=1);

namespace MyWaiter\Domain\ApiKey\Handler;

use MyWaiter\Domain\ApiKey\ApiKey;
use MyWaiter\Domain\ApiKey\ApiKeyRepositoryInterface;
use MyWaiter\Domain\ApiKey\Command\CreateApiKey;
use MyWaiter\Domain\ApiKey\Event\ApiKeyCreated;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateApiKeyHandler implements MessageHandlerInterface
{
    private ApiKeyRepositoryInterface $repository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ApiKeyRepositoryInterface $repository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateApiKey $command): void
    {
        $apiKey = new ApiKey(
            $command->getId(),
            $command->getName(),
            $command->getKey(),
            $command->getType()
        );

        $this->repository->save($apiKey);

        $this->eventDispatcher->dispatch(new ApiKeyCreated($apiKey));
    }
}
