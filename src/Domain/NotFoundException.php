<?php

declare(strict_types=1);

namespace MyWaiter\Domain;

use Ramsey\Uuid\UuidInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

final class NotFoundException extends RuntimeException implements Exception\NotFoundException
{
    private function __construct(string $message)
    {
        parent::__construct($message, Response::HTTP_NOT_FOUND);
    }

    public static function byResource(string $resource): self
    {
        return new self(sprintf('Resource "%s" not found.', $resource));
    }

    public static function byId(string $resource, UuidInterface $uuid): self
    {
        return new self(sprintf('Resource "%s" with id "%s" not found.', $resource, $uuid->toString()));
    }
}
