<?php

declare(strict_types=1);

namespace MyWaiter\Domain\User\Command;

use MyWaiter\Domain\User\Role;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateUser
{
    /**
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    private mixed $id;

    /**
     * @Assert\Email()
     */
    private mixed $email;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=true)
     */
    private mixed $phone;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=8)
     */
    private mixed $password;

    /**
     * @Assert\Count(min=1)
     * @Assert\All(
     *     @Assert\Choice(
     *     callback={"MyWaiter\Domain\User\Role", "roles"},
     *     message="Not allowed role."
     *     )
     * )
     */
    private mixed $roles;

    private function __construct(mixed $id, mixed $email, mixed $phone, mixed $password, mixed $roles)
    {
        $this->id = $id;
        $this->email = $email;
        $this->phone = $phone;
        $this->password = $password;
        $this->roles = $roles;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'] ?? Uuid::uuid4()->toString(),
            $data['email'] ?? '',
            $data['phone'] ?? null,
            $data['password'] ?? '',
            $data['roles'] ?? [Role::SHOP],
        );
    }

    public function getId(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    public function getEmail(): string
    {
        $email = $this->email;

        \Webmozart\Assert\Assert::stringNotEmpty($email, 'Email should not be empty.');
        \Webmozart\Assert\Assert::email($email, 'Not valid email.');

        return $email;
    }

    public function getPassword(): string
    {
        $password = $this->password;

        \Webmozart\Assert\Assert::stringNotEmpty($password, 'Password should not be empty.');
        \Webmozart\Assert\Assert::minLength($password, 8, 'Password min length is 8.');

        return $password;
    }

    public function getPhone(): ?string
    {
        $password = $this->password;

        \Webmozart\Assert\Assert::nullOrStringNotEmpty($password, 'Phone should not be empty.');

        return $password;
    }

    /**
     * @return string[]
     *
     * @psalm-return list<non-empty-string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        \Webmozart\Assert\Assert::minCount($roles, 1);
        \Webmozart\Assert\Assert::allInArray($roles, Role::roles());

        return $roles;
    }
}
