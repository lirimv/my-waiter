<?php

declare(strict_types=1);

namespace MyWaiter\Domain\User;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface UserRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): User;

    public function save(User $user): void;

    /**
     * @psalm-param non-empty-string $email
     *
     * @throws NotFoundException
     */
    public function getByEmail(string $email): User;
}
