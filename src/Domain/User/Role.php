<?php

declare(strict_types=1);

namespace MyWaiter\Domain\User;

final class Role
{
    public const SHOP = 'ROLE_SHOP';
    public const SHOP_USER = 'ROLE_SHOP_USER';
    public const ADMIN = 'ROLE_ADMIN';
    public const ADMIN_USER = 'ROLE_ADMIN_USER';

    private function __construct()
    {
    }

    /**
     * @return string[]
     *
     * @psalm-return list<non-empty-string>
     */
    public static function roles(): array
    {
        return [self::SHOP, self::SHOP_USER, self::ADMIN, self::ADMIN_USER];
    }
}
