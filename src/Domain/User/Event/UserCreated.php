<?php

declare(strict_types=1);

namespace MyWaiter\Domain\User\Event;

use MyWaiter\Domain\User\User;
use Ramsey\Uuid\UuidInterface;

final class UserCreated
{
    private UuidInterface $userId;

    public function __construct(User $user)
    {
        $this->userId = $user->getId();
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }
}
