<?php

namespace MyWaiter\Domain\User;

use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepositoryInterface::class)
 */
class User implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary"))
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $phone;

    /**
     * @var string[]
     *
     * @ORM\Column(type="json")
     */
    private array $roles;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $password;

    /**
     * @param string[] $roles
     *
     * @psalm-param list<string>;
     * @psalm-param non-empty-string $password;
     */
    public function __construct(UuidInterface $id, string $email, ?string $password, ?string $phone, array $roles = [])
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->phone = $phone;
        $this->roles = $roles;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @psalm-param non-empty-string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array<string> $roles
     * @psalm-param list<string>
     */
    public function setRoles(array $roles): void
    {
        $this->roles = array_unique($roles);
    }

    public function addRoles(string ...$roles): void
    {
        $this->roles = array_unique(array_merge($this->roles, $roles));
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
    }
}
