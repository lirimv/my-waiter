<?php

declare(strict_types=1);

namespace MyWaiter\Domain\User\Handler;

use MyWaiter\Domain\User\Command\CreateUser;
use MyWaiter\Domain\User\Event\UserCreated;
use MyWaiter\Domain\User\User;
use MyWaiter\Domain\User\UserRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class CreateUserHandler implements MessageHandlerInterface
{
    private UserRepositoryInterface $users;
    private EncoderFactoryInterface $encoderFactory;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        UserRepositoryInterface $users,
        EncoderFactoryInterface $encoderFactory,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->users = $users;
        $this->encoderFactory = $encoderFactory;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateUser $command): void
    {
        $encoder = $this->encoderFactory->getEncoder(User::class);

        $user = new User(
            $command->getId(),
            $command->getEmail(),
            $encoder->encodePassword($command->getPassword(), ''),
            $command->getPhone(),
            $command->getRoles(),
        );

        $this->users->save($user);

        $this->eventDispatcher->dispatch(new UserCreated($user));
    }
}
