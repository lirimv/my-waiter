<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Assortment\Handler;

use MyWaiter\Domain\Assortment\AssortmentRepositoryInterface;
use MyWaiter\Domain\Assortment\Command\ReplaceAssortment;
use MyWaiter\Domain\Assortment\Event\AssortmentReplaced;
use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Channel\ChannelRepositoryInterface;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ReplaceAssortmentHandler implements MessageHandlerInterface
{
    private AssortmentRepositoryInterface $assortments;
    private ChannelRepositoryInterface $channels;
    private DishRepositoryInterface $dishes;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AssortmentRepositoryInterface $assortments,
        ChannelRepositoryInterface $channels,
        DishRepositoryInterface $dishes,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->assortments = $assortments;
        $this->channels = $channels;
        $this->dishes = $dishes;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(ReplaceAssortment $command): void
    {
        $assortment = $this->assortments->get($command->id());

        $assortment->setName($command->getName());

        $assortment->setChannels(array_values(array_map(function (UuidInterface $id): Channel {
            return $this->channels->get($id);
        }, $command->getChannels())));

        $assortment->setDishes(array_values(array_map(function (UuidInterface $id): Dish {
            return $this->dishes->get($id);
        }, $command->getDishes())));

        $this->assortments->save($assortment);

        $this->eventDispatcher->dispatch(new AssortmentReplaced($assortment));
    }
}
