<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Assortment\Handler;

use MyWaiter\Domain\Assortment\Assortment;
use MyWaiter\Domain\Assortment\AssortmentRepositoryInterface;
use MyWaiter\Domain\Assortment\Command\CreateAssortment;
use MyWaiter\Domain\Assortment\Event\AssortmentCreated;
use MyWaiter\Domain\Channel\ChannelRepositoryInterface;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateAssortmentHandler implements MessageHandlerInterface
{
    private AssortmentRepositoryInterface $assortments;
    private ChannelRepositoryInterface $channels;
    private DishRepositoryInterface $dishes;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        AssortmentRepositoryInterface $assortments,
        ChannelRepositoryInterface $channels,
        DishRepositoryInterface $dishes,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->assortments = $assortments;
        $this->channels = $channels;
        $this->dishes = $dishes;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(CreateAssortment $command): void
    {
        $assortment = new Assortment(
            $command->getId(),
            $command->getName()
        );

        foreach ($command->getChannels() as $channelId) {
            $assortment->addChannel($this->channels->get($channelId));
        }

        foreach ($command->getDishes() as $dishId) {
            $assortment->addDish($this->dishes->get($dishId));
        }

        $this->assortments->save($assortment);

        $this->eventDispatcher->dispatch(new AssortmentCreated($assortment));
    }
}
