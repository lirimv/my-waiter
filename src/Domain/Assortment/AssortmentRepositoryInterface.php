<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Assortment;

use MyWaiter\Domain\Exception\NotFoundException;
use Ramsey\Uuid\UuidInterface;

interface AssortmentRepositoryInterface
{
    /**
     * @throws NotFoundException
     */
    public function get(UuidInterface $id): Assortment;

    /**
     * @return Assortment[]
     *
     * @psalm-return list<Assortment>
     */
    public function getAll(): array;

    public function save(Assortment $address): void;

    public function delete(Assortment $assortment): void;
}
