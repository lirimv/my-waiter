<?php

namespace MyWaiter\Domain\Assortment;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity(repositoryClass=AssortmentRepositoryInterface::class)
 */
class Assortment implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @var Collection|Dish[]
     *
     * @ORM\ManyToMany(targetEntity=Dish::class)
     */
    private iterable $dishes;

    /**
     * @var Collection|Channel[]
     *
     * @ORM\ManyToMany(targetEntity=Channel::class, inversedBy="assortments")
     */
    private iterable $channels;

    /**
     * @psalm-param non-empty-string $name
     */
    public function __construct(?UuidInterface $id, string $name)
    {
        $this->id = $id ?? Uuid::uuid4();
        $this->name = $name;
        $this->dishes = new ArrayCollection();
        $this->channels = new ArrayCollection();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-param non-empty-string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Dish[]
     *
     * @psalm-return list<Dish>
     */
    public function getDishes(): array
    {
        return $this->dishes->toArray();
    }

    public function addDish(Dish $dish): void
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes[] = $dish;
        }
    }

    /**
     * @param Dish[] $dishes
     *
     * @psalm-var list<Dish>
     */
    public function setDishes(array $dishes): void
    {
        Assert::allIsInstanceOf($dishes, Dish::class);

        $this->dishes = new ArrayCollection($dishes);
    }

    public function removeDish(Dish $dish): void
    {
        if ($this->dishes->contains($dish)) {
            $this->dishes->removeElement($dish);
        }
    }

    /**
     * @return Channel[]
     *
     * @psalm-return list<Channel>
     */
    public function getChannels(): array
    {
        return $this->channels->toArray();
    }

    public function addChannel(Channel $channel): void
    {
        if (!$this->channels->contains($channel)) {
            $this->channels[] = $channel;
        }
    }

    /**
     * @param Channel[] $channels
     *
     * @psalm-var list<Channel>
     */
    public function setChannels(array $channels): void
    {
        Assert::allIsInstanceOf($channels, Channel::class);

        $this->channels = new ArrayCollection($channels);
    }

    public function removeChannel(Channel $channel): void
    {
        if ($this->channels->contains($channel)) {
            $this->channels->removeElement($channel);
        }
    }
}
