<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Assortment\Command;

use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use OpenApi\Annotations as OA;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-immutable
 */
final class ReplaceAssortment implements Command, IdAwareCommand
{
    /**
     * @Assert\Uuid()
     * @Assert\NotBlank()
     */
    private mixed $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @OA\Property(type="string", example="Online Gerichte")
     */
    private mixed $name;

    /**
     * @Assert\All({
     *     @Assert\Uuid()
     * })
     *
     * @psalm-var list<string>
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", minLength=1))
     */
    private mixed $dishes;

    /**
     * @Assert\All({
     *     @Assert\Uuid()
     * })
     *
     * @psalm-var list<string>
     *
     * @OA\Property(type="array", items=@OA\Items(type="string", minLength=1))
     */
    private mixed $channels;

    public static function fromArray(array $data): self
    {
        $self = new self();

        $self->id = $data['id'] ?? null;
        $self->name = $data['name'] ?? null;
        $self->dishes = $data['dishes'] ?? [];
        $self->channels = $data['channels'] ?? [];

        return $self;
    }

    public function id(): UuidInterface
    {
        $id = $this->id;

        \Webmozart\Assert\Assert::stringNotEmpty($id);
        \Webmozart\Assert\Assert::uuid($id);

        return Uuid::fromString($id);
    }

    /**
     * @psalm-return non-empty-string
     */
    public function getName(): string
    {
        $name = $this->name;

        \Webmozart\Assert\Assert::stringNotEmpty($name);

        return $name;
    }

    /**
     * @psalm-return list<UuidInterface>
     */
    public function getDishes(): array
    {
        $dishes = $this->dishes;

        \Webmozart\Assert\Assert::allStringNotEmpty($dishes);
        \Webmozart\Assert\Assert::allUuid($dishes);

        return array_map(static fn ($id) => Uuid::fromString($id), $dishes);
    }

    /**
     * @psalm-return list<UuidInterface>
     */
    public function getChannels(): array
    {
        $channels = $this->channels;

        \Webmozart\Assert\Assert::allStringNotEmpty($channels);
        \Webmozart\Assert\Assert::allUuid($channels);

        return array_map(static fn ($id) => Uuid::fromString($id), $channels);
    }

    public static function getIds(): array
    {
        return ['id'];
    }
}
