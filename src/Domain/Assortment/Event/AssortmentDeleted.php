<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Assortment\Event;

use MyWaiter\Domain\Assortment\Assortment;

final class AssortmentDeleted
{
    private Assortment $assortment;

    public function __construct(Assortment $assortment)
    {
        $this->assortment = $assortment;
    }

    public function getEntity(): Assortment
    {
        return $this->assortment;
    }
}
