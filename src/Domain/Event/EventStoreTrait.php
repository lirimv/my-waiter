<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

trait EventStoreTrait
{
    /**
     * @var Collection|EventStore[]
     *
     * @ORM\ManyToMany(targetEntity="\MyWaiter\Domain\Event\EventStore", cascade={"all"})
     * @JoinTable(
     *     name="entity_event",
     *     joinColumns={@JoinColumn(name="event_id", referencedColumnName="id")},
     *     inverseJoinColumns={@JoinColumn(name="entity_id", referencedColumnName="id", unique=true)}
     * )
     */
    protected iterable $events;

    /**
     * {@inheritdoc}
     */
    public function getEvents(): array
    {
        return $this->events->toArray();
    }

    public function addEvent(EventStore $event): void
    {
        $this->events[] = $event;
    }
}
