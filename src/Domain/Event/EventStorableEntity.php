<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event;

use MyWaiter\Domain\Entity\EntityInterface;

interface EventStorableEntity extends EntityInterface
{
    /**
     * @return EventStore[]
     *
     * @psalm-return list<EventStore>
     */
    public function getEvents(): array;

    public function addEvent(EventStore $event): void;
}
