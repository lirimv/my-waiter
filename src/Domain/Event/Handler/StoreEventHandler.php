<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event\Handler;

use Doctrine\ORM\EntityManagerInterface;
use MyWaiter\Domain\Event\Command\StoreEvent;
use MyWaiter\Domain\Event\EventStorableEntity;
use MyWaiter\Domain\Event\EventStore;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class StoreEventHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $em;
    private LoggerInterface $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    public function __invoke(StoreEvent $command): void
    {
        $entity = $this->em->find($command->getEntity(), $command->getId());

        if (!$entity instanceof EventStorableEntity) {
            $this->logger->warning('Could not store event!');

            return;
        }

        $entity->addEvent(new EventStore(Uuid::uuid4(), $command->getEvent(), $command->getPayload()));

        $this->em->persist($entity);
        $this->em->flush();
    }
}
