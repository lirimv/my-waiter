<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event;

use Ramsey\Uuid\UuidInterface;

interface DomainEvent
{
    public function getId(): UuidInterface;

    /** @psalm-return class-string */
    public function getClass(): string;

    public function getPayload(): array;

    public static function getName(): string;
}
