<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event\Command;

use MyWaiter\Domain\Event\DomainEvent;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

final class StoreEvent
{
    /** @psalm-var class-string */
    private string $entity;
    private UuidInterface $id;
    private string $event;
    private array $payload;

    /** @psalm-param class-string $entity */
    public function __construct(string $entity, UuidInterface $id, string $event, array $payload)
    {
        $this->entity = $entity;
        $this->id = $id;
        $this->event = $event;
        $this->payload = $payload;
    }

    public static function fromDomainEvent(DomainEvent $event): self
    {
        return new self(
            $event->getClass(),
            $event->getId(),
            $event::getName(),
            $event->getPayload()
        );
    }

    /**
     * @psalm-return class-string
     */
    public function getEntity(): string
    {
        $entityClass = $this->entity;

        Assert::classExists($entityClass);

        return $entityClass;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getEvent(): string
    {
        return $this->event;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}
