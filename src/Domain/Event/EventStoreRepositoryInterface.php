<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event;

use MyWaiter\Domain\Entity\EntityInterface;
use Ramsey\Uuid\UuidInterface;

interface EventStoreRepositoryInterface
{
    public function get(UuidInterface $id): EventStore;

    public function save(EntityInterface $entity): void;
}
