<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event;

use Doctrine\ORM\Mapping as ORM;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Entity\Timestampable;
use MyWaiter\Domain\Entity\TimestampableInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=EventStoreRepositoryInterface::class)
 */
class EventStore implements EntityInterface, TimestampableInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid_binary")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $event;

    /**
     * @ORM\Column(type="json")
     */
    private array $payload;

    public function __construct(UuidInterface $id, string $event, array $payload)
    {
        $this->id = $id;
        $this->event = $event;
        $this->payload = $payload;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getEvent(): string
    {
        return $this->event;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}
