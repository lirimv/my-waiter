<?php

declare(strict_types=1);

namespace MyWaiter\Domain\Event\Subscriber;

use MyWaiter\Domain\Channel\Event\ChannelReplaced;
use MyWaiter\Domain\Event\Command\StoreEvent;
use MyWaiter\Domain\Event\DomainEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class EventStoreSubscriber implements EventSubscriberInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ChannelReplaced::class => 'storeEvent',
        ];
    }

    public function storeEvent(DomainEvent $event): void
    {
        $this->messageBus->dispatch(StoreEvent::fromDomainEvent($event));
    }
}
