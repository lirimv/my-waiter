<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Security;

use function strtolower;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractUser implements UserInterface
{
    protected string $username;
    protected ?string $password;

    /**
     * @var string[]
     */
    protected array $roles;

    /**
     * @param string[] $roles
     */
    protected function __construct(string $username, ?string $password, array $roles)
    {
        $this->username = $username;
        $this->password = $password;
        $this->roles = $roles;
    }

    abstract public function isAdminUser(): bool;

    abstract public function isShopUser(): bool;

    abstract public function isAnonymousShopUser(): bool;

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): string
    {
        return strtolower($this->username);
    }

    public function eraseCredentials(): void
    {
    }
}
