<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Security\Exception;

use MyWaiter\Domain\Exception\AccessDeniedException;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

final class AuthorizationFailedException extends RuntimeException implements AccessDeniedException
{
    public function __construct()
    {
        parent::__construct('Forbidden.', Response::HTTP_FORBIDDEN);
    }
}
