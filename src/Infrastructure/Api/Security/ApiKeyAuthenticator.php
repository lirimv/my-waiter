<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Api\Security;

use MyWaiter\Domain\ApiKey\ApiKey;
use MyWaiter\Domain\User\Role;
use MyWaiter\Infrastructure\Doctrine\Repository\ApiKeyRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

final class ApiKeyAuthenticator extends AbstractGuardAuthenticator
{
    private const HEADER_KEY = 'X-API-KEY';

    private static array $roleMap = [
        ApiKey::ADMIN => Role::ADMIN,
        ApiKey::SHOP => Role::SHOP,
    ];

    private ApiKeyRepository $apiKeyRepository;

    public function __construct(ApiKeyRepository $apiKeyRepository)
    {
        $this->apiKeyRepository = $apiKeyRepository;
    }

    public function start(Request $request, AuthenticationException $authException = null): JsonResponse
    {
        return new JsonResponse(['code' => 401, 'message' => 'No Permission.', 'data' => null], 401);
    }

    public function supports(Request $request): bool
    {
        return (bool) $request->headers->get(self::HEADER_KEY);
    }

    public function getCredentials(Request $request): array
    {
        return [
            'apiKey' => $request->headers->get(self::HEADER_KEY),
            'path' => $request->getPathInfo(),
        ];
    }

    /**
     * @psalm-param array{
     *      apiKey: non-empty-string,
     *      path: non-empty-string
     * }
     */
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        $key = $this->apiKeyRepository->findByApiKey($credentials['apiKey']);
        $path = $credentials['path'];

        if (!$key || !$path) {
            throw new CustomUserMessageAuthenticationException('Authentication failed due to missing data.');
        }

        $type = $key->getType();

        if (!isset(self::$roleMap[$type])) {
            throw new AuthenticationException(sprintf('Api key type "%s" could not be mapped to a role', $type));
        }

        if (!$this->keyTypeMatchesAdapter($path, $type)) {
            throw new AuthenticationException(sprintf('Api key type "%s" does not match adapter endpoint "%s"', $type, $path));
        }

        return new User($key->getName(), null, [self::$roleMap[$key->getType()]]);
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?JsonResponse
    {
        return new JsonResponse(['code' => 401, 'message' => $exception->getMessageKey(), 'data' => null], 401);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): Response | null
    {
        return null;
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }

    private function keyTypeMatchesAdapter(string $path, string $type): bool
    {
        switch ($type) {
            case ApiKey::SHOP:
                return str_starts_with($path, '/shop/v1/');
            case ApiKey::ADMIN:
                return str_starts_with($path, '/admin/v1/');
        }

        return false;
    }
}
