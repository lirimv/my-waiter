<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Api\Subscriber;

use Exception;
use MyWaiter\Domain\Exception\DomainException;
use MyWaiter\Domain\Exception\ViolationException;
use MyWaiter\Infrastructure\Api\Exception\MalformedJsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

final class JsonExceptionSubscriber implements EventSubscriberInterface
{
    private const SHOW_STACKTRACE_IN = ['dev', 'test'];

    private LoggerInterface $logger;
    private string $environment;

    public function __construct(LoggerInterface $logger, string $environment)
    {
        $this->logger = $logger;
        $this->environment = $environment;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $response = $this->transformException($event->getThrowable());

        if ($response) {
            $event->setResponse($response);
        }
    }

    /**
     * Note: if you change exclusions to become user errors, remember to adjust `config/packages/prod/sentry.yaml` too,
     *       or else sentry will be cluttered with errors that we don't consider to be actual application errors, but
     *       rather user errors.
     */
    private function transformException(Throwable $exception): ?Response
    {
        if (($exception instanceof HandlerFailedException) && ($e = $exception->getPrevious()) && $e instanceof Exception) {
            return $this->transformException($e);
        }

        if ($exception instanceof ViolationException) {
            $data = [
                'code' => $exception->getCode(),
                'message' => 'Validation errors.',
                'data' => $this->transformViolationList($exception->getViolationList()),
            ];

            $this->logger->notice('A ValidationException reached the user', $data);

            return new JsonResponse($data, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($exception instanceof HttpException) {
            $data = [
                'code' => $exception->getStatusCode(),
                'message' => $this->mapHttpCodeToMessage($exception->getStatusCode()),
            ];

            $this->logger->notice('A HttpException reached the user', $data);

            return new JsonResponse($data, $exception->getStatusCode());
        }

        if ($exception instanceof DomainException) {
            return new JsonResponse([
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ], $exception->getCode());
        }

        if ($exception instanceof MalformedJsonException) {
            return new JsonResponse([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Malformed JSON: '.$exception->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->logger->critical(
            'A Exception that is not handled by the listener reached the user',
            [
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
            ]
        );

        if ($this->showStacktrace()) {
            return null;
        }

        return new JsonResponse([
            'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => $this->mapHttpCodeToMessage(Response::HTTP_INTERNAL_SERVER_ERROR),
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    private function transformViolationList(ConstraintViolationListInterface $violationList): array
    {
        $errors = [];

        /** @var ConstraintViolation $violation */
        foreach ($violationList as $violation) {
            $errors[] = [
                'property' => $violation->getPropertyPath(),
                'message' => $violation->getMessage(),
                'code' => $violation->getCode(),
            ];
        }

        return $errors;
    }

    private function mapHttpCodeToMessage(int $code): string
    {
        return Response::$statusTexts[$code] ?? 'Http error.';
    }

    private function showStacktrace(): bool
    {
        return in_array($this->environment, self::SHOW_STACKTRACE_IN, true);
    }
}
