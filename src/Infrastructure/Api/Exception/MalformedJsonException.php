<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Api\Exception;

use Exception;

final class MalformedJsonException extends Exception
{
}
