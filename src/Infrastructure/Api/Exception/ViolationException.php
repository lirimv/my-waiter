<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Api\Exception;

use MyWaiter\Domain\Exception\ViolationException as DomainViolationException;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ViolationException extends RuntimeException implements DomainViolationException
{
    private ConstraintViolationListInterface $violationList;

    public function __construct(ConstraintViolationListInterface $violationList)
    {
        $this->violationList = $violationList;

        parent::__construct('Violations!', Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function getViolationList(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}
