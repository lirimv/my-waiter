<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Api;

use InvalidArgumentException;
use MyWaiter\Domain\Command\Command;
use MyWaiter\Domain\Command\IdAwareCommand;
use MyWaiter\Infrastructure\Api\Exception\MalformedJsonException;
use MyWaiter\Infrastructure\Api\Exception\ViolationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class CommandResolver
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function resolveRequest(Request $request, string $commandClass): Command
    {
        if (!is_a($commandClass, Command::class, true)) {
            throw new InvalidArgumentException(sprintf('%s is not a command.', $commandClass));
        }

        $data = $this->extractPayload($request);

        if (is_a($commandClass, IdAwareCommand::class, true)) {
            foreach ($commandClass::getIds() as $id) {
                $value = $request->get($id);

                if (!$value) {
                    throw new InvalidArgumentException(sprintf('missing "%s" in request', $id));
                }

                $data[$id] = $value;
            }
        }

        $command = $commandClass::fromArray($data);

        $violations = $this->validator->validate($command);

        if (count($violations) > 0) {
            throw new ViolationException($violations);
        }

        return $command;
    }

    private function extractPayload(Request $request): array
    {
        $content = $request->getContent();

        if ('' === $content) {
            return [];
        }

        $data = json_decode((string) $content, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new MalformedJsonException(json_last_error_msg(), json_last_error());
        }

        return $data;
    }
}
