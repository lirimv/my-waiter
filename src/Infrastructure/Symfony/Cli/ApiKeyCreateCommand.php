<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Symfony\Cli;

use MyWaiter\Domain\ApiKey\Command\CreateApiKey;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webmozart\Assert\Assert;

final class ApiKeyCreateCommand extends Command
{
    protected static $defaultName = 'mywaiter:api-key:create';

    private MessageBusInterface $commandBus;
    private ValidatorInterface $validator;

    public function __construct(MessageBusInterface $commandBus, ValidatorInterface $validator)
    {
        parent::__construct();

        $this->setDescription('create api key');
        $this->addArgument(
            'name',
            InputArgument::REQUIRED,
            'Select a name'
        );
        $this->addArgument(
            'type',
            InputArgument::REQUIRED,
            'Whether the api key should have permission for "admin" or "shop"'
        );
        $this->addArgument(
            'keyValue',
            InputArgument::OPTIONAL,
            'If provided, the API key will have the given value: only to be used to pre-seed environments'
            .' with constant keys (staging/demo)'
        );

        $this->commandBus = $commandBus;
        $this->validator = $validator;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output = new SymfonyStyle($input, $output);

        $command = CreateApiKey::fromArray(
            [
                'name' => $input->getArgument('name'),
                'type' => $input->getArgument('type'),
                'key' => $input->getArgument('keyValue'),
            ]
        );

        /** @var ConstraintViolationInterface[] $violations */
        $violations = $this->validator->validate($command);
        if (\count($violations) > 0) {
            foreach ($violations as $violation) {
                $message = $violation->getMessage();

                Assert::string($message);

                $output->error(sprintf('%s: %s', $violation->getPropertyPath(), $message));
            }

            return 1;
        }

        $this->commandBus->dispatch($command);

        $output->success(sprintf(
            'New %s api key %s created: "%s"',
            $command->getType(),
            $command->getName(),
            $command->getKey())
        );

        return 0;
    }
}
