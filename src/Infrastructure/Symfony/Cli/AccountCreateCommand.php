<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Symfony\Cli;

use function count;
use MyWaiter\Domain\Account\Command\CreateAccount;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webmozart\Assert\Assert;

final class AccountCreateCommand extends Command
{
    protected static $defaultName = 'mywaiter:account:create';

    private MessageBusInterface $commandBus;
    private ValidatorInterface $validator;

    public function __construct(MessageBusInterface $commandBus, ValidatorInterface $validator)
    {
        parent::__construct();

        $this->setDescription('Create admin user');
        $this->addArgument(
            'email',
            InputArgument::REQUIRED,
            'Enter an email address.'
        );
        $this->addArgument(
            'password',
            InputArgument::REQUIRED,
            'Enter a password.'
        );
        $this->addArgument(
            'phone',
            InputArgument::OPTIONAL,
            'Enter a phone number.'
        );

        $this->commandBus = $commandBus;
        $this->validator = $validator;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output = new SymfonyStyle($input, $output);

        $command = CreateAccount::fromArray(
            [
                'admin' => [
                    'email' => $input->getArgument('email'),
                    'password' => $input->getArgument('password'),
                    'phone' => $input->getArgument('phone'),
                ],
            ]
        );

        /** @var ConstraintViolationInterface[] $violations */
        $violations = $this->validator->validate($command);
        if (count($violations) > 0) {
            foreach ($violations as $violation) {
                $message = $violation->getMessage();

                Assert::string($message);

                $output->error(sprintf('%s: %s', $violation->getPropertyPath(), $message));
            }

            return 1;
        }

        $this->commandBus->dispatch($command);

        $output->success(sprintf(
                'New account with admin user "%s" created.',
                $command->getAdmin()->getEmail(),
        ));

        return 0;
    }
}
