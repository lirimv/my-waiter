<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Dish\Variation\Collection;
use MyWaiter\Domain\Dish\Variation\DishVariation;
use MyWaiter\Domain\Dish\Variation\DishVariationRepositoryInterface;
use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\NotFoundException;
use MyWaiter\Infrastructure\Doctrine\QueryHelper;
use Ramsey\Uuid\UuidInterface;

/**
 * @method DishVariation|null find($id, $lockMode = null, $lockVersion = null)
 * @method DishVariation|null findOneBy(array $criteria, array $orderBy = null)
 * @method DishVariation[]    findAll()
 * @method DishVariation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishVariationRepository extends ServiceEntityRepository implements DishVariationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DishVariation::class);
    }

    public function get(UuidInterface $id): DishVariation
    {
        $variation = $this->findOneBy(['id' => $id]);

        if (null === $variation) {
            throw NotFoundException::byId(DishVariation::class, $id);
        }

        return $variation;
    }

    public function save(DishVariation $variation): void
    {
        $this->getEntityManager()->persist($variation);
        $this->getEntityManager()->flush();
    }

    public function delete(DishVariation $variation): void
    {
        $this->getEntityManager()->remove($variation);
        $this->getEntityManager()->flush();
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function findByCriteria(Criteria $criteria): Collection
    {
        $tableAlias = 'd';

        $qb = $this->createQueryBuilder($tableAlias)
            ->setFirstResult($criteria->getOffset())
            ->setMaxResults($criteria->getLimit());

        $queryHelper = QueryHelper::create(
            $this->getEntityManager()->getClassMetadata(DishVariation::class),
            $qb,
            $tableAlias
        );
        $queryHelper->addFilters($criteria->getFilter());
        $queryHelper->addSorting($criteria->getOrder());

        $paginator = new Paginator($qb->getQuery());

        return Collection::fromPaginatorAndCriteria($paginator, $criteria);
    }
}
