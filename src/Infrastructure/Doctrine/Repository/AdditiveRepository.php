<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Additive\Additive;
use MyWaiter\Domain\Additive\AdditiveRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Additive|null find($id, $lockMode = null, $lockVersion = null)
 * @method Additive|null findOneBy(array $criteria, array $orderBy = null)
 * @method Additive[]    findAll()
 * @method Additive[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdditiveRepository extends ServiceEntityRepository implements AdditiveRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Additive::class);
    }

    public function get(UuidInterface $id): Additive
    {
        $additive = $this->findOneBy(['id' => $id]);

        if (null === $additive) {
            throw NotFoundException::byId(Additive::class, $id);
        }

        return $additive;
    }

    /**
     * @return Additive[]
     *
     * @psalm-return list<Additive>
     */
    public function all(): array
    {
        $qb = $this->createQueryBuilder('a')->orderBy('a.name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function save(Additive $additive): void
    {
        $this->getEntityManager()->persist($additive);
        $this->getEntityManager()->flush();
    }

    public function delete(Additive $additive): void
    {
        $this->getEntityManager()->remove($additive);
        $this->getEntityManager()->flush();
    }
}
