<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Dish\Collection;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\DishRepositoryInterface;
use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\NotFoundException;
use MyWaiter\Infrastructure\Doctrine\QueryHelper;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Dish|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dish|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dish[]    findAll()
 * @method Dish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishRepository extends ServiceEntityRepository implements DishRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dish::class);
    }

    public function get(UuidInterface $id): Dish
    {
        $dish = $this->findOneBy(['id' => $id]);

        if (null === $dish) {
            throw NotFoundException::byId(Dish::class, $id);
        }

        return $dish;
    }

    /**
     * @return Dish[]
     *
     * @psalm-return list<Dish>
     */
    public function all(): array
    {
        $qb = $this->createQueryBuilder('d')->orderBy('d.number', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function save(Dish $dish): void
    {
        $this->getEntityManager()->persist($dish);
        $this->getEntityManager()->flush();
    }

    public function delete(Dish $dish): void
    {
        $this->getEntityManager()->remove($dish);
        $this->getEntityManager()->flush();
    }

    public function findByCriteria(Criteria $criteria): Collection
    {
        $tableAlias = 'd';

        $qb = $this->createQueryBuilder($tableAlias)
            ->setFirstResult($criteria->getOffset())
            ->setMaxResults($criteria->getLimit());

        $queryHelper = QueryHelper::create(
            $this->getEntityManager()->getClassMetadata(Dish::class),
            $qb,
            $tableAlias
        );
        $queryHelper->addFilters($criteria->getFilter());
        $queryHelper->addSorting($criteria->getOrder());

        $paginator = new Paginator($qb->getQuery());

        return Collection::fromPaginatorAndCriteria($paginator, $criteria);
    }
}
