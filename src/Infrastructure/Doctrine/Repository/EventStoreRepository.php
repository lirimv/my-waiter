<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Entity\EntityInterface;
use MyWaiter\Domain\Event\EventStore;
use MyWaiter\Domain\Event\EventStoreRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @method EventStore|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventStore|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventStore[]    findAll()
 * @method EventStore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventStoreRepository extends ServiceEntityRepository implements EventStoreRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventStore::class);
    }

    public function get(UuidInterface $id): EventStore
    {
        $event = $this->find($id);

        if (null === $event) {
            throw NotFoundException::byId(EventStore::class, $id);
        }

        return $event;
    }

    public function getRepository(): EntityRepository
    {
        /** @var EntityRepository $objectManager */
        $objectManager = $this->getEntityManager()->getRepository(EventStore::class);

        Assert::isInstanceOf($objectManager, EntityRepository::class);

        return $objectManager;
    }

    public function save(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }
}
