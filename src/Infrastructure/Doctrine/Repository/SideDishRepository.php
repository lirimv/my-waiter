<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Dish\SideDish\Collection;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\SideDish\SideDishRepositoryInterface;
use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\NotFoundException;
use MyWaiter\Infrastructure\Doctrine\QueryHelper;
use Ramsey\Uuid\UuidInterface;

/**
 * @method SideDish|null find($id, $lockMode = null, $lockVersion = null)
 * @method SideDish|null findOneBy(array $criteria, array $orderBy = null)
 * @method SideDish[]    findAll()
 * @method SideDish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SideDishRepository extends ServiceEntityRepository implements SideDishRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SideDish::class);
    }

    public function get(UuidInterface $id): SideDish
    {
        $sideDish = $this->findOneBy(['id' => $id]);

        if (null === $sideDish) {
            throw NotFoundException::byId(SideDish::class, $id);
        }

        return $sideDish;
    }

    /**
     * @return SideDish[]
     *
     * @psalm-return list<SideDish>
     */
    public function all(): array
    {
        $qb = $this->createQueryBuilder('d')->orderBy('d.name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function save(SideDish $sideDish): void
    {
        $this->getEntityManager()->persist($sideDish);
        $this->getEntityManager()->flush();
    }

    public function delete(SideDish $sideDish): void
    {
        $this->getEntityManager()->remove($sideDish);
        $this->getEntityManager()->flush();
    }

    public function findByCriteria(Criteria $criteria): Collection
    {
        $tableAlias = 'd';

        $qb = $this->createQueryBuilder($tableAlias)
            ->setFirstResult($criteria->getOffset())
            ->setMaxResults($criteria->getLimit());

        $queryHelper = QueryHelper::create(
            $this->getEntityManager()->getClassMetadata(SideDish::class),
            $qb,
            $tableAlias
        );
        $queryHelper->addFilters($criteria->getFilter());
        $queryHelper->addSorting($criteria->getOrder());

        $paginator = new Paginator($qb->getQuery());

        return Collection::fromPaginatorAndCriteria($paginator, $criteria);
    }
}
