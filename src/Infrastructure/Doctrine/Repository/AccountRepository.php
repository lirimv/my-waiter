<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Account\Account;
use MyWaiter\Domain\Account\AccountRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @method Account|null find($id, $lockMode = null, $lockVersion = null)
 * @method Account|null findOneBy(array $criteria, array $orderBy = null)
 * @method Account[]    findAll()
 * @method Account[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountRepository extends ServiceEntityRepository implements AccountRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    /**
     * @throws NotFoundException
     */
    public function getFirst(): Account
    {
        $qb = $this->getRepository()->createQueryBuilder('a');

        try {
            $account = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw NotFoundException::byResource(Account::class);
        }

        return $account;
    }

    public function get(UuidInterface $id): Account
    {
        $account = $this->findOneBy(['id' => $id]);

        if (null === $account) {
            throw NotFoundException::byId(Account::class, $id);
        }

        return $account;
    }

    public function getRepository(): EntityRepository
    {
        /** @var EntityRepository $objectManager */
        $objectManager = $this->getEntityManager()->getRepository(Account::class);

        Assert::isInstanceOf($objectManager, EntityRepository::class);

        return $objectManager;
    }

    public function save(Account $account): void
    {
        $this->_em->persist($account);
        $this->_em->flush();
    }
}
