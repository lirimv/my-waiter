<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\Group\Collection;
use MyWaiter\Domain\Dish\Group\DishGroup;
use MyWaiter\Domain\Dish\Group\DishGroupRepositoryInterface;
use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\NotFoundException;
use MyWaiter\Infrastructure\Doctrine\QueryHelper;
use Ramsey\Uuid\UuidInterface;

/**
 * @method DishGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DishGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DishGroup[]    findAll()
 * @method DishGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishGroupRepository extends ServiceEntityRepository implements DishGroupRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DishGroup::class);
    }

    public function get(UuidInterface $id): DishGroup
    {
        $dish = $this->findOneBy(['id' => $id]);

        if (null === $dish) {
            throw NotFoundException::byId(Dish::class, $id);
        }

        return $dish;
    }

    public function save(DishGroup $dishGroup): void
    {
        $this->getEntityManager()->persist($dishGroup);
        $this->getEntityManager()->flush();
    }

    public function delete(DishGroup $dishGroup): void
    {
        $this->getEntityManager()->remove($dishGroup);
        $this->getEntityManager()->flush();
    }

    public function findByCriteria(Criteria $criteria): Collection
    {
        $tableAlias = 'd';

        $qb = $this->createQueryBuilder($tableAlias)
            ->setFirstResult($criteria->getOffset())
            ->setMaxResults($criteria->getLimit());

        $queryHelper = QueryHelper::create(
            $this->getEntityManager()->getClassMetadata(DishGroup::class),
            $qb,
            $tableAlias
        );
        $queryHelper->addFilters($criteria->getFilter());
        $queryHelper->addSorting($criteria->getOrder());

        $paginator = new Paginator($qb->getQuery());

        $query = $qb->getQuery()->getSQL();
        $parameters = $qb->getParameters();

        return Collection::fromPaginatorAndCriteria($paginator, $criteria);
    }
}
