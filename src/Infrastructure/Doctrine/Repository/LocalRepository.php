<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Local\Local;
use MyWaiter\Domain\Local\LocalRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @method Local|null find($id, $lockMode = null, $lockVersion = null)
 * @method Local|null findOneBy(array $criteria, array $orderBy = null)
 * @method Local[]    findAll()
 * @method Local[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalRepository extends ServiceEntityRepository implements LocalRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Local::class);
    }

    public function get(UuidInterface $id): Local
    {
        $local = $this->findOneBy(['id' => $id]);

        if (null === $local) {
            throw NotFoundException::byId(Local::class, $id);
        }

        return $local;
    }

    public function save(Local $local): void
    {
        $this->getEntityManager()->persist($local);
        $this->getEntityManager()->flush();
    }

    public function delete(Local $local): void
    {
        $this->getEntityManager()->remove($local);
        $this->getEntityManager()->flush();
    }

    public function getAll(): array
    {
        $locals = $this->findAll();

        Assert::allIsInstanceOf($locals, Local::class);

        return $locals;
    }
}
