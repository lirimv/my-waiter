<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Dish\Ingredient\Ingredient;
use MyWaiter\Domain\Dish\Ingredient\IngredientRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Ingredient|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ingredient|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ingredient[]    findAll()
 * @method Ingredient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IngredientRepository extends ServiceEntityRepository implements IngredientRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ingredient::class);
    }

    public function get(UuidInterface $id): Ingredient
    {
        $variation = $this->findOneBy(['id' => $id]);

        if (null === $variation) {
            throw NotFoundException::byId(Ingredient::class, $id);
        }

        return $variation;
    }

    /**
     * @return Ingredient[]
     *
     * @psalm-return list<Ingredient>
     */
    public function getAll(): array
    {
        $qb = $this->createQueryBuilder('i')->orderBy('i.name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function save(Ingredient $ingredient): void
    {
        $this->getEntityManager()->persist($ingredient);
        $this->getEntityManager()->flush();
    }

    public function delete(Ingredient $ingredient): void
    {
        $this->getEntityManager()->remove($ingredient);
        $this->getEntityManager()->flush();
    }
}
