<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Channel\ChannelRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Channel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Channel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Channel[]    findAll()
 * @method Channel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelRepository extends ServiceEntityRepository implements ChannelRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Channel::class);
    }

    public function get(UuidInterface $id): Channel
    {
        $channel = $this->findOneBy(['id' => $id]);

        if (null === $channel) {
            throw NotFoundException::byId(Channel::class, $id);
        }

        return $channel;
    }

    public function save(Channel $channel): void
    {
        $this->getEntityManager()->persist($channel);
        $this->getEntityManager()->flush();
    }

    public function delete(Channel $channel): void
    {
        $this->getEntityManager()->remove($channel);
        $this->getEntityManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getAll(): array
    {
        return $this->findAll();
    }
}
