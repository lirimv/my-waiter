<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Dish\Extra\Collection;
use MyWaiter\Domain\Dish\Extra\Extra;
use MyWaiter\Domain\Dish\Extra\ExtraRepositoryInterface;
use MyWaiter\Domain\Entity\Criteria;
use MyWaiter\Domain\NotFoundException;
use MyWaiter\Infrastructure\Doctrine\QueryHelper;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Extra|null find($id, $lockMode = null, $lockVersion = null)
 * @method Extra|null findOneBy(array $criteria, array $orderBy = null)
 * @method Extra[]    findAll()
 * @method Extra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraRepository extends ServiceEntityRepository implements ExtraRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Extra::class);
    }

    public function get(UuidInterface $id): Extra
    {
        $variation = $this->findOneBy(['id' => $id]);

        if (null === $variation) {
            throw NotFoundException::byId(Extra::class, $id);
        }

        return $variation;
    }

    public function save(Extra $extra): void
    {
        $this->getEntityManager()->persist($extra);
        $this->getEntityManager()->flush();
    }

    public function delete(Extra $extra): void
    {
        $this->getEntityManager()->remove($extra);
        $this->getEntityManager()->flush();
    }

    public function findByCriteria(Criteria $criteria): Collection
    {
        $tableAlias = 'e';

        $qb = $this->createQueryBuilder($tableAlias)
            ->setFirstResult($criteria->getOffset())
            ->setMaxResults($criteria->getLimit());

        $queryHelper = QueryHelper::create(
            $this->getEntityManager()->getClassMetadata(Extra::class),
            $qb,
            $tableAlias
        );
        $queryHelper->addFilters($criteria->getFilter());
        $queryHelper->addSorting($criteria->getOrder());

        $sql = $qb->getQuery()->getSQL();
        $paramteter = $qb->getParameters();
        $paginator = new Paginator($qb->getQuery());

        return Collection::fromPaginatorAndCriteria($paginator, $criteria);
    }
}
