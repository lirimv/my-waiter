<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\NotFoundException;
use MyWaiter\Domain\Table\Table;
use MyWaiter\Domain\Table\TableRepositoryInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Table|null find($id, $lockMode = null, $lockVersion = null)
 * @method Table|null findOneBy(array $criteria, array $orderBy = null)
 * @method Table[]    findAll()
 * @method Table[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableRepository extends ServiceEntityRepository implements TableRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Table::class);
    }

    public function get(UuidInterface $id): Table
    {
        $table = $this->find($id);

        if (null === $table) {
            throw NotFoundException::byId(Table::class, $id);
        }

        return $table;
    }

    public function save(Table $table): void
    {
        $this->getEntityManager()->persist($table);
        $this->getEntityManager()->flush();
    }

    public function delete(Table $table): void
    {
        $this->getEntityManager()->remove($table);
        $this->getEntityManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getAll(): array
    {
        return $this->findAll();
    }
}
