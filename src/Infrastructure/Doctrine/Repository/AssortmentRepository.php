<?php

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\Assortment\Assortment;
use MyWaiter\Domain\Assortment\AssortmentRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Assortment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Assortment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Assortment[]    findAll()
 * @method Assortment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssortmentRepository extends ServiceEntityRepository implements AssortmentRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Assortment::class);
    }

    public function get(UuidInterface $id): Assortment
    {
        $assortment = $this->findOneBy(['id' => $id]);

        if (null === $assortment) {
            throw NotFoundException::byId(Assortment::class, $id);
        }

        return $assortment;
    }

    public function save(Assortment $assortment): void
    {
        $this->getEntityManager()->persist($assortment);
        $this->getEntityManager()->flush();
    }

    public function delete(Assortment $assortment): void
    {
        $this->getEntityManager()->remove($assortment);
        $this->getEntityManager()->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getAll(): array
    {
        return $this->findAll();
    }
}
