<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use MyWaiter\Domain\ApiKey\ApiKey;
use MyWaiter\Domain\ApiKey\ApiKeyRepositoryInterface;
use MyWaiter\Domain\NotFoundException;
use Ramsey\Uuid\UuidInterface;
use Webmozart\Assert\Assert;

/**
 * @method ApiKey|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiKey|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiKey[]    findAll()
 * @method ApiKey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ApiKeyRepository extends ServiceEntityRepository implements ApiKeyRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApiKey::class);
    }

    public function get(UuidInterface $id): ApiKey
    {
        $key = $this->find($id);

        if (!$key instanceof ApiKey) {
            throw NotFoundException::byId(ApiKey::class, $id);
        }

        return $key;
    }

    public function getAll(): array
    {
        return $this->findAll();
    }

    public function save(ApiKey $key): void
    {
        $this->getEntityManager()->persist($key);
        $this->getEntityManager()->flush();
    }

    public function delete(ApiKey $key): void
    {
        $this->getEntityManager()->remove($key);
        $this->getEntityManager()->flush();
    }

    public function findByApiKey(string $apiKey): ?ApiKey
    {
        $entity = $this->findOneBy([
            'value' => $apiKey,
        ]);

        Assert::nullOrIsInstanceOf($entity, ApiKey::class);

        return $entity;
    }
}
