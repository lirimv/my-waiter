<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Doctrine\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126195327 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->connection->beginTransaction();

        $this->addSql('CREATE TABLE account (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account_admin_user (account_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', admin_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_5111A60D9B6B5FBA (account_id), UNIQUE INDEX UNIQ_5111A60D642B8210 (admin_id), PRIMARY KEY(account_id, admin_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE additive (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', abbreviation VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', street VARCHAR(255) NOT NULL, street_number VARCHAR(255) DEFAULT NULL, city VARCHAR(255) NOT NULL, postal_code VARCHAR(255) DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, country VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE api_key (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE assortment (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE assortment_dish (assortment_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_CE6B9D6CD7A15862 (assortment_id), INDEX IDX_CE6B9D6C148EB0CB (dish_id), PRIMARY KEY(assortment_id, dish_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE assortment_channel (assortment_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', channel_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_8F7485B1D7A15862 (assortment_id), INDEX IDX_8F7485B172F5A1AA (channel_id), PRIMARY KEY(assortment_id, channel_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE channel (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, base_url VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE channel_local (channel_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', local_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_3CF7807072F5A1AA (channel_id), INDEX IDX_3CF780705D5A2101 (local_id), PRIMARY KEY(channel_id, local_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE channel_event (channel_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', event_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_8C8F023F72F5A1AA (channel_id), UNIQUE INDEX UNIQ_8C8F023F71F7E88B (event_id), PRIMARY KEY(channel_id, event_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dining_table (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', channel_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', number VARCHAR(255) DEFAULT NULL, seats SMALLINT UNSIGNED NOT NULL, link VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_255380272F5A1AA (channel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', group_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid_binary)\', number VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', price_net_amount BIGINT NOT NULL, price_net_currency_code VARCHAR(3) NOT NULL, INDEX IDX_957D8CB8FE54D947 (group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_side_dish (dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', side_dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_1340E9DE148EB0CB (dish_id), INDEX IDX_1340E9DEC884D3E8 (side_dish_id), PRIMARY KEY(dish_id, side_dish_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_extra (dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', extra_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_27052F28148EB0CB (dish_id), INDEX IDX_27052F282B959FC6 (extra_id), PRIMARY KEY(dish_id, extra_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_ingredient (dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', ingredient_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_77196056148EB0CB (dish_id), INDEX IDX_77196056933FE08C (ingredient_id), PRIMARY KEY(dish_id, ingredient_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_additive (dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', additive_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_FFC74262148EB0CB (dish_id), INDEX IDX_FFC74262384C93F0 (additive_id), PRIMARY KEY(dish_id, additive_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_group (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dish_variation (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', dish_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', price_net_amount BIGINT NOT NULL, price_net_currency_code VARCHAR(3) NOT NULL, INDEX IDX_B8E277BB148EB0CB (dish_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_store (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', event VARCHAR(255) NOT NULL, payload LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', price_net_amount BIGINT NOT NULL, price_net_currency_code VARCHAR(3) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingredient (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE local (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', account_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', address_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_8BD688E89B6B5FBA (account_id), INDEX IDX_8BD688E8F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE side_dish (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', price_net_amount BIGINT NOT NULL, price_net_currency_code VARCHAR(3) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE side_dish_extra (side_dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', extra_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_B30B0637C884D3E8 (side_dish_id), INDEX IDX_B30B06372B959FC6 (extra_id), PRIMARY KEY(side_dish_id, extra_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE side_dish_ingredient (side_dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', ingredient_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_5720E2A5C884D3E8 (side_dish_id), INDEX IDX_5720E2A5933FE08C (ingredient_id), PRIMARY KEY(side_dish_id, ingredient_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE side_dish_additive (side_dish_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', additive_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', INDEX IDX_4B431457C884D3E8 (side_dish_id), INDEX IDX_4B431457384C93F0 (additive_id), PRIMARY KEY(side_dish_id, additive_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tax (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', rate INT NOT NULL, label VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8E81BA768CDE5729 (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary)\', email VARCHAR(180) NOT NULL, phone VARCHAR(255) DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE account_admin_user ADD CONSTRAINT FK_5111A60D9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE account_admin_user ADD CONSTRAINT FK_5111A60D642B8210 FOREIGN KEY (admin_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE assortment_dish ADD CONSTRAINT FK_CE6B9D6CD7A15862 FOREIGN KEY (assortment_id) REFERENCES assortment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assortment_dish ADD CONSTRAINT FK_CE6B9D6C148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assortment_channel ADD CONSTRAINT FK_8F7485B1D7A15862 FOREIGN KEY (assortment_id) REFERENCES assortment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE assortment_channel ADD CONSTRAINT FK_8F7485B172F5A1AA FOREIGN KEY (channel_id) REFERENCES channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE channel_local ADD CONSTRAINT FK_3CF7807072F5A1AA FOREIGN KEY (channel_id) REFERENCES channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE channel_local ADD CONSTRAINT FK_3CF780705D5A2101 FOREIGN KEY (local_id) REFERENCES local (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE channel_event ADD CONSTRAINT FK_8C8F023F72F5A1AA FOREIGN KEY (channel_id) REFERENCES channel (id)');
        $this->addSql('ALTER TABLE channel_event ADD CONSTRAINT FK_8C8F023F71F7E88B FOREIGN KEY (event_id) REFERENCES event_store (id)');
        $this->addSql('ALTER TABLE dining_table ADD CONSTRAINT FK_255380272F5A1AA FOREIGN KEY (channel_id) REFERENCES channel (id)');
        $this->addSql('ALTER TABLE dish ADD CONSTRAINT FK_957D8CB8FE54D947 FOREIGN KEY (group_id) REFERENCES dish_group (id)');
        $this->addSql('ALTER TABLE dish_side_dish ADD CONSTRAINT FK_1340E9DE148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_side_dish ADD CONSTRAINT FK_1340E9DEC884D3E8 FOREIGN KEY (side_dish_id) REFERENCES side_dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_extra ADD CONSTRAINT FK_27052F28148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_extra ADD CONSTRAINT FK_27052F282B959FC6 FOREIGN KEY (extra_id) REFERENCES extra (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_ingredient ADD CONSTRAINT FK_77196056148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_ingredient ADD CONSTRAINT FK_77196056933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_additive ADD CONSTRAINT FK_FFC74262148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_additive ADD CONSTRAINT FK_FFC74262384C93F0 FOREIGN KEY (additive_id) REFERENCES additive (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dish_variation ADD CONSTRAINT FK_B8E277BB148EB0CB FOREIGN KEY (dish_id) REFERENCES dish (id)');
        $this->addSql('ALTER TABLE local ADD CONSTRAINT FK_8BD688E89B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE local ADD CONSTRAINT FK_8BD688E8F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE side_dish_extra ADD CONSTRAINT FK_B30B0637C884D3E8 FOREIGN KEY (side_dish_id) REFERENCES side_dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE side_dish_extra ADD CONSTRAINT FK_B30B06372B959FC6 FOREIGN KEY (extra_id) REFERENCES extra (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE side_dish_ingredient ADD CONSTRAINT FK_5720E2A5C884D3E8 FOREIGN KEY (side_dish_id) REFERENCES side_dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE side_dish_ingredient ADD CONSTRAINT FK_5720E2A5933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE side_dish_additive ADD CONSTRAINT FK_4B431457C884D3E8 FOREIGN KEY (side_dish_id) REFERENCES side_dish (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE side_dish_additive ADD CONSTRAINT FK_4B431457384C93F0 FOREIGN KEY (additive_id) REFERENCES additive (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->connection->beginTransaction();

        $this->addSql('ALTER TABLE account_admin_user DROP FOREIGN KEY FK_5111A60D9B6B5FBA');
        $this->addSql('ALTER TABLE local DROP FOREIGN KEY FK_8BD688E89B6B5FBA');
        $this->addSql('ALTER TABLE dish_additive DROP FOREIGN KEY FK_FFC74262384C93F0');
        $this->addSql('ALTER TABLE side_dish_additive DROP FOREIGN KEY FK_4B431457384C93F0');
        $this->addSql('ALTER TABLE local DROP FOREIGN KEY FK_8BD688E8F5B7AF75');
        $this->addSql('ALTER TABLE assortment_dish DROP FOREIGN KEY FK_CE6B9D6CD7A15862');
        $this->addSql('ALTER TABLE assortment_channel DROP FOREIGN KEY FK_8F7485B1D7A15862');
        $this->addSql('ALTER TABLE assortment_channel DROP FOREIGN KEY FK_8F7485B172F5A1AA');
        $this->addSql('ALTER TABLE channel_local DROP FOREIGN KEY FK_3CF7807072F5A1AA');
        $this->addSql('ALTER TABLE channel_event DROP FOREIGN KEY FK_8C8F023F72F5A1AA');
        $this->addSql('ALTER TABLE dining_table DROP FOREIGN KEY FK_255380272F5A1AA');
        $this->addSql('ALTER TABLE assortment_dish DROP FOREIGN KEY FK_CE6B9D6C148EB0CB');
        $this->addSql('ALTER TABLE dish_side_dish DROP FOREIGN KEY FK_1340E9DE148EB0CB');
        $this->addSql('ALTER TABLE dish_extra DROP FOREIGN KEY FK_27052F28148EB0CB');
        $this->addSql('ALTER TABLE dish_ingredient DROP FOREIGN KEY FK_77196056148EB0CB');
        $this->addSql('ALTER TABLE dish_additive DROP FOREIGN KEY FK_FFC74262148EB0CB');
        $this->addSql('ALTER TABLE dish_variation DROP FOREIGN KEY FK_B8E277BB148EB0CB');
        $this->addSql('ALTER TABLE dish DROP FOREIGN KEY FK_957D8CB8FE54D947');
        $this->addSql('ALTER TABLE channel_event DROP FOREIGN KEY FK_8C8F023F71F7E88B');
        $this->addSql('ALTER TABLE dish_extra DROP FOREIGN KEY FK_27052F282B959FC6');
        $this->addSql('ALTER TABLE side_dish_extra DROP FOREIGN KEY FK_B30B06372B959FC6');
        $this->addSql('ALTER TABLE dish_ingredient DROP FOREIGN KEY FK_77196056933FE08C');
        $this->addSql('ALTER TABLE side_dish_ingredient DROP FOREIGN KEY FK_5720E2A5933FE08C');
        $this->addSql('ALTER TABLE channel_local DROP FOREIGN KEY FK_3CF780705D5A2101');
        $this->addSql('ALTER TABLE dish_side_dish DROP FOREIGN KEY FK_1340E9DEC884D3E8');
        $this->addSql('ALTER TABLE side_dish_extra DROP FOREIGN KEY FK_B30B0637C884D3E8');
        $this->addSql('ALTER TABLE side_dish_ingredient DROP FOREIGN KEY FK_5720E2A5C884D3E8');
        $this->addSql('ALTER TABLE side_dish_additive DROP FOREIGN KEY FK_4B431457C884D3E8');
        $this->addSql('ALTER TABLE account_admin_user DROP FOREIGN KEY FK_5111A60D642B8210');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE account_admin_user');
        $this->addSql('DROP TABLE additive');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE api_key');
        $this->addSql('DROP TABLE assortment');
        $this->addSql('DROP TABLE assortment_dish');
        $this->addSql('DROP TABLE assortment_channel');
        $this->addSql('DROP TABLE channel');
        $this->addSql('DROP TABLE channel_local');
        $this->addSql('DROP TABLE channel_event');
        $this->addSql('DROP TABLE dining_table');
        $this->addSql('DROP TABLE dish');
        $this->addSql('DROP TABLE dish_side_dish');
        $this->addSql('DROP TABLE dish_extra');
        $this->addSql('DROP TABLE dish_ingredient');
        $this->addSql('DROP TABLE dish_additive');
        $this->addSql('DROP TABLE dish_group');
        $this->addSql('DROP TABLE dish_variation');
        $this->addSql('DROP TABLE event_store');
        $this->addSql('DROP TABLE extra');
        $this->addSql('DROP TABLE ingredient');
        $this->addSql('DROP TABLE local');
        $this->addSql('DROP TABLE side_dish');
        $this->addSql('DROP TABLE side_dish_extra');
        $this->addSql('DROP TABLE side_dish_ingredient');
        $this->addSql('DROP TABLE side_dish_additive');
        $this->addSql('DROP TABLE tax');
        $this->addSql('DROP TABLE user');
    }
}
