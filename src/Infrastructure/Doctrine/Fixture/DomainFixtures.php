<?php

namespace MyWaiter\Infrastructure\Doctrine\Fixture;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Money\Money;
use MyWaiter\Domain\Account\Account;
use MyWaiter\Domain\Address\Address;
use MyWaiter\Domain\ApiKey\ApiKey;
use MyWaiter\Domain\Assortment\Assortment;
use MyWaiter\Domain\Channel\Channel;
use MyWaiter\Domain\Channel\ChannelType;
use MyWaiter\Domain\Dish\Dish;
use MyWaiter\Domain\Dish\Group\DishGroup;
use MyWaiter\Domain\Dish\Price;
use MyWaiter\Domain\Dish\SideDish\SideDish;
use MyWaiter\Domain\Dish\Variation\DishVariation;
use MyWaiter\Domain\Dish\Variation\DishVariationType;
use MyWaiter\Domain\Local\Local;
use MyWaiter\Domain\Table\Table;
use MyWaiter\Domain\User\Role;
use MyWaiter\Domain\User\User;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Yaml;
use Webmozart\Assert\Assert;

class DomainFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;
    private ObjectManager $manager;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->createApiKeys();
        $this->createAdminUser();
        $account = $this->createAccount();
        $address = $this->createAddress();
        $local = $this->createLocal($account, $address);

        $onlineChannel = $this->createChannel(ChannelType::online());
        $onlineChannel->addLocal($local);

        $restaurantChannel = $this->createChannel(ChannelType::locally());
        $restaurantChannel->addLocal($local);

        for ($i = 1; $i < 10; ++$i) {
            $this->createTable($restaurantChannel);
        }

        $dishes = [];
        $menu = Yaml::parseFile(__DIR__.'/products.yml');

        $sideDishes['pommes'] = $this->createSideDish('Pommes', 200);
        $sideDishes['beilagensalat'] = $this->createSideDish('Beilagensalat', 250);

        foreach ($menu as $group) {
            Assert::keyExists($group, 'name');
            Assert::keyExists($group, 'products');
            $dishGroup = $this->createDishGroup($group['name']);

            foreach ($group['products'] as $product) {
                Assert::keyExists($product, 'name');
                Assert::keyExists($product, 'price');
                Assert::keyExists($product, 'description');

                if (array_key_exists('number', $product)) {
                    $number = $product['number'];
                    $name = $product['name'];
                } else {
                    $parts = explode('.', $product['name']);
                    $number = trim($parts[0]);
                    $name = trim($parts[1]);
                }

                $price = (int) (((float) $product['price']) * 100);

                $dish = $this->createDish($dishGroup, $name, $number, $product['description'], $price);

                if (array_key_exists('side', $product)) {
                    foreach ($product['side'] as $side) {
                        $dish->addSideDish($sideDishes[$side]);
                    }
                }
                $dishes[] = $dish;
            }
        }

        $localAssortment = $this->createAssortment('Restaurant Artikel');
        $localAssortment->addChannel($restaurantChannel);
        foreach ($dishes as $dish) {
            $localAssortment->addDish($dish);
        }

        $onlineAssortment = $this->createAssortment('Onlineshop Artikel');
        $onlineAssortment->addChannel($onlineChannel);
        $onlineAssortment->addDish($dishes[0]);
        $onlineAssortment->addDish($dishes[1]);
        $onlineAssortment->addDish($dishes[2]);
        $onlineAssortment->addDish($dishes[8]);
        $onlineAssortment->addDish($dishes[7]);

        $manager->flush();
    }

    private function createApiKeys(): void
    {
        $shopApiKey = new ApiKey(Uuid::uuid4(), 'shop_api_key', 'b240c07efbd8bb2e38e657f69dbfccf0f9639c94', 'shop');
        $adminApiKey = new ApiKey(Uuid::uuid4(), 'admin_api_key', '6985093bb10eae1bb75309fa220c4a138222694f', 'admin');

        $this->manager->persist($shopApiKey);
        $this->manager->persist($adminApiKey);
    }

    private function createAdminUser(): User
    {
        $user = new User(
            Uuid::uuid4(),
            'lirim@veliu.net',
            null,
            '+4917660411082',
            [Role::ADMIN, Role::ADMIN_USER]
        );

        $user->setPassword($this->encoder->encodePassword(\MyWaiter\Adapter\Shop\Security\User::fromUser($user), 'test'));

        $this->manager->persist($user);

        return $user;
    }

    private function createAccount(): Account
    {
        $account = new Account(Uuid::uuid4());

        $this->manager->persist($account);

        return $account;
    }

    private function createAddress(): Address
    {
        $address = new Address(
            null,
            'Kantstrasse',
            '7',
            'Obertshausen',
            '63179',
            'Hessen',
            'DE'
        );

        $this->manager->persist($address);

        return $address;
    }

    private function createLocal(Account $account, Address $address): Local
    {
        $local = new Local(
            null,
            'Alte Schmiede '.random_int(1, 10000),
            $account,
            $address
        );

        $this->manager->persist($local);

        return $local;
    }

    private function createChannel(ChannelType $type): Channel
    {
        $channel = new Channel(
            null,
            'La Fattoria '.$type,
            'online@example.test',
            $type,
            'https://pizzeria-lafattoria.test'
        );

        $this->manager->persist($channel);

        return $channel;
    }

    private function createTable(Channel $channel): Table
    {
        $table = new Table(
            null,
            'Nr. '.random_int(1, 100),
            random_int(2, 12),
            $channel,
        );

        $this->manager->persist($table);

        return $table;
    }

    private function createDishGroup(string $name): DishGroup
    {
        $group = new DishGroup(null, $name, 'Mhhh lecker '.$name);

        $this->manager->persist($group);

        return $group;
    }

    private function createDish(
        DishGroup $group,
        ?string $name = null,
        ?string $number = null,
        ?string $description = null,
        ?int $price = null
    ): Dish {
        $rand = (string) random_int(1, 1000);
        $dish = new Dish(
            Uuid::uuid4(),
            $name ?? 'Pizza no '.$rand,
            $number ?? $rand,
            $description ?? 'Beschreibung '.$rand,
            new Price(Money::EUR($price ?? random_int(100, 2500))),
            $group
        );

        $this->manager->persist($dish);

        return $dish;
    }

    private function createDishVariation(DishVariationType $type, Dish $dish): DishVariation
    {
        $variation = new DishVariation(
            Uuid::uuid4(),
            'Variation '.$type.random_int(1, 1000),
            $type,
            new Price(Money::EUR(400)),
            $dish
        );

        $this->manager->persist($variation);

        return $variation;
    }

    private function createSideDish(?string $name, ?int $price): SideDish
    {
        $sideDish = new SideDish(
            Uuid::uuid4(),
            $name ?? 'Beilage',
            new Price(Money::EUR($price ?? 500))
        );

        $this->manager->persist($sideDish);

        return $sideDish;
    }

    private function createAssortment(string $name): Assortment
    {
        $assortment = new Assortment(null, $name);

        $this->manager->persist($assortment);

        return $assortment;
    }
}
