<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\Mapping\ClassMetadata;
use MyWaiter\Domain\Entity\Filter;
use MyWaiter\Domain\Entity\Sorting;
use MyWaiter\Domain\Metadata;
use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;

final class QueryHelper
{
    /** @var string[] */
    private array $joinedFields = [];
    private ClassMetadata $classMetadata;
    private QueryBuilder $queryBuilder;
    private string $alias;

    private function __construct(
        ClassMetadata $classMetadata,
        QueryBuilder $queryBuilder,
        string $alias
    ) {
        $this->classMetadata = $classMetadata;
        $this->queryBuilder = $queryBuilder;
        $this->alias = $alias;
    }

    public static function create(
        ClassMetadata $classMetadata,
        QueryBuilder $queryBuilder,
        string $alias
    ): QueryHelper {
        return new self($classMetadata, $queryBuilder, $alias);
    }

    private function addJoinForFieldPath(string $fieldNamePath): bool
    {
        // make sure no es syntax sneaks in here
        $fieldNamePath = str_replace('.keyword', '', $fieldNamePath);

        $pathSegments = explode('.', $fieldNamePath);
        Assert::lessThanEq(count($pathSegments), 2);

        $joinPropertyName = $pathSegments[0];
        if (in_array($joinPropertyName, $this->joinedFields, true)) {
            return true;
        }

        if (
            !$this->classMetadata->hasAssociation($joinPropertyName)
        ) {
            return false;
        }

        $this->queryBuilder->join(sprintf('%s.%s', $this->alias, $joinPropertyName), $this->applyJoinPropertyRenaming($joinPropertyName));
        $this->joinedFields[] = $joinPropertyName;

        return true;
    }

    /**
     * Whenever needed this method will join fields that are marked as SingleValuedAssociations (no
     * toMany!) on the root entity automatically.
     */
    public function addFilters(Filter $filter): void
    {
        foreach ($filter->all() as $name => $value) {
            $type = $this->classMetadata->getTypeOfField($name);

            if ('id' === $name || $this->classMetadata->hasAssociation($name)) {
                $type = Metadata::FILTER_DATA_TYPE_UUID;
            }

            $this->addFilter($name, $value, $type ?? Metadata::FILTER_DATA_TYPE_STRING);
        }
    }

    /**
     * Whenever needed this method will join fields that are marked as SingleValuedAssociations (no
     * toMany!) on the root entity automatically.
     */
    public function addSorting(Sorting $sorting): void
    {
        foreach ($sorting->all() as $field => $value) {
            $fieldExpr = sprintf('%s.%s', $this->alias, $field);
            if ($this->addJoinForFieldPath($field)) {
                $fieldExpr = $this->applyJoinPropertyRenaming($field);
            }

            $this->queryBuilder->addOrderBy($fieldExpr, strtoupper($value));
        }
    }

    private function addFilter(string $field, mixed $value, string $dataType = Metadata::FILTER_DATA_TYPE_STRING): void
    {
        Assert::oneOf($dataType, [Metadata::FILTER_DATA_TYPE_STRING, Metadata::FILTER_DATA_TYPE_UUID, Metadata::FILTER_DATA_TYPE_NUMBER]);

        $this->createFieldFilter($field, $value, $dataType);
    }

    /**
     * @param string|string[] $value
     */
    private function createFieldFilter(string $field, $value, string $dataType): void
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $parameterName = $this->buildParameterName($field);

        $fieldExpr = sprintf('%s.%s', $this->alias, $field);
        if ($this->addJoinForFieldPath($field)) {
            $fieldExpr = $this->applyJoinPropertyRenaming($field);
        }

        $this->queryBuilder->andWhere($this->queryBuilder->expr()->in($fieldExpr, ':'.$parameterName));

        if (Metadata::FILTER_DATA_TYPE_UUID === $dataType) {
            $value = array_map(static fn (string $uuidString) => Uuid::fromString($uuidString)->getBytes(), $value);
        }

        $this->queryBuilder->setParameter($parameterName, $value, Connection::PARAM_STR_ARRAY);
    }

    private function buildParameterName(string $field): string
    {
        return 'param'.sha1(sprintf('%s.secret', $field));
    }

    private function applyJoinPropertyRenaming(string $originalName): string
    {
        return 'join_'.$originalName;
    }
}
