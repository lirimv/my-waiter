<?php

declare(strict_types=1);

namespace MyWaiter\Infrastructure\Doctrine\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Lcobucci\Clock\Clock;
use MyWaiter\Domain\Entity\TimestampableInterface;

final class TimestampableSubscriber implements EventSubscriber
{
    private Clock $clock;

    public function __construct(Clock $clock)
    {
        $this->clock = $clock;
    }

    public function getSubscribedEvents(): array
    {
        return [Events::prePersist, Events::preUpdate];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        if (!$entity instanceof TimestampableInterface) {
            return;
        }

        $now = $this->clock->now();

        $entity->setCreatedAt($now);
        $entity->setUpdatedAt($now);
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof TimestampableInterface) {
            return;
        }

        if ([] === $args->getEntityChangeSet()) {
            return;
        }

        $entity->setUpdatedAt($this->clock->now());
    }
}
