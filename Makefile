composer-install:
	composer install

fixtures:
	bin/console doctrine:fixtures:load -n --append

db-reload:
	bin/console doctrine:database:drop --if-exists --force
	bin/console doctrine:database:create
	bin/console doctrine:migration:migrate --no-interaction

init: composer-install db-reload fixtures

install:
	composer install --no-dev --optimize-autoloader
	bin/console doctrine:database:create --if-not-exists
	bin/console doctrine:migration:migrate --no-interaction
	bin/console c:c

phpstan:
	vendor/bin/phpstan analyse -c phpstan.neon

php-cs-fix:
	PHP_CS_FIXER_IGNORE_ENV=1 vendor/bin/php-cs-fixer fix --allow-risky=yes

lint: php-cs-fix phpstan psalm deptrac-adapters deptrac-hexagonal-layers

psalm:
	./vendor/bin/psalm

deptrac-adapters:																## checks that adapters do not talk to each other
	vendor/bin/deptrac analyse --no-progress depfile-adapters.yml

deptrac-hexagonal-layers:														## checks how hexagonal layers talk to each other
	vendor/bin/deptrac analyse --no-progress depfile-hexagonal-layers.yml
